<?php

declare(strict_types = 1);

/**
 * @file
 * Views hooks for the Date Point module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function date_point_field_views_data(FieldStorageConfigInterface $field_storage): array {
  // @todo Fix this once we drop support for Drupal 11.1.
  // @phpstan-ignore function.deprecated
  $data = \views_field_default_views_data($field_storage);
  foreach ($data as $table_name => $table_data) {
    $column = $field_storage->getName() . '_value';
    if ($field_storage->getType() === 'dp_date_time') {
      $data[$table_name][$column]['filter']['id'] = 'dp_date_time';
      $data[$table_name][$column]['argument']['id'] = 'dp_date_time';
    }
    elseif ($field_storage->getType() === 'dp_date') {
      $data[$table_name][$column]['filter']['id'] = 'dp_date';
    }
    elseif ($field_storage->getType() === 'dp_time') {
      $data[$table_name][$column]['filter']['id'] = 'dp_time';
    }
  }
  return $data;
}
