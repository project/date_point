<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Tests\UnitTestCase;
use Drupal\date_point\Data\DateTime\Granularity;

/**
 * A test for granularity enum.
 *
 * @group date_point
 */
final class GranularityTest extends UnitTestCase {

  /**
   * {@selfdoc}
   */
  public function testGranularity(): void {
    $expected_cases = [
      Granularity::MICROSECOND,
      Granularity::MILLISECOND,
      Granularity::SECOND,
      Granularity::MINUTE,
      Granularity::HOUR,
      Granularity::DAY,
      Granularity::MONTH,
      Granularity::YEAR,
    ];
    // @phpstan-ignore-next-line
    self::assertSame($expected_cases, Granularity::cases());
    $expected_values = [
      'microsecond',
      'millisecond',
      'second',
      'minute',
      'hour',
      'day',
      'month',
      'year',
    ];
    self::assertSame($expected_values, \array_column(Granularity::cases(), 'value'));
  }

  /**
   * {@selfdoc}
   */
  public function testGetLabel(): void {
    self::assertEquals(new TM('Microsecond'), Granularity::MICROSECOND->getLabel());
    self::assertEquals(new TM('Millisecond'), Granularity::MILLISECOND->getLabel());
    self::assertEquals(new TM('Second'), Granularity::SECOND->getLabel());
    self::assertEquals(new TM('Minute'), Granularity::MINUTE->getLabel());
    self::assertEquals(new TM('Hour'), Granularity::HOUR->getLabel());
    self::assertEquals(new TM('Day'), Granularity::DAY->getLabel());
    self::assertEquals(new TM('Month'), Granularity::MONTH->getLabel());
    self::assertEquals(new TM('Year'), Granularity::YEAR->getLabel());
  }

  /**
   * {@selfdoc}
   */
  public function testGetOptions(): void {
    $expected_options = [
      'microsecond' => new TM('Microsecond'),
      'millisecond' => new TM('Millisecond'),
      'second' => new TM('Second'),
      'minute' => new TM('Minute'),
      'hour' => new TM('Hour'),
      'day' => new TM('Day'),
      'month' => new TM('Month'),
      'year' => new TM('Year'),
    ];
    self::assertEquals($expected_options, Granularity::getOptions());
  }

  /**
   * @dataProvider providerGetBoundaries
   */
  public function testGetBoundaries(Granularity $granularity, DTI $date, array $expected_boundaries): void {
    $actual_boundaries = $granularity->getBoundaries($date);
    self::assertSame($expected_boundaries, $actual_boundaries);
  }

  /**
   * Data provider for testGetBoundaries()
   *
   * Returns three timestamps for each granularity (except microsecond).
   *  - At the beginning of the interval.
   *  - In the middle of the interval.
   *  - In the end of the interval.
   */
  public static function providerGetBoundaries(): \Generator {
    // -- Microsecond.
    yield [
      Granularity::MICROSECOND,
      new DTI('2000-01-01 00:00:00.000000+00:00'),
      ['2000-01-01 00:00:00.000000', '2000-01-01 00:00:00.000001'],
    ];
    // -- Millisecond.
    yield [
      Granularity::MILLISECOND,
      new DTI('2000-01-01 00:00:00.000+00:00'),
      ['2000-01-01 00:00:00.000', '2000-01-01 00:00:00.001'],
    ];
    yield [
      Granularity::MILLISECOND,
      new DTI('2015-12-01 12:43:49.456+04:00'),
      ['2015-12-01 08:43:49.456', '2015-12-01 08:43:49.457'],
    ];
    yield [
      Granularity::MILLISECOND,
      new DTI('2015-12-01 12:20:10.999999+14:00'),
      ['2015-11-30 22:20:10.999', '2015-11-30 22:20:11.000'],
    ];
    // -- Second.
    yield [
      Granularity::SECOND,
      new DTI('2000-01-01 00:00:00+00:00'),
      ['2000-01-01 00:00:00', '2000-01-01 00:00:01'],
    ];
    yield [
      Granularity::SECOND,
      new DTI('2024-03-01 15:30:47.123+05:00'),
      ['2024-03-01 10:30:47', '2024-03-01 10:30:48'],
    ];
    yield [
      Granularity::SECOND,
      new DTI('2024-03-01 12:25:47.999+05:00'),
      ['2024-03-01 07:25:47', '2024-03-01 07:25:48'],
    ];
    // -- Minute.
    yield [
      Granularity::MINUTE,
      new DTI('2000-01-01 00:00:00+00:00'),
      ['2000-01-01 00:00:00', '2000-01-01 00:01:00'],
    ];
    yield [
      Granularity::MINUTE,
      new DTI('2024-03-01 15:30:00+05:00'),
      ['2024-03-01 10:30:00', '2024-03-01 10:31:00'],
    ];
    yield [
      Granularity::MINUTE,
      new DTI('2024-03-01 15:30:59.999999+05:00'),
      ['2024-03-01 10:30:00', '2024-03-01 10:31:00'],
    ];
    // -- Hour.
    yield [
      Granularity::HOUR,
      new DTI('2000-01-01 00:00:00+00:00'),
      ['2000-01-01 00:00:00', '2000-01-01 01:00:00'],
    ];
    yield [
      Granularity::HOUR,
      new DTI('2024-03-01 15:00:00+05:00'),
      ['2024-03-01 10:00:00', '2024-03-01 11:00:00'],
    ];
    yield [
      Granularity::HOUR,
      new DTI('2024-03-01 15:59:59.999+05:00'),
      ['2024-03-01 10:00:00', '2024-03-01 11:00:00'],
    ];
    // -- Day.
    yield [
      Granularity::DAY,
      new DTI('2000-01-01 00:00:00+00:00'),
      ['2000-01-01 00:00:00', '2000-01-02 00:00:00'],
    ];
    yield [
      Granularity::DAY,
      new DTI('2024-03-01 00:00:00+05:00'),
      ['2024-02-29 19:00:00', '2024-03-01 19:00:00'],
    ];
    yield [
      Granularity::DAY,
      new DTI('2024-03-01 23:59:59.999+05:00'),
      ['2024-02-29 19:00:00', '2024-03-01 19:00:00'],
    ];
    // -- Month.
    yield [
      Granularity::MONTH,
      new DTI('2000-01-01 00:00:00+00:00'),
      ['2000-01-01 00:00:00', '2000-02-01 00:00:00'],
    ];
    yield [
      Granularity::MONTH,
      new DTI('2012-05-15 16:27:59+12:00'),
      ['2012-04-30 12:00:00', '2012-05-31 12:00:00'],
    ];
    yield [
      Granularity::MONTH,
      new DTI('2024-03-31 23:59:59.999+00:00'),
      ['2024-03-01 00:00:00', '2024-04-01 00:00:00'],
    ];
    // -- Year.
    yield [
      Granularity::YEAR,
      new DTI('2000-01-01 00:00:00+00:00'),
      ['2000-01-01 00:00:00', '2001-01-01 00:00:00'],
    ];
    yield [
      Granularity::YEAR,
      new DTI('2012-08-15 12:27:59+12:00'),
      ['2011-12-31 12:00:00', '2012-12-31 12:00:00'],
    ];
    yield [
      Granularity::YEAR,
      new DTI('2024-12-31 23:59:59.999+02:00'),
      ['2023-12-31 22:00:00', '2024-12-31 22:00:00'],
    ];
  }

}
