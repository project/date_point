<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit\DateTime;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Tests\UnitTestCase;
use Drupal\date_point\Data\DateTime\Behavior;

/**
 * A test for Behavior enum.
 *
 * @group date_point
 */
final class BehaviorTest extends UnitTestCase {

  /**
   * {@selfdoc}
   */
  public function testBehavior(): void {
    $expected_cases = [
      Behavior::MANUAL,
      Behavior::CREATED,
      Behavior::UPDATED,
    ];
    // @phpstan-ignore-next-line
    self::assertSame($expected_cases, Behavior::cases());
    $expected_values = [
      'manual',
      'created',
      'updated',
    ];
    self::assertSame($expected_values, \array_column(Behavior::cases(), 'value'));
  }

  /**
   * {@selfdoc}
   */
  public function testGetLabel(): void {
    self::assertEquals(new TM('Manual'), Behavior::MANUAL->getLabel());
    self::assertEquals(new TM('Created'), Behavior::CREATED->getLabel());
    self::assertEquals(new TM('Updated'), Behavior::UPDATED->getLabel());
  }

  /**
   * {@selfdoc}
   */
  public function testGetOptions(): void {
    $expected_options = [
      'manual' => new TM('Manual'),
      'created' => new TM('Created'),
      'updated' => new TM('Updated'),
    ];
    self::assertEquals($expected_options, Behavior::getOptions());
  }

}
