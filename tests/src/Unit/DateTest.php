<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit;

use DateTimeImmutable as DTI;
use Drupal\Tests\UnitTestCase;
use Drupal\date_point\Data\Date;

/**
 * A test for date value object.
 *
 * @group date_point
 */
final class DateTest extends UnitTestCase {

  /**
   * @dataProvider dateProvider
   */
  public function testConstructor(string $input, ?string $expected_date): void {
    if (!$expected_date) {
      self::expectExceptionObject(
        new \DateMalformedStringException(\sprintf('Wrong date value "%s".', $input)),
      );
    }
    $date = new Date($input);
    self::assertSame($expected_date, $date->format('Y-m-d'));
  }

  /**
   * {@selfdoc}
   */
  public static function dateProvider(): \Generator {
    yield ['123', NULL];
    yield ['hhh', NULL];
    yield ['2024 - 12-05', NULL];
    yield [' 2024-12-05 ', NULL];
    yield ['2024-12-05', '2024-12-05'];
    yield ['0000-12-05', '0000-12-05'];
    yield ['0000-1-5', '0000-01-05'];
    yield ['12345-1-5', NULL];
  }

  /**
   * {@selfdoc}
   */
  public function testFromDateTime(): void {
    $dateTime = new DTI('2024-01-01', new \DateTimeZone('Europe/Moscow'));
    $date = Date::fromDateTime($dateTime);
    self::assertSame('2024-01-01', $date->format('Y-m-d'));

    $dateTime = new DTI('2024-01-05', new \DateTimeZone('UTC'));
    $date = Date::fromDateTime($dateTime);
    self::assertSame('2024-01-05', $date->format('Y-m-d'));
  }

  /**
   * {@selfdoc}
   */
  public function testFormat(): void {
    $date = new Date('2000-04-01');
    self::assertSame('01.04.2000', $date->format('d.m.Y'));
  }

  /**
   * {@selfdoc}
   */
  public function testToString(): void {
    $date = new Date('2000-10-01');
    self::assertSame('2000-10-01', (string) $date);
  }

  /**
   * {@selfdoc}
   */
  public function testJsonSerialize(): void {
    $date = new Date('2000-10-01');
    self::assertSame('"2000-10-01"', \json_encode($date));
  }

  /**
   * {@selfdoc}
   */
  public function testToDatetime(): void {
    $date = new Date('2000-10-15');
    self::assertEquals(
      new \DateTimeImmutable('2000-10-15'),
      $date->toDateTime(),
    );
    self::assertEquals(
      new \DateTimeImmutable('2000-10-15', new \DateTimeZone('Europe/Moscow')),
      $date->toDateTime(new \DateTimeZone('Europe/Moscow')),
    );
    self::assertEquals(
      new \DateTimeImmutable('2000-10-15', new \DateTimeZone('Asia/Shanghai')),
      $date->toDateTime(new \DateTimeZone('Asia/Shanghai')),
    );
  }

}
