<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Tests\UnitTestCase;
use Drupal\date_point\Data\Precision;

/**
 * A test for precision enum.
 *
 * @group date_point
 */
final class PrecisionTest extends UnitTestCase {

  /**
   * {@selfdoc}
   */
  public function testPrecision(): void {
    $expected_cases = [
      Precision::SECOND,
      Precision::MILLISECOND,
      Precision::MICROSECOND,
    ];
    // @phpstan-ignore-next-line
    self::assertSame($expected_cases, Precision::cases());
    $expected_values = [
      'second',
      'millisecond',
      'microsecond',
    ];
    self::assertSame($expected_values, \array_column(Precision::cases(), 'value'));
  }

  /**
   * {@selfdoc}
   */
  public function testToInteger(): void {
    self::assertEquals(0, Precision::SECOND->toInteger());
    self::assertEquals(3, Precision::MILLISECOND->toInteger());
    self::assertEquals(6, Precision::MICROSECOND->toInteger());
  }

  /**
   * {@selfdoc}
   */
  public function testGetLabel(): void {
    self::assertEquals(new TM('Second'), Precision::SECOND->getLabel());
    self::assertEquals(new TM('Millisecond'), Precision::MILLISECOND->getLabel());
    self::assertEquals(new TM('Microsecond'), Precision::MICROSECOND->getLabel());
  }

  /**
   * {@selfdoc}
   */
  public function testGetOptions(): void {
    $expected_options = [
      'second' => new TM('Second'),
      'millisecond' => new TM('Millisecond'),
      'microsecond' => new TM('Microsecond'),
    ];
    self::assertEquals($expected_options, Precision::getOptions());
  }

}
