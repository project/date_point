<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit;

use DateTimeImmutable as DTI;
use Drupal\Tests\UnitTestCase;
use Drupal\date_point\Data\Time;

/**
 * A test for time value object.
 *
 * @group date_point
 */
final class TimeTest extends UnitTestCase {

  /**
   * @dataProvider timeProvider
   */
  public function testConstructor(string $input, ?string $expected_time): void {
    if (!$expected_time) {
      self::expectExceptionObject(
        new \DateMalformedStringException(\sprintf('Wrong time value "%s".', $input)),
      );
    }
    $time = new Time($input);
    self::assertSame($expected_time, $time->format('H:i:s.u'));
  }

  /**
   * {@selfdoc}
   */
  public static function timeProvider(): \Generator {
    yield ['123', NULL];
    yield ['hhh', NULL];
    yield ['12: 15', NULL];
    yield [' 12:15 ', NULL];
    yield ['12:55', '12:55:00.000000'];
    yield ['05:12:45', '05:12:45.000000'];
    yield ['05:12:45.1', '05:12:45.100000'];
    yield ['05:12:45.12', '05:12:45.120000'];
    yield ['05:12:45.123', '05:12:45.123000'];
    yield ['05:12:45.1234', '05:12:45.123400'];
    yield ['05:12:45.12345', '05:12:45.123450'];
    yield ['05:12:45.123456', '05:12:45.123456'];
    yield ['24:00:00', NULL];
    yield ['00:60:00', NULL];
    yield ['00:00:60', NULL];
    yield ['23:59:59.999999', '23:59:59.999999'];
  }

  /**
   * {@selfdoc}
   */
  public function testFromDateTime(): void {
    $dateTime = new DTI('15:00:21', new \DateTimeZone('Europe/Moscow'));
    $time = Time::fromDateTime($dateTime);
    self::assertSame('15:00:21.000000', $time->format('H:i:s.u'));

    $dateTime = new DTI('2024-02-29 00:00:00.123456', new \DateTimeZone('Europe/Moscow'));
    $time = Time::fromDateTime($dateTime);
    self::assertSame('00:00:00.123456', $time->format('H:i:s.u'));
  }

  /**
   * @dataProvider secondsProvider
   */
  public function testFromSeconds(int|float $seconds, ?string $expected_time): void {
    if (!$expected_time) {
      self::expectExceptionObject(
        new \OutOfRangeException('The time value should be in withing 0 - 86400 seconds.'),
      );
    }
    $time = Time::fromSeconds($seconds);
    self::assertSame($expected_time, $time->format('H:i:s.u'));
  }

  /**
   * {@selfdoc}
   */
  public static function secondsProvider(): \Generator {
    yield [0, '00:00:00.000000'];
    yield [0.123, '00:00:00.123000'];
    yield [55_555.123456, '15:25:55.123456'];
    yield [60, '00:01:00.000000'];
    yield [3_600, '01:00:00.000000'];
    yield [24 * 3_600, NULL];
    yield [-1, NULL];
  }

  /**
   * {@selfdoc}
   */
  public function testFormat(): void {
    $time = new Time('20:15:54');
    self::assertSame('08:15 PM', $time->format('h:i A'));
  }

  /**
   * {@selfdoc}
   */
  public function testToString(): void {
    $time = new Time('20:15:54');
    self::assertSame('20:15:54', (string) $time);
  }

  /**
   * {@selfdoc}
   */
  public function testJsonSerialize(): void {
    $time = new Time('20:15:54');
    self::assertSame('"20:15:54"', \json_encode($time));
  }

}
