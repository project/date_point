<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit;

use DateTimeImmutable as DTI;
use Drupal\date_point\Data\YearMonth;
use Drupal\Tests\UnitTestCase;

/**
 * A test for YearMonth value object.
 *
 * @group date_point
 */
final class YearMonthTest extends UnitTestCase {

  /**
   * @dataProvider dateProvider
   */
  public function testConstructor(string $input, ?string $expected_month): void {
    if (!$expected_month) {
      self::expectExceptionObject(
        new \DateMalformedStringException(\sprintf('Wrong month value "%s".', $input)),
      );
    }
    $date = new YearMonth($input);
    self::assertSame($expected_month, $date->format('Y-m-d'));
  }

  /**
   * {@selfdoc}
   */
  public static function dateProvider(): \Generator {
    yield ['2024-12', '2024-12-01'];
    yield ['0000-11', '0000-11-01'];
    yield ['123', NULL];
    yield ['hhh', NULL];
    yield ['2024 - 12-05', NULL];
    yield [' 2024-12-05 ', NULL];
    yield ['0000-1', NULL];
    yield ['2024-13', NULL];
    yield ['2024-00', NULL];
    yield ['202400', NULL];
    yield ['2024-aa', NULL];
    yield ['2024-21', NULL];
    yield ['12345-1-5', NULL];
  }

  /**
   * {@selfdoc}
   */
  public function testFromDateTime(): void {
    $datetime = new DTI('2024-01-01', new \DateTimeZone('Europe/Moscow'));
    $month = YearMonth::fromDateTime($datetime);
    self::assertSame('2024-01-01', $month->format('Y-m-d'));

    $datetime = new DTI('2024-01-05', new \DateTimeZone('UTC'));
    $month = YearMonth::fromDateTime($datetime);
    self::assertSame('2024-01-01', $month->format('Y-m-d'));
  }

  /**
   * {@selfdoc}
   */
  public function testFormat(): void {
    $month = new YearMonth('2000-04');
    self::assertSame('01.04.2000', $month->format('d.m.Y'));
  }

  /**
   * {@selfdoc}
   */
  public function testToString(): void {
    $month = new YearMonth('2000-10');
    self::assertSame('2000-10', (string) $month);
  }

  /**
   * {@selfdoc}
   */
  public function testJsonSerialize(): void {
    $month = new YearMonth('2000-10');
    self::assertSame('"2000-10"', \json_encode($month));
  }

  /**
   * {@selfdoc}
   */
  public function testToDatetime(): void {
    $month = new YearMonth('2000-10');
    self::assertEquals(
      new \DateTimeImmutable('2000-10-01'),
      $month->toDateTime(),
    );
    self::assertEquals(
      new \DateTimeImmutable('2000-10-01', new \DateTimeZone('Europe/Moscow')),
      $month->toDateTime(new \DateTimeZone('Europe/Moscow')),
    );
    self::assertEquals(
      new \DateTimeImmutable('2000-10-01', new \DateTimeZone('Asia/Shanghai')),
      $month->toDateTime(new \DateTimeZone('Asia/Shanghai')),
    );
  }

  /**
   * {@selfdoc}
   */
  public function testGetBoundaries(): void {
    $month = new YearMonth('2024-10');

    $expected_boundaries = [
      '2024-10-01 03:00:00.000000',
      '2024-11-01 03:00:00.000000',
    ];
    self::assertSame($expected_boundaries, $month->getBoundaries(new \DateTimeZone('Europe/Moscow')));

    $expected_boundaries = [
      '2024-10-01 00:00:00.000000',
      '2024-11-01 00:00:00.000000',
    ];
    self::assertSame($expected_boundaries, $month->getBoundaries(new \DateTimeZone('UTC')));
  }

}
