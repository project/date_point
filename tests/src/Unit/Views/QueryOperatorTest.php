<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Unit\DateTime;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Tests\UnitTestCase;
use Drupal\date_point\Data\Views\QueryOperator;

/**
 * A test for query operator enum.
 *
 * @group date_point
 */
final class QueryOperatorTest extends UnitTestCase {

  /**
   * {@selfdoc}
   */
  public function testQueryOperator(): void {
    $expected_cases = [
      QueryOperator::EARLIER,
      QueryOperator::EARLIER_EQUAL,
      QueryOperator::EQUAL,
      QueryOperator::NOT_EQUAL,
      QueryOperator::LATER,
      QueryOperator::LATER_EQUAL,
      QueryOperator::EMPTY,
      QueryOperator::NOT_EMPTY,
    ];
    // @phpstan-ignore-next-line
    self::assertSame($expected_cases, QueryOperator::cases());
    $expected_values = [
      '<',
      '<=',
      '=',
      '!=',
      '>',
      '>=',
      'empty',
      'not_empty',
    ];
    self::assertSame($expected_values, \array_column(QueryOperator::cases(), 'value'));
  }

  /**
   * {@selfdoc}
   */
  public function testGetOptions(): void {
    $expected_options = [
      '<' => new TM('Is earlier than'),
      '<=' => new TM('Is earlier than or equal to'),
      '=' => new TM('Is equal to'),
      '!=' => new TM('Is not equal to'),
      '>=' => new TM('Is later than or equal to'),
      '>' => new TM('Is later than'),
      'empty' => new TM('Is empty (NULL)'),
      'not_empty' => new TM('Is not empty (NOT NULL)'),
    ];
    self::assertEquals($expected_options, QueryOperator::getOptions());
  }

}
