<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\Date;

use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;
use Drupal\date_point\Plugin\Field\FieldWidget\Date\DateWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class WidgetTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testDateWidget(): void {
    $this->createField(
      type: DateItem::ID,
      widget: DateWidget::ID,
      formatter: 'string',
      storage_settings: [],
    );

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "date" and @name = "field_example[0][value]"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $driver = $this->getSession()->getDriver();
    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $driver->evaluateScript('document.querySelector(\'input[name="field_example[0][value]"]\').value = "1987-05-12";');
    $driver->click('//input[@value = "Save"]');
    $this->assertRenderedValue('1987-05-12');
  }

}
