<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\Date;

use Drupal\date_point\Plugin\Field\FieldFormatter\Date\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class CustomFormatterTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testFormatter(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    $this->createField(
      type: DateItem::ID,
      widget: 'string_textfield',
      formatter: 'string',
      storage_settings: [],
    );

    // Configure formatter with default settings.
    $this->openPageDisplayPage();
    $this->selectFormatterType(CustomFormatter::ID);
    $this->assertFormatterSummary('Format: Y-m-d – 2024-01-01');
    $this->submitDisplayForm();

    $node_id = $this->createPage('1834-08-27');

    // Verify formatter output.
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "1834-08-27" and text() = "1834-08-27"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    // Change format and granularity.
    $this->openPageDisplayPage();
    $this->openFormatterSettingsForm();

    $driver->setValue('//label[text() = "Date format"]/following-sibling::input', 'd.m.Y');
    $this->submitFormatterSettingsForm();
    $this->assertWidgetSummary('Format: d.m.Y – 01.01.2024');
    $driver->click('//input[@value = "Save"]');

    // Verify formatter output.
    $this->drupalGet('/node/' . $node_id);
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "1834-08-27" and text() = "27.08.1834"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);
  }

}
