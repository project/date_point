<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\Date;

use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class FieldItemTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testDateItem(): void {
    $this->createField(
      type: DateItem::ID,
      widget: 'string_textfield',
      formatter: 'string',
      storage_settings: [],
    );
    $this->createPage('2012-06-11');
    $this->assertRenderedValue('2012-06-11');
  }

}
