<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\DateTime;

use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\DefaultFormatter;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\TextfieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class DefaultFormatterTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.date')->set('timezone.default', 'Europe/Moscow')->save();
    $clock = $this->container->get('dp_clock_mock.clock.state');
    // The default pattern has been changed in Drupal 11.1, se we replace the
    // pattern to the formatter on older Drupal versions.
    // @see https://www.drupal.org/node/3467774
    $this->config('core.date_format.medium')->set('pattern', 'd.m.Y h:i')->save();
    $clock->set('2024-01-01 15:30:56.123456+03:00');
  }

  /**
   * {@selfdoc}
   */
  public function testFormatter(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    $this->createField(
      type: DateTimeItem::ID,
      widget: TextfieldWidget::ID,
      formatter: DefaultFormatter::ID,
      storage_settings: ['precision' => 'millisecond'],
    );

    // Configure default formatter with default settings.
    $this->openPageDisplayPage();
    $this->assertFormatterSummary('Time zone: Default<br>Format: Default medium date – 01.01.2024 03:30');
    $this->submitDisplayForm();

    // Verify formatter output.
    $node_id = $this->createPage('2024-01-25 12:25:55.456');
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "2024-01-25T09:25:55.456Z" and text() = "25.01.2024 12:25"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    // Change timezone and format.
    $this->openPageDisplayPage();
    $this->openFormatterSettingsForm();
    $xpath = <<< 'XPATH'
      //label[text() = 'Format']
      /following-sibling::select
      /option[text() = "Default medium date – 01.01.2024 03:30"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);
    $driver->selectOption('//label[text() = "Time zone override"]/following-sibling::select', 'Asia/Yekaterinburg');
    $driver->selectOption('//label[text() = "Format"]/following-sibling::select', 'dp_storage_ms');
    $this->submitFormatterSettingsForm();
    $this->assertFormatterSummary('Time zone: Asia/Yekaterinburg<br>Format: Date point storage (with milliseconds) – 2024-01-01 17:30:56.123');
    $this->submitDisplayForm();

    // Verify that formatter output respects timezone override.
    $this->drupalGet('/node/' . $node_id);
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "2024-01-25T09:25:55.456Z" and text() = "2024-01-25 14:25:55.456"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);
  }

}
