<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\DateTime;

use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\PlainFormatter;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\DateTimeLocalWidget;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\TextfieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class WidgetsTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.date')->set('timezone.default', 'Europe/Moscow')->save();
  }

  /**
   * {@selfdoc}
   */
  public function testTextfieldWidget(): void {
    $this->createField(
      type: DateTimeItem::ID,
      widget: TextfieldWidget::ID,
      formatter: PlainFormatter::ID,
      storage_settings: ['precision' => 'millisecond'],
    );

    $node_id = $this->createPage('2005-03-02 14:25:10.456');
    $this->assertRenderedValue('2005-03-02 14:25:10.456');

    $this->drupalGet('/node/' . $node_id . '/edit');
    // Make sure the value is displayed in local timezone.
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "text" and @value = "2005-03-02 14:25:10.456"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);
  }

  /**
   * {@selfdoc}
   */
  public function testDatetimeLocalWidget(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    // -- Default configuration.
    $this->createField(
      type: DateTimeItem::ID,
      widget: DateTimeLocalWidget::ID,
      formatter: PlainFormatter::ID,
      storage_settings: ['precision' => 'millisecond'],
    );
    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "datetime-local" and @name = "field_example[0][value]" and @step = "0.001"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    $this->drupalGet('/node/add/page');
    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $this->setDateLocalValue('2005-03-02 14:25:10.456');
    $driver->click('//input[@value = "Save"]');
    $assert_session->statusMessageContains('Page Test has been created.');
    $this->assertRenderedValue('2005-03-02 14:25:10.456');

    // Make sure the value is displayed in local timezone.
    $this->drupalGet('/node/1/edit');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "datetime-local" and @name = "field_example[0][value]" and @step = "0.001" and @value = "2005-03-02T14:25:10.456"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    // -- Field with seconds.
    $this->updateField(['precision' => 'second']);
    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "datetime-local" and @name = "field_example[0][value]" and @step = "1"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    $this->drupalGet('/node/add/page');
    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $this->setDateLocalValue('2005-03-02 14:25:10');
    $driver->click('//input[@value = "Save"]');
    $assert_session->statusMessageContains('Page Test has been created.');
    $this->assertRenderedValue('2005-03-02 14:25:10');

    // -- Custom step.
    $this->openPageFormDisplayPage();
    $this->assertWidgetSummary('Step: auto');
    $this->openWidgetSettingsForm();
    $driver->setValue('//input[@name = "fields[field_example][settings_edit_form][settings][step]"]', '60');
    $this->submitWidgetSettingsForm();
    $this->assertWidgetSummary('Step: 60');
    $this->submitDisplayForm();

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "datetime-local" and @name = "field_example[0][value]" and @step = "60"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $this->setDateLocalValue('2005-03-02 14:25');
    $driver->click('//input[@value = "Save"]');
    $assert_session->statusMessageContains('Page Test has been created.');
    $this->assertRenderedValue('2005-03-02 14:25:00');
  }

  /**
   * {@selfdoc}
   */
  private function setDateLocalValue(string $value): void {
    // DrupalSelenium2Driver::setValue does not work for datetime-local inputs.
    $this->getSession()->getDriver()->evaluateScript(
      \sprintf('document.querySelector(\'input[name="field_example[0][value]"]\').value = "%s";', $value),
    );
  }

}
