<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\DateTime;

use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\PlainFormatter;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\TextfieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class PlainFormatterTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.date')->set('timezone.default', 'Europe/Moscow')->save();
  }

  /**
   * {@selfdoc}
   */
  public function testFormatter(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    $this->createField(
      type: DateTimeItem::ID,
      widget: TextfieldWidget::ID,
      formatter: PlainFormatter::ID,
      storage_settings: ['precision' => 'millisecond'],
    );

    // -- Configure plain formatter with default settings.
    $this->openPageDisplayPage();
    $this->assertFormatterSummary('Time zone: Default');
    $this->submitDisplayForm();

    // -- Verify formatter output.
    $node_id = $this->createPage('2024-01-25 12:25:55.456');
    $assert_session->elementExists('xpath', '//div[text() = "Example"]/following-sibling::div[text() = "2024-01-25 12:25:55.456"]');

    // -- Change formatter timezone.
    $this->openPageDisplayPage();
    $this->assertFormatterSummary('Time zone: Default');
    $this->openFormatterSettingsForm();
    $driver->selectOption('//label[text() = "Time zone override"]/following-sibling::select', 'Asia/Yekaterinburg');
    $this->submitFormatterSettingsForm();
    $this->assertFormatterSummary('Time zone: Asia/Yekaterinburg');
    $this->submitDisplayForm();

    // -- Verify that formatter output respects timezone override.
    $this->drupalGet('/node/' . $node_id);
    $assert_session->elementExists('xpath', '//div[text() = "Example"]/following-sibling::div[text() = "2024-01-25 14:25:55.456"]');
  }

}
