<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\DateTime;

use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\PlainFormatter;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\TextfieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class CustomFormatterTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('system.date')->set('timezone.default', 'Europe/Moscow')->save();
    $clock = $this->container->get('dp_clock_mock.clock.state');
    $clock->set('2024-01-01 15:30:56.123456+03:00');
  }

  /**
   * {@selfdoc}
   */
  public function testFormatter(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    $this->createField(
      type: DateTimeItem::ID,
      widget: TextfieldWidget::ID,
      formatter: PlainFormatter::ID,
      storage_settings: ['precision' => 'millisecond'],
    );

    // -- Configure default formatter with default settings.
    $this->openPageDisplayPage();
    $this->selectFormatterType(CustomFormatter::ID);
    $this->assertFormatterSummary('Time zone: Default<br>Format: Y-m-d H:i:s – 2024-01-01 15:30:56');
    $this->submitDisplayForm();

    // Verify formatter output.
    $node_id = $this->createPage('2024-01-25 12:25:55.123');
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "2024-01-25T09:25:55.123Z" and text() = "2024-01-25 12:25:55"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    // -- Change timezone and format.
    $this->openPageDisplayPage();
    $this->assertFormatterSummary('Time zone: Default<br>Format: Y-m-d H:i:s – 2024-01-01 15:30:56');
    $this->openFormatterSettingsForm();
    $driver->selectOption('//label[text() = "Time zone override"]/following-sibling::select', 'Asia/Yekaterinburg');
    $driver->setValue('//label[text() = "Date/time format"]/following-sibling::input', 'd.m.Y H:i:s.u');
    $this->submitFormatterSettingsForm();
    $this->assertFormatterSummary('Time zone: Asia/Yekaterinburg<br>Format: d.m.Y H:i:s.u – 01.01.2024 17:30:56.123456');
    $this->submitDisplayForm();

    // -- Verify that formatter output respects timezone override.
    $this->drupalGet('/node/' . $node_id);
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "2024-01-25T09:25:55.123Z" and text() = "25.01.2024 14:25:55.123000"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);
  }

}
