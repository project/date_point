<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\DateTime;

use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\PlainFormatter;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\TextfieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class FieldItemTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testPrecision(): void {
    $this->createField(
      type: DateTimeItem::ID,
      widget: TextfieldWidget::ID,
      formatter: PlainFormatter::ID,
      storage_settings: ['precision' => 'second'],
    );

    // Verify that only seconds were stored.
    $this->createPage('2024-01-25 12:25:55.123456');
    $this->assertRenderedValue('2024-01-25 12:25:55');

    // -- Verify that milliseconds were stored.
    $this->updateField(['precision' => 'millisecond']);
    $this->createPage('2024-01-25 12:25:55.123456');
    $this->assertRenderedValue('2024-01-25 12:25:55.123');

    // -- Verify that microseconds were stored.
    $this->updateField(['precision' => 'microsecond']);
    $this->createPage('2024-01-25 12:25:55.123456');
    $this->assertRenderedValue('2024-01-25 12:25:55.123456');
  }

  /**
   * {@selfdoc}
   */
  public function testBehavior(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();
    $clock = $this->container->get('dp_clock_mock.clock.state');

    $this->createField(
      type: DateTimeItem::ID,
      widget: TextfieldWidget::ID,
      formatter: PlainFormatter::ID,
      storage_settings: ['precision' => 'second'],
      field_settings: ['behavior' => 'manual'],
    );

    // -- Test 'manual' behavior.
    $this->createPage(NULL);
    $assert_session->pageTextNotContains('Example');
    $this->drupalGet('/node/1/edit');
    $assert_session->fieldValueEquals('field_example[0][value]', '');
    $driver->setValue('//input[@name="field_example[0][value]"]', '2024-01-25T12:25:55');
    $driver->click('//input[@value = "Save"]');
    $this->assertRenderedValue('2024-01-25 12:25:55');

    // -- Test 'created' behavior.
    $this->updateField([], ['behavior' => 'created']);
    $clock->set('2024-01-01 15:00:00');
    $node_id = $this->createPage(NULL);
    $this->assertRenderedValue('2024-01-01 15:00:00');
    $clock->set('2024-02-01 20:00:00');
    $this->drupalGet("/node/$node_id/edit");
    $assert_session->elementExists(
      'xpath',
      '//input[@name = "field_example[0][value]" and not(@disabled) and @value = "2024-01-01 15:00:00"]',
    );
    $driver->click('//input[@value = "Save"]');
    // Check that the value has not been updated.
    $this->assertRenderedValue('2024-01-01 15:00:00');

    // -- Test 'updated' behavior.
    $this->updateField([], ['behavior' => 'updated']);
    $clock->set('2024-01-01 15:00:00');
    $node_id = $this->createPage(NULL);
    $this->assertRenderedValue('2024-01-01 15:00:00');
    $clock->set('2024-02-01 20:00:00');
    $this->drupalGet("/node/$node_id/edit");
    $assert_session->elementExists(
      'xpath',
      '//input[@name = "field_example[0][value]" and @disabled = "disabled" and @value = "2024-01-01 15:00:00"]',
    );
    $driver->click('//input[@value = "Save"]');
    // Check that the value has been updated.
    $this->assertRenderedValue('2024-02-01 20:00:00');
  }

}
