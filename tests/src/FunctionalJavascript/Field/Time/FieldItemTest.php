<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\Time;

use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class FieldItemTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testPrecision(): void {
    $db_driver = $this->container->get('database')->driver();
    if ($db_driver === 'sqlite') {
      $this->markTestSkipped('SQLite does not have native time type.');
    }
    $this->createField(
      type: TimeItem::ID,
      widget: 'string_textfield',
      formatter: 'string',
      storage_settings: ['precision' => 'second'],
    );

    // Verify that only seconds were stored.
    $this->createPage('12:25:55.123456');
    $this->assertRenderedValue('12:25:55');

    // -- Verify that milliseconds were stored.
    $this->updateField(['precision' => 'millisecond']);
    $this->createPage('12:25:55.123456');
    $this->assertRenderedValue('12:25:55.123');

    // -- Verify that microseconds were stored.
    $this->updateField(['precision' => 'microsecond']);
    $this->createPage('12:25:55.123456');
    $this->assertRenderedValue('12:25:55.123456');
  }

}
