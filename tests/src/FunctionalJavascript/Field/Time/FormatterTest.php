<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\Time;

use Drupal\date_point\Plugin\Field\FieldFormatter\Time\TimeFormatter;
use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class FormatterTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testFormatter(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    $this->createField(
      type: TimeItem::ID,
      widget: 'string_textfield',
      formatter: 'string',
      storage_settings: ['precision' => 'millisecond'],
    );

    // Configure formatter with default settings.
    $this->openPageDisplayPage();
    $this->selectFormatterType(TimeFormatter::ID);
    $this->assertFormatterSummary('Format: H:i:s – 15:30:56');
    $this->submitDisplayForm();

    $node_id = $this->createPage('12:25:55.123');

    // Verify formatter output.
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "12:25:55.123" and text() = "12:25:55"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    // Change format.
    $this->openPageDisplayPage();
    $this->openFormatterSettingsForm();

    $driver->setValue('//label[text() = "Time format"]/following-sibling::input', 'H:i:s A');
    $this->submitFormatterSettingsForm();
    $this->assertWidgetSummary('Format: H:i:s A – 15:30:56 PM');
    $driver->click('//input[@value = "Save"]');

    // Verify formatter output.
    $this->drupalGet('/node/' . $node_id);
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "12:25:55.123" and text() = "12:25:55 PM"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);
  }

}
