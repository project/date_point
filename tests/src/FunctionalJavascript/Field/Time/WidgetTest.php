<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\Time;

use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem;
use Drupal\date_point\Plugin\Field\FieldWidget\Time\TimeWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class WidgetTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testWidget(): void {
    $db_driver = $this->container->get('database')->driver();
    if ($db_driver === 'sqlite') {
      $this->markTestSkipped('SQLite does not have native time type.');
    }
    $driver = $this->getSession()->getDriver();

    // -- Default configuration.
    $this->createField(
      type: TimeItem::ID,
      widget: 'string_textfield',
      formatter: 'string',
      storage_settings: ['precision' => 'second'],
    );

    $this->openPageFormDisplayPage();
    $this->selectWidgetType(TimeWidget::ID);

    $this->assertWidgetSummary('Step: auto');

    $this->submitDisplayForm();

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "time" and @name = "field_example[0][value]" and @step = "1"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $this->setTimeValue('20:15:55');
    $driver->click('//input[@value = "Save"]');
    $this->assertRenderedValue('20:15:55');

    // -- Field with milliseconds.
    $this->updateField(['precision' => 'millisecond']);

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "time" and @name = "field_example[0][value]" and @step = "0.001"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $this->setTimeValue('20:15:55.123');

    $driver->click('//input[@value = "Save"]');
    $this->assertRenderedValue('20:15:55.123');

    // -- Custom step.
    $this->openPageFormDisplayPage();
    $this->openWidgetSettingsForm();
    $driver->setValue('//input[@name = "fields[field_example][settings_edit_form][settings][step]"]', '60');
    $this->submitWidgetSettingsForm();
    $this->assertWidgetSummary('Step: 60');
    $this->submitDisplayForm();

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "time" and @name = "field_example[0][value]" and @step = "60"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $this->setTimeValue('20:15');

    $driver->click('//input[@value = "Save"]');

    $expected_value = match($db_driver) {
      'pgsql' => '20:15:00',
      default => '20:15:00.000',
    };
    $this->assertRenderedValue($expected_value);
  }

  /**
   * {@selfdoc}
   */
  private function setTimeValue(string $value): void {
    $driver = $this->getSession()->getDriver();
    $driver->evaluateScript(
      \sprintf('document.querySelector(\'input[name="field_example[0][value]"]\').value = "%s";', $value),
    );
  }

}
