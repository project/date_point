<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\YearMonth;

use Drupal\date_point\Plugin\Field\FieldFormatter\YearMonth\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;
use Drupal\date_point\Plugin\Field\FieldWidget\YearMonth\TextFieldWidget;
use Drupal\date_point\Plugin\Field\FieldWidget\YearMonth\YearMonthWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class WidgetTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testTextFieldWidget(): void {
    $this->createField(
      type: YearMonthItem::ID,
      widget: TextFieldWidget::ID,
      formatter: CustomFormatter::ID,
      storage_settings: [],
    );

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "text" and @name = "field_example[0][value]" and @placeholder = "YYYY-MM"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $driver = $this->getSession()->getDriver();
    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $driver->setValue('//input[@name = "field_example[0][value]"]', '1987-05');
    $driver->click('//input[@value = "Save"]');
    $this->assertRenderedValue('May 1987');
  }

  /**
   * {@selfdoc}
   */
  public function testMonthWidget(): void {
    $this->createField(
      type: YearMonthItem::ID,
      widget: YearMonthWidget::ID,
      formatter: CustomFormatter::ID,
      storage_settings: [],
    );

    $this->drupalGet('/node/add/page');
    $xpath = <<< 'XPATH'
      //label[text() = "Example"]
      /following-sibling::input
      [@type = "month" and @name = "field_example[0][value]"]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $driver = $this->getSession()->getDriver();
    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    $driver->setValue('//input[@name = "field_example[0][value]"]', 'May 1987');
    $driver->evaluateScript('document.querySelector(\'input[name="field_example[0][value]"]\').value = "1987-05";');
    $driver->click('//input[@value = "Save"]');
    $this->assertRenderedValue('May 1987');
  }

}
