<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\YearMonth;

use Drupal\date_point\Plugin\Field\FieldFormatter\YearMonth\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;
use Drupal\date_point\Plugin\Field\FieldWidget\YearMonth\TextFieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class CustomFormatterTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testFormatter(): void {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();

    $this->createField(
      type: YearMonthItem::ID,
      widget: TextFieldWidget::ID,
      formatter: 'string',
      storage_settings: [],
    );

    // Configure formatter with default settings.
    $this->openPageDisplayPage();
    $this->selectFormatterType(CustomFormatter::ID);
    $this->assertFormatterSummary('Format: F Y – January 2024');
    $this->submitDisplayForm();

    $node_id = $this->createPage('1964-08');
    // Verify formatter output.
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "1964-08" and text() = "August 1964"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);

    // Change format and granularity.
    $this->openPageDisplayPage();
    $this->openFormatterSettingsForm();

    $driver->setValue('//label[text() = "Month format"]/following-sibling::input', 'm.Y');
    $this->submitFormatterSettingsForm();
    $this->assertWidgetSummary('Format: m.Y – 01.2024');
    $driver->click('//input[@value = "Save"]');

    // Verify formatter output.
    $this->drupalGet('/node/' . $node_id);
    $xpath = <<< 'XPATH'
      //div[text() = "Example"]
      /following-sibling::div
      /time[@datetime = "1964-08" and text() = "08.1964"]
      XPATH;
    $assert_session->elementExists('xpath', $xpath);
  }

}
