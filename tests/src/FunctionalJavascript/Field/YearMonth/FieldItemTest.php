<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Field\YearMonth;

use Drupal\date_point\Plugin\Field\FieldFormatter\YearMonth\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;
use Drupal\date_point\Plugin\Field\FieldWidget\YearMonth\TextFieldWidget;
use Drupal\Tests\date_point\FunctionalJavascript\TestBase;

/**
 * @group date_point
 */
final class FieldItemTest extends TestBase {

  /**
   * {@selfdoc}
   */
  public function testDateItem(): void {
    $this->createField(
      type: YearMonthItem::ID,
      widget: TextFieldWidget::ID,
      formatter: CustomFormatter::ID,
      storage_settings: [],
    );
    $this->createPage('2012-06');
    $this->assertRenderedValue('June 2012');
  }

}
