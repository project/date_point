<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Views;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * @group date_point
 */
final class DateFilterTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'date_point_views_test'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->config('system.date')
      ->set('timezone.default', 'Asia/Vladivostok')
      ->set('timezone.user.configurable', 0)
      ->save();
  }

  /**
   * {@selfdoc}
   */
  public function testDateFilter(): void {
    $this->drupalGet('/admin/date-points/date');
    self::assertSame(505, $this->countFilteredItems());

    // -- OP: "Is equal to".
    $this->setValue('2000-01-04');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('2025-02-11');
    $this->apply();
    $this->assertFilteredItems('2025-02-11');

    // -- OP: "Is not equal to".
    $this->setValue('2024-01-01');
    $this->selectOperator('!=');
    $this->apply();
    self::assertSame(496, $this->countFilteredItems());

    // -- OP: "Is earlier than or equal to".
    $this->setValue('2023-12-31');
    $this->selectOperator('<=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('2024-01-02');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01',
      '2024-01-01',
      '2024-01-01',
      '2024-01-01',
      '2024-01-02',
      '2024-01-02',
      '2024-01-02',
    );

    $this->setValue('2024-01-01');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01',
      '2024-01-01',
      '2024-01-01',
      '2024-01-01',
    );

    // -- OP: "Is earlier than".
    $this->setValue('2023-12-31');
    $this->selectOperator('<');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('2024-01-02');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01',
      '2024-01-01',
      '2024-01-01',
      '2024-01-01',
    );

    // -- OP: "Is later than or equal to".
    $this->setValue('2028-01-01');
    $this->selectOperator('>=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('2026-01-03');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-03',
      '2026-01-03',
      '2026-01-04',
      '2026-01-04',
      '2026-01-05',
    );

    // -- OP: "Is later than".
    $this->setValue('2028-01-01');
    $this->selectOperator('>');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('2026-01-04');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-05',
    );

    // -- OP: "Is empty (NULL)".
    $this->selectOperator('empty');
    $this->apply();
    $this->assertFilteredItems(NULL, NULL, NULL, NULL, NULL);

    // -- OP: "Is not empty (NULL)".
    $this->selectOperator('not_empty');
    $this->apply();
    self::assertSame(500, $this->countFilteredItems());
  }

  /**
   * @phpstan-param '='|'!='|'>'|'<'|'>='|'<='|'empty'|'not_empty' $operator
   */
  private function selectOperator(string $operator): void {
    $this->getSession()->getDriver()->selectOption('//select[@name = "date_op"]', $operator);
  }

  /**
   * {@selfdoc}
   */
  private function setValue(string $value): void {
    $this->getSession()->getDriver()->evaluateScript(
      \sprintf('document.querySelector(\'input[name="date"]\').value = "%s";', $value),
    );
  }

  /**
   * {@selfdoc}
   */
  private function apply(): void {
    $this->click('input[value="Apply"]');
  }

  /**
   * {@selfdoc}
   */
  private function assertFilteredItems(?string ...$expected_records): void {
    $tbody_items = $this->xpath('//div[contains(@class, "view-date-points")]//tbody');
    self::assertCount(1, $tbody_items, 'Views table exists.');
    $filtered_items = \array_map(
      // This must return NULLs for missing items.
      static fn (NodeElement $element): ?string => $element->find('css', 'time')?->getText(),
      $this->xpath('//div[contains(@class, "view-date-points")]//tbody//tr/td[3]'),
    );
    self::assertSame($expected_records, $filtered_items);
  }

  /**
   * self::assertFilteredItems() is very slow for large number of items.
   *
   * @return non-negative-int
   */
  private function countFilteredItems(): int {
    return \count($this->xpath('//div[contains(@class, "view-date-points")]//tbody//tr/td[3]'));
  }

}
