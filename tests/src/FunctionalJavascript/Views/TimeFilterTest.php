<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Views;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * @group date_point
 */
final class TimeFilterTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'date_point_views_test'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->config('system.date')
      ->set('timezone.default', 'Asia/Vladivostok')
      ->set('timezone.user.configurable', 0)
      ->save();
  }

  /**
   * {@selfdoc}
   */
  public function testTimeFilter(): void {
    $this->drupalGet('/admin/date-points/time');
    self::assertSame(505, $this->countFilteredItems());

    // -- OP: "Is equal to".
    $this->setValue('00:00:00');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('00:25:39');
    $this->apply();
    $this->assertFilteredItems('00:25:39');

    // -- OP: "Is not equal to".
    $this->setValue('00:25:39');
    $this->selectOperator('!=');
    $this->apply();
    self::assertSame(499, $this->countFilteredItems());

    // -- OP: "Is earlier than or equal to".
    $this->setValue('00:00:00');
    $this->selectOperator('<=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('00:14:15');
    $this->apply();
    $this->assertFilteredItems(
      '00:02:51',
      '00:05:42',
      '00:08:33',
      '00:11:24',
      '00:14:15',
    );

    $this->setValue('00:14:14');
    $this->apply();
    $this->assertFilteredItems(
      '00:02:51',
      '00:05:42',
      '00:08:33',
      '00:11:24',
    );

    // -- OP: "Is earlier than".
    $this->setValue('00:00:00');
    $this->selectOperator('<');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('00:14:15');
    $this->apply();
    $this->assertFilteredItems(
      '00:02:51',
      '00:05:42',
      '00:08:33',
      '00:11:24',
    );

    // -- OP: "Is later than or equal to".
    $this->setValue('23:36:27');
    $this->selectOperator('>=');
    $this->apply();
    $this->assertFilteredItems(
      '23:36:27',
      '23:39:18',
      '23:42:09',
      '23:45:00',
    );

    $this->setValue('23:36:28');
    $this->apply();
    $this->assertFilteredItems(
      '23:39:18',
      '23:42:09',
      '23:45:00',
    );

    // -- OP: "Is later than".
    $this->setValue('23:59:00');
    $this->selectOperator('>');
    $this->apply();
    $this->assertFilteredItems();

    $this->setValue('23:36:28');
    $this->apply();
    $this->assertFilteredItems(
      '23:39:18',
      '23:42:09',
      '23:45:00',
    );

    // -- OP: "Is empty (NULL)".
    $this->selectOperator('empty');
    $this->apply();
    $this->assertFilteredItems(NULL, NULL, NULL, NULL, NULL);

    // -- OP: "Is not empty (NULL)".
    $this->selectOperator('not_empty');
    $this->apply();
    self::assertSame(500, $this->countFilteredItems());
  }

  /**
   * @phpstan-param '='|'!='|'>'|'<'|'>='|'<='|'empty'|'not_empty' $operator
   */
  private function selectOperator(string $operator): void {
    $this->getSession()->getDriver()->selectOption('//select[@name = "time_op"]', $operator);
  }

  /**
   * {@selfdoc}
   */
  private function setValue(string $value): void {
    $this->getSession()->getDriver()->evaluateScript(
      \sprintf('document.querySelector(\'input[name="time"]\').value = "%s";', $value),
    );
  }

  /**
   * {@selfdoc}
   */
  private function apply(): void {
    $this->click('input[value="Apply"]');
  }

  /**
   * {@selfdoc}
   */
  private function assertFilteredItems(?string ...$expected_records): void {
    $tbody_items = $this->xpath('//div[contains(@class, "view-date-points")]//tbody');
    self::assertCount(1, $tbody_items, 'Views table exists.');
    $filtered_items = \array_map(
      // This must return NULLs for missing items.
      static fn (NodeElement $element): ?string => $element->find('css', 'time')?->getText(),
      $this->xpath('//div[contains(@class, "view-date-points")]//tbody//tr/td[3]'),
    );
    self::assertSame($expected_records, $filtered_items);
  }

  /**
   * self::assertFilteredItems() is very slow for large number of items.
   *
   * @return non-negative-int
   */
  private function countFilteredItems(): int {
    return \count($this->xpath('//div[contains(@class, "view-date-points")]//tbody//tr/td[3]'));
  }

}
