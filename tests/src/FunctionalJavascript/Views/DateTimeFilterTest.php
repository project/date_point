<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript\Views;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * @group date_point
 */
final class DateTimeFilterTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'date_point_views_test'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->config('system.date')
      ->set('timezone.default', 'Asia/Vladivostok')
      ->set('timezone.user.configurable', 0)
      ->save();
  }

  /**
   * {@selfdoc}
   */
  public function testDateTimeWidgetSecondGranularity(): void {
    $this->drupalGet('/admin/date-points/date-time');
    self::assertSame(505, $this->countFilteredItems());

    // -- OP: "Is equal to".
    $this->setDateLocalValue('2000-01-04 10:35:56');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateLocalValue('2025-02-11 14:35:56');
    $this->apply();
    $this->assertFilteredItems('2025-02-11 14:35:56');

    // -- OP: "Is not equal to".
    $this->setDateLocalValue('2000-01-04 10:35:56');
    $this->selectDateTimeOperator('!=');
    $this->apply();
    self::assertSame(500, $this->countFilteredItems());

    // -- OP: "Is earlier than or equal to".
    $this->setDateLocalValue('2023-12-31 23:59:59');
    $this->selectDateTimeOperator('<=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateLocalValue('2024-01-02 10:35:56');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01 18:35:56',
      '2024-01-01 22:35:56',
      '2024-01-02 02:35:56',
      '2024-01-02 06:35:56',
      '2024-01-02 10:35:56',
    );

    $this->setDateLocalValue('2024-01-02 10:35:55');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01 18:35:56',
      '2024-01-01 22:35:56',
      '2024-01-02 02:35:56',
      '2024-01-02 06:35:56',
    );

    // -- OP: "Is earlier than".
    $this->setDateLocalValue('2023-12-31 23:59:59');
    $this->selectDateTimeOperator('<');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateLocalValue('2024-01-02 04:35:55');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01 18:35:56',
      '2024-01-01 22:35:56',
      '2024-01-02 02:35:56',
    );

    // -- OP: "Is later than or equal to".
    $this->setDateLocalValue('2028-01-01 00:00:00');
    $this->selectDateTimeOperator('>=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateLocalValue('2026-01-03 10:35:56');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-03 10:35:56',
      '2026-01-03 22:35:56',
      '2026-01-04 10:35:56',
      '2026-01-05 02:35:56',
      '2026-01-05 18:35:56',
    );

    // -- OP: "Is later than".
    $this->setDateLocalValue('2028-01-01 00:00:00');
    $this->selectDateTimeOperator('>');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateLocalValue('2026-01-04 10:35:56');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-05 02:35:56',
      '2026-01-05 18:35:56',
    );

    // -- OP: "Is empty (NULL)".
    $this->selectDateTimeOperator('empty');
    $this->apply();
    $this->assertFilteredItems(NULL, NULL, NULL, NULL, NULL);

    // -- OP: "Is not empty (NULL)".
    $this->selectDateTimeOperator('not_empty');
    $this->apply();
    self::assertSame(500, $this->countFilteredItems());
  }

  /**
   * {@selfdoc}
   */
  public function testDateWidgetDayGranularity(): void {
    $this->drupalGet('/admin/date-points/date-time');
    self::assertSame(505, $this->countFilteredItems());

    // -- OP: "Is equal to".
    $this->setDateValue('2000-01-04');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateValue('2024-01-02');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-02 02:35:56',
      '2024-01-02 06:35:56',
      '2024-01-02 10:35:56',
      '2024-01-02 18:35:56',
    );

    // -- OP: "Is not equal to".
    $this->setDateValue('2024-01-02');
    $this->selectDateOperator('!=');
    $this->apply();
    self::assertSame(496, $this->countFilteredItems());

    // -- OP: "Is earlier than or equal to".
    $this->setDateValue('2023-12-31');
    $this->selectDateOperator('<=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateValue('2024-01-02');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01 18:35:56',
      '2024-01-01 22:35:56',
      '2024-01-02 02:35:56',
      '2024-01-02 06:35:56',
      '2024-01-02 10:35:56',
      '2024-01-02 18:35:56',
    );

    // -- OP: "Is earlier than".
    $this->setDateValue('2023-12-31');
    $this->selectDateOperator('<');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateValue('2024-01-02');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01 18:35:56',
      '2024-01-01 22:35:56',
    );

    // -- OP: "Is later than or equal to".
    $this->setDateValue('2028-01-01');
    $this->selectDateOperator('>=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateValue('2026-01-03');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-03 02:35:56',
      '2026-01-03 10:35:56',
      '2026-01-03 22:35:56',
      '2026-01-04 10:35:56',
      '2026-01-05 02:35:56',
      '2026-01-05 18:35:56',
    );

    // -- OP: "Is later than".
    $this->setDateValue('2028-01-01');
    $this->selectDateOperator('>');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateValue('2026-01-03');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-04 10:35:56',
      '2026-01-05 02:35:56',
      '2026-01-05 18:35:56',
    );

    // -- OP: "Is empty (NULL)".
    $this->selectDateOperator('empty');
    $this->apply();
    $this->assertFilteredItems(NULL, NULL, NULL, NULL, NULL);

    // -- OP: "Is not empty (NULL)".
    $this->selectDateOperator('not_empty');
    $this->apply();
    self::assertSame(500, $this->countFilteredItems());
  }

  /**
   * {@selfdoc}
   */
  public function testTextfieldWidgetMonthGranularity(): void {
    $this->drupalGet('/admin/date-points/date-time');
    self::assertSame(505, $this->countFilteredItems());

    // -- OP: "Is equal to".
    $this->setTextfieldValue('2000-01');
    $this->apply();
    $this->assertFilteredItems();

    $this->setTextfieldValue('2024-01');
    $this->apply();
    $this->assertFilteredItems(
      '2024-01-01 18:35:56',
      '2024-01-01 22:35:56',
      '2024-01-02 02:35:56',
      '2024-01-02 06:35:56',
      '2024-01-02 10:35:56',
      '2024-01-02 18:35:56',
      '2024-01-03 02:35:56',
      '2024-01-03 10:35:56',
      '2024-01-03 22:35:56',
      '2024-01-04 10:35:56',
      '2024-01-05 02:35:56',
      '2024-01-05 18:35:56',
      '2024-01-06 14:35:56',
      '2024-01-07 14:35:56',
      '2024-01-08 18:35:56',
      '2024-01-10 02:35:56',
      '2024-01-11 14:35:56',
      '2024-01-13 10:35:56',
      '2024-01-15 14:35:56',
      '2024-01-18 02:35:56',
      '2024-01-20 22:35:56',
      '2024-01-24 06:35:56',
      '2024-01-28 02:35:56',
    );

    // -- OP: "Is not equal to".
    $this->setTextfieldValue('2024-01');
    $this->setTextfieldOperator('!=');
    $this->apply();
    self::assertSame(477, $this->countFilteredItems());

    // -- OP: "Is earlier than or equal to".
    $this->setTextfieldValue('2023-12');
    $this->setTextfieldOperator('<=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setTextfieldValue('2024-03');
    $this->apply();
    self::assertSame(63, $this->countFilteredItems());

    // -- OP: "Is earlier than".
    $this->setTextfieldValue('2024-01');
    $this->setTextfieldOperator('<');
    $this->apply();
    $this->assertFilteredItems();

    $this->setTextfieldValue('2024-02');
    $this->apply();
    self::assertSame(23, $this->countFilteredItems());

    // -- OP: "Is later than or equal to".
    $this->setTextfieldValue('2028-01');
    $this->setTextfieldOperator('>=');
    $this->apply();
    $this->assertFilteredItems();

    $this->setTextfieldValue('2026-01');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-02 02:35:56',
      '2026-01-02 06:35:56',
      '2026-01-02 10:35:56',
      '2026-01-02 18:35:56',
      '2026-01-03 02:35:56',
      '2026-01-03 10:35:56',
      '2026-01-03 22:35:56',
      '2026-01-04 10:35:56',
      '2026-01-05 02:35:56',
      '2026-01-05 18:35:56',
    );

    // -- OP: "Is later than".
    $this->setTextfieldValue('2028-01');
    $this->setTextfieldOperator('>');
    $this->apply();
    $this->assertFilteredItems();

    $this->setTextfieldValue('2025-12-03');
    $this->apply();
    $this->assertFilteredItems(
      '2026-01-02 02:35:56',
      '2026-01-02 06:35:56',
      '2026-01-02 10:35:56',
      '2026-01-02 18:35:56',
      '2026-01-03 02:35:56',
      '2026-01-03 10:35:56',
      '2026-01-03 22:35:56',
      '2026-01-04 10:35:56',
      '2026-01-05 02:35:56',
      '2026-01-05 18:35:56',
    );

    // -- OP: "Is empty (NULL)".
    $this->setTextfieldOperator('empty');
    $this->apply();
    $this->assertFilteredItems(NULL, NULL, NULL, NULL, NULL);

    // -- OP: "Is not empty (NULL)".
    $this->setTextfieldOperator('not_empty');
    $this->apply();
    self::assertSame(500, $this->countFilteredItems());

    // -- Make sure that date format does not matter.
    $this->setTextfieldOperator('=');
    $this->setTextfieldValue('2024-10');
    $this->apply();
    self::assertSame(19, $this->countFilteredItems());
    $this->setTextfieldValue('2024-10-15');
    $this->apply();
    self::assertSame(19, $this->countFilteredItems());
    $this->setTextfieldValue('2024-10-15 14:15');
    $this->apply();
    self::assertSame(19, $this->countFilteredItems());
    $this->setTextfieldValue('2024-10-30 14:15:59.123456');
    $this->apply();
    self::assertSame(19, $this->countFilteredItems());

    // -- Make sure wrong date will filter out all items.
    $this->setTextfieldValue('wrong');
    $this->setTextfieldOperator('=');
    $this->apply();
    $this->assertFilteredItems();
  }

  /**
   * {@selfdoc}
   */
  public function testTimezoneOverride(): void {
    $this->drupalGet('/admin/date-points/date-time');
    self::assertSame(505, $this->countFilteredItems());

    $this->setDateLocalUtcValue('2000-01-04 10:35:56');
    $this->apply();
    $this->assertFilteredItems();

    $this->setDateLocalUtcValue('2025-02-11 04:35:56');
    $this->apply();
    $this->assertFilteredItems('2025-02-11 14:35:56');
  }

  /**
   * @phpstan-param '='|'!='|'>'|'<'|'>='|'<='|'empty'|'not_empty' $operator
   */
  private function selectDateTimeOperator(string $operator): void {
    $this->getSession()->getDriver()->selectOption('//select[@name = "w_datetime__g_second_op"]', $operator);
  }

  /**
   * @phpstan-param '='|'!='|'>'|'<'|'>='|'<='|'empty'|'not_empty' $operator
   */
  private function selectDateOperator(string $operator): void {
    $this->getSession()->getDriver()->selectOption('//select[@name = "w_date__g_day_op"]', $operator);
  }

  /**
   * @phpstan-param '='|'!='|'>'|'<'|'>='|'<='|'empty'|'not_empty' $operator
   */
  private function setTextfieldOperator(string $operator): void {
    $this->getSession()->getDriver()->selectOption('//select[@name = "w_textfield__g_month_op"]', $operator);
  }

  /**
   * {@selfdoc}
   */
  private function setDateLocalUtcValue(string $value): void {
    // DrupalSelenium2Driver::setValue does not work for datetime-local inputs.
    $this->getSession()->getDriver()->evaluateScript(
      \sprintf('document.querySelector(\'input[name="w_datetime__g_second__tz_utc"]\').value = "%s";', $value),
    );
  }

  /**
   * {@selfdoc}
   */
  private function setDateLocalValue(string $value): void {
    // DrupalSelenium2Driver::setValue does not work for datetime-local inputs.
    $this->getSession()->getDriver()->evaluateScript(
      \sprintf('document.querySelector(\'input[name="w_datetime__g_second"]\').value = "%s";', $value),
    );
  }

  /**
   * {@selfdoc}
   */
  private function setDateValue(string $value): void {
    $this->getSession()->getDriver()->evaluateScript(
      \sprintf('document.querySelector(\'input[name="w_date__g_day"]\').value = "%s";', $value),
    );
  }

  /**
   * {@selfdoc}
   */
  private function setTextfieldValue(string $value): void {
    $this->getSession()->getDriver()->setValue('//input[@name = "w_textfield__g_month"]', $value);
  }

  /**
   * {@selfdoc}
   */
  private function apply(): void {
    $this->click('input[value="Apply"]');
  }

  /**
   * {@selfdoc}
   */
  private function assertFilteredItems(?string ...$expected_records): void {
    $tbody_items = $this->xpath('//div[contains(@class, "view-date-points")]//tbody');
    self::assertCount(1, $tbody_items, 'Views table exists.');

    $filtered_items = \array_map(
      // This must return NULLs for missing items.
      static fn (NodeElement $element): ?string => $element->find('css', 'time')?->getText(),
      $this->xpath('//div[contains(@class, "view-date-points")]//tbody//tr/td[3]'),
    );
    self::assertSame($expected_records, $filtered_items);
  }

  /**
   * self::assertFilteredItems() is very slow for large number of items.
   *
   * @return non-negative-int
   */
  private function countFilteredItems(): int {
    return \count($this->xpath('//div[contains(@class, "view-date-points")]//tbody//tr/td[3]'));
  }

}
