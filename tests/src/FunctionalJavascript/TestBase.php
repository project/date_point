<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\dp_clock_mock\Clock\StateClock;
use WebDriver\Exception\NoSuchElement;

/**
 * @group date_point
 *
 * @method \Drupal\FunctionalJavascriptTests\WebDriverWebAssert assertSession()
 */
abstract class TestBase extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'node', 'field_ui', 'dp_clock_mock'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'page', 'name' => 'Page']);
    $permissions = [
      'administer content types',
      'administer node fields',
      'create page content',
      'administer node form display',
      'administer node display',
      'delete any page content',
      'edit any page content',
    ];
    $this->drupalLogin(
      // @phpstan-ignore argument.type
      $this->drupalCreateUser($permissions),
    );

    $this->config('system.date')->set('timezone.default', 'UTC')->save();
    $this->config('system.date')->set('timezone.user.configurable', FALSE)->save();
    $clock = \dp_clock_mock('dp_clock_mock.clock.state');
    \assert($clock instanceof StateClock);
    $clock->set('2024-01-01 15:30:56.123456');
  }

  /**
   * @phpstan-param array{precision?: string} $storage_settings
   * @phpstan-param array{behavior?: string} $field_settings
   */
  final protected function createField(string $type, string $widget, string $formatter, array $storage_settings, array $field_settings = []): void {
    // -- Configure field item.
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();
    $this->drupalGet('/admin/structure/types/manage/page/fields/add-field');
    $assert_session->elementExists('xpath', '//h1[text() = "Add field"]');
    $driver->click('//input[@name = "new_storage_type" and @value = "date_time"]');
    $driver->click('//input[@value = "Continue"]');
    $driver->setValue('//input[@name = "label"]', 'Example');
    $driver->setValue('//input[@name = "group_field_options_wrapper"]', $type);
    $driver->click('//input[@value = "Continue"]');
    if (isset($storage_settings['precision']) && $storage_settings['precision'] !== 'second') {
      $driver->selectOption('//fieldset[@id = "edit-field-storage-subform-settings-precision--wrapper"]//input', $storage_settings['precision']);
      $assert_session->assertWaitOnAjaxRequest();
    }
    if ($field_settings['behavior'] ?? NULL) {
      $driver->selectOption('//fieldset[@data-drupal-selector = "edit-settings-behavior"]//input', $field_settings['behavior']);
    }
    $driver->click('//input[@value = "Save settings"]');
    $assert_session->statusMessageContains('Saved Example configuration.');

    // -- Configure widget.
    $this->openPageFormDisplayPage();
    $selected_widget = $driver->getAttribute('//select[@name = "fields[field_example][type]"]/option[@selected]', 'value');
    if ($selected_widget !== $widget) {
      $driver->selectOption('//select[@name = "fields[field_example][type]"]', $widget);
      $assert_session->assertWaitOnAjaxRequest();
    }
    $this->submitDisplayForm();

    // -- Configure formatter.
    $this->openPageDisplayPage();
    $selected_formatter = $driver->getAttribute('//select[@name = "fields[field_example][type]"]/option[@selected]', 'value');
    if ($selected_formatter !== $formatter) {
      $driver->selectOption('//select[@name = "fields[field_example][type]"]', $formatter);
      $assert_session->assertWaitOnAjaxRequest();
    }
    $this->submitDisplayForm();
  }

  /**
   * @phpstan-param array{precision?: string} $storage_settings
   * @phpstan-param array{behavior?: string} $field_settings
   */
  final protected function updateField(array $storage_settings, array $field_settings = []): void {
    $this->deleteAllNodes();
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();
    $this->drupalGet('/admin/structure/types/manage/page/fields/node.page.field_example');
    $assert_session->elementExists('xpath', '//h1[. = "Example settings for Page"]');
    if (isset($storage_settings['precision'])) {
      $driver->selectOption('//fieldset[@id = "edit-field-storage-subform-settings-precision--wrapper"]//input', $storage_settings['precision']);
      $assert_session->assertWaitOnAjaxRequest();
    }

    if ($field_settings['behavior'] ?? NULL) {
      $driver->selectOption('//fieldset[@data-drupal-selector = "edit-settings-behavior"]//input', $field_settings['behavior']);
    }
    $driver->click('//input[@value = "Save settings"]');
    $assert_session->statusMessageContains('Saved Example configuration.');
  }

  /**
   * {@selfdoc}
   */
  final protected function openPageDisplayPage(): void {
    $this->drupalGet('/admin/structure/types/manage/page/display');
  }

  /**
   * {@selfdoc}
   */
  final protected function openPageFormDisplayPage(): void {
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
  }

  /**
   * {@selfdoc}
   */
  final protected function createPage(?string $value): string {
    $driver = $this->getSession()->getDriver();
    $assert_session = $this->assertSession();
    $this->drupalGet('/node/add/page');
    $driver->setValue('//input[@name = "title[0][value]"][1]', 'Test');
    if ($value) {
      $driver->setValue('//input[@name="field_example[0][value]"][1]', $value);
    }
    $driver->click('//input[@value = "Save"]');
    $assert_session->statusMessageContains('Page Test has been created.');
    $url_parts = \explode('/', $this->getSession()->getCurrentUrl());
    return \end($url_parts);
  }

  /**
   * {@selfdoc}
   */
  final protected function deleteAllNodes(): void {
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    $driver = $this->getSession()->getDriver();
    foreach ($node_storage->loadMultiple() as $node) {
      $this->drupalGet(\sprintf('/node/%d/delete', $node->id()));
      $driver->click('//input[@value = "Delete"]');
    }
  }

  /**
   * {@selfdoc}
   */
  final protected function selectWidgetType(string $type): void {
    $this->getSession()->getDriver()->selectOption('//select[@name = "fields[field_example][type]"]', $type);
    $this->assertSession()->assertWaitOnAjaxRequest();
  }

  /**
   * {@selfdoc}
   */
  final protected function selectFormatterType(string $type): void {
    $this->selectWidgetType($type);
  }

  /**
   * {@selfdoc}
   */
  final protected function submitDisplayForm(): void {
    $this->getSession()->getDriver()->click('//input[@value = "Save"]');
    $this->assertSession()->statusMessageContains('Your settings have been saved.');
  }

  /**
   * {@selfdoc}
   */
  final protected function openWidgetSettingsForm(): void {
    $this->getSession()->getDriver()->click('//tr[@id = "field-example"]//input[@name = "field_example_settings_edit"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
  }

  /**
   * {@selfdoc}
   */
  final protected function openFormatterSettingsForm(): void {
    $this->openWidgetSettingsForm();
  }

  /**
   * {@selfdoc}
   */
  final protected function submitWidgetSettingsForm(): void {
    $this->getSession()->getDriver()->click('//input[@name = "field_example_plugin_settings_update"]');
    $this->assertSession()->assertWaitOnAjaxRequest();
  }

  /**
   * {@selfdoc}
   */
  final protected function submitFormatterSettingsForm(): void {
    $this->submitWidgetSettingsForm();
  }

  /**
   * {@selfdoc}
   */
  final protected function assertWidgetSummary(string $expected_summary): void {
    $actual_summary = $this->getSession()
      ->getDriver()
      ->getHtml('//tr[@id = "field-example"]//div[@class = "field-plugin-summary"]');
    self::assertSame($expected_summary, $actual_summary);
  }

  /**
   * {@selfdoc}
   */
  final protected function assertFormatterSummary(string $expected_summary): void {
    $this->assertWidgetSummary($expected_summary);
  }

  /**
   * {@selfdoc}
   */
  final protected function assertRenderedValue(string $expected_value): void {
    try {
      $actual_value = $this->getSession()
        ->getDriver()
        ->getText('//div[text() = "Example"]/following-sibling::div');
    }
    catch (NoSuchElement) {
      self::fail('Could not find formatted value.');
    }
    self::assertSame($expected_value, $actual_value);
  }

}
