<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Clock\SystemClock;
use function Drupal\date_point\clock;
use function Drupal\date_point\now;
use function Drupal\date_point\time;

/**
 * A test for date point helpers.
 *
 * @group date_point
 */
final class DatePointTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testDatePoint(): void {
    self::assertInstanceOf(SystemClock::class, clock());
    // @phpstan-ignore-next-line
    self::assertInstanceOf(\DateTimeImmutable::class, now());
    // Assert that now() points to the current time.
    self::assertContains(\time() - now()->getTimestamp(), [0, 1]);
    // Assert that time() points to the current time.
    self::assertContains(\time() - time(), [0, 1]);
  }

}
