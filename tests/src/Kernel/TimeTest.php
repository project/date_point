<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\dp_clock_mock\Clock\MemoryClock;

/**
 * A test for time service.
 *
 * @group date_point
 */
final class TimeTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $memory_clock = $this->container->get('dp_clock_mock.clock.memory');
    \assert($memory_clock instanceof MemoryClock);
    $memory_clock->set('1970-01-01 01:00:00+00:00');
    $this->container->set('date_point.clock.request', $memory_clock);
    $this->container->set('date_point.clock.system', $memory_clock);
  }

  /**
   * {@selfdoc}
   */
  public function testTime(): void {
    $time = $this->container->get('date_point.time');
    self::assertSame(3_600, $time->getRequestTime());
    self::assertSame(3_600.0, $time->getRequestMicroTime());
    self::assertSame(3_600, $time->getCurrentTime());
    self::assertSame(3_600.0, $time->getCurrentMicroTime());
  }

}
