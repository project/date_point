<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Validation\Constraint;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Data\Precision;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * A test for date time validator.
 *
 * @group date_point
 */
final class DateTimeConstraintValidatorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'entity_test'];

  /**
   * @dataProvider validatorDataProvider
   */
  public function testValidator(?string $input, array $expected_violations, array $constraint_options): void {
    $definition = BaseFieldDefinition::create('string')
      ->addPropertyConstraints('value', ['DateTime' => $constraint_options]);
    $data = $this->container
      ->get('typed_data_manager')
      ->create($definition, 'entity_test');

    $data->setValue($input);
    self::assertSame($expected_violations, self::decodeViolations($data->validate()));
  }

  /**
   * {@selfdoc}
   */
  public static function validatorDataProvider(): \Generator {
    yield ['wrong', ['This value is not a valid datetime.'], []];
    yield ['2023-12-01 09:12:53', [], []];
    yield ['wrong', ['This value is not a valid datetime (a four digit year could not be found).'], ['verbose' => TRUE]];
    yield ['2023-12-01', ['This value is not a valid datetime (not enough data available to satisfy format).'], ['verbose' => TRUE]];
    yield ['2023-12-01T09:12:53', ['This value is not a valid datetime (unexpected data found).'], ['verbose' => TRUE]];
    yield ['2023-12-01 09:12:53', [], []];
    yield ['2023-12-01 09:12:53.123', ['This value is not a valid datetime (trailing data).'], ['verbose' => TRUE]];
    yield ['2023-12-01 09:12:53.123', [], ['verbose' => TRUE, 'precision' => Precision::MILLISECOND]];
    yield ['2023-12-01 09:12:53', [], ['verbose' => TRUE, 'precision' => Precision::MILLISECOND]];
    yield ['2023-12-01 09:12:53.', ['This value is not a valid datetime (not enough data available to satisfy format).'], ['verbose' => TRUE, 'precision' => Precision::MILLISECOND]];
    yield ['2023-12-01 09:12:53', [], ['verbose' => TRUE, 'precision' => Precision::MICROSECOND]];
    yield ['2023-12-01 09:12:53.000', [], ['verbose' => TRUE, 'precision' => Precision::MICROSECOND]];
    yield ['2023-12-01 09:12:53.123456', [], ['verbose' => TRUE, 'precision' => Precision::MICROSECOND]];
    yield ['2023-12-01 09:12:53.123456', ['This value is not a valid datetime (trailing data).'], ['verbose' => TRUE, 'precision' => Precision::MILLISECOND]];
    yield ['2024-01-25 15:00:00', ['This date should be "2024-01-26 15:00:00" or later.'], ['min' => '2024-01-26 15:00:00']];
    yield ['2024-01-25 15:00:00', [], ['min' => '2024-01-25 15:00:00']];
    yield ['2024-01-25 15:00:00', ['This date should be "2024-01-24 15:00:00" or earlier.'], ['max' => '2024-01-24 15:00:00']];
    yield ['2024-01-25 15:00:00.002', ['This date should be "2024-01-25 15:00:00.001" or earlier.'], ['max' => '2024-01-25 15:00:00.001', 'precision' => Precision::MILLISECOND]];
    yield ['2024-01-25 15:30:00', [], ['min' => '2024-01-25 15:00:00', 'max' => '2024-01-25 16:00:00']];
  }

  /**
   * {@selfdoc}
   *
   * @return array<int, string>
   */
  private static function decodeViolations(ConstraintViolationListInterface $violations): array {
    return \array_map(
      static fn (ConstraintViolationInterface $violation): string =>
        \strip_tags((string) $violation->getMessage()),
      \iterator_to_array($violations),
    );
  }

}
