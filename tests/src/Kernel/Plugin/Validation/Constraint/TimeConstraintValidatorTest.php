<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Validation\Constraint;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * A test for time validator.
 *
 * @group date_point
 */
final class TimeConstraintValidatorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'entity_test'];

  /**
   * @dataProvider validatorDataProvider
   */
  public function testValidator(mixed $input, array $expected_violations): void {
    $definition = BaseFieldDefinition::create('string')
      ->addPropertyConstraints('value', ['Time' => []]);
    $data = $this->container
      ->get('typed_data_manager')
      ->create($definition, 'entity_test');

    $data->setValue($input);
    self::assertSame($expected_violations, self::decodeViolations($data->validate()));
  }

  /**
   * {@selfdoc}
   */
  public static function validatorDataProvider(): \Generator {
    // The implementation is tested in a unit test. We just need to ensure
    // it's properly integrated with the constraint.
    // @see \Drupal\Tests\date_point\Unit\TimeTest::testConstructor
    yield [NULL, []];
    yield ['', []];
    yield ['wrong', ['Wrong time value "wrong".']];
    yield ['2023-12-01 09:12:53', ['Wrong time value "2023-12-01 09:12:53".']];
    yield ['12:55', []];
    yield ['12:55:34', []];
    yield ['12:55:34.123', []];
    yield ['12:55:34.123456', []];
    yield ['25:00:00', ['Wrong time value "25:00:00".']];
  }

  /**
   * {@selfdoc}
   *
   * @return array<int, string>
   */
  private static function decodeViolations(ConstraintViolationListInterface $violations): array {
    return \array_map(
      static fn (ConstraintViolationInterface $violation): string =>
        \strip_tags((string) $violation->getMessage()),
      \iterator_to_array($violations),
    );
  }

}
