<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList;
use Drupal\dp_clock_mock\Clock\MemoryClock;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class DateTimeItemListTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testPreSave(): void {
    $clock = $this->container->get('dp_clock_mock.clock.memory');
    \assert($clock instanceof MemoryClock);
    $this->container->set('date_point.clock', $clock);

    // -- 'manual' behavior.
    self::createField('field_example_m', 'millisecond', 'manual');
    $entity = EntityTest::create();
    $entity->save();
    self::assertSame([], $entity->get('field_example_m')->getValue());
    $entity->save();
    self::assertSame([], $entity->get('field_example_m')->getValue());

    // -- 'created' behavior.
    self::createField('field_example_c', 'millisecond', 'created');
    $clock->set('2025-10-04 00:00:00.123+00:00');
    $entity = EntityTest::create();
    $entity->save();
    self::assertSame([['value' => '2025-10-04 00:00:00.123']], $entity->get('field_example_c')->getValue());
    $clock->set('2025-10-04 00:10:00.123+00:00');
    $entity->save();
    // The value should not be changed as the entity was not new.
    self::assertSame([['value' => '2025-10-04 00:00:00.123']], $entity->get('field_example_c')->getValue());

    // -- 'updated' behavior.
    self::createField('field_example_u', 'millisecond', 'updated');
    $entity = EntityTest::create();
    $clock->set('2025-10-04 00:00:00.123+00:00');
    $entity->save();
    self::assertSame([['value' => '2025-10-04 00:00:00.123']], $entity->get('field_example_u')->getValue());
    $clock->set('2025-10-04 00:10:00.123+00:00');
    $entity->save();
    // The value should be changed because of 'updated' behavior.
    self::assertSame([['value' => '2025-10-04 00:10:00.123']], $entity->get('field_example_u')->getValue());

    $clock->reset();
  }

  /**
   * {@selfdoc}
   */
  public function testGetDateTime(): void {
    self::createField('field_example', 'second', 'manual');
    $entity = EntityTest::create();
    $item = $entity->get('field_example');
    \assert($item instanceof DateTimeItemList);
    self::assertNull($item->getDateTime());

    $entity->set('field_example', '2023-03-12 22:15:28');
    $item = $entity->get('field_example');
    \assert($item instanceof DateTimeItemList);
    $date_point = $item->getDateTime();
    self::assertSame('2023-03-12T22:15:28+00:00', $date_point->format(\DateTimeInterface::ATOM));
  }

  /**
   * {@selfdoc}
   */
  public function testSetDateTime(): void {
    self::createField('field_example', 'millisecond', 'manual');
    $item_list = EntityTest::create()->get('field_example');
    \assert($item_list instanceof DateTimeItemList);

    $item_list->setDateTime(new DTI('2021-10-04 23:10:00.123+00:00'));
    self::assertSame('2021-10-04 23:10:00.123', $item_list->value);
  }

  /**
   * {@selfdoc}
   */
  private static function createField(string $field_name, string $precision, string $behavior): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'type' => 'dp_date_time',
      'settings' => ['precision' => $precision],
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'bundle' => 'entity_test',
      'settings' => ['behavior' => $behavior],
    ];
    FieldConfig::create($field_values)->save();
  }

}
