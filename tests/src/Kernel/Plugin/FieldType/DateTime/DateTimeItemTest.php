<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class DateTimeItemTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testDatePointItem(): void {
    self::createField('field_example', 'second', 'manual');

    $entity = EntityTest::create();

    // -- Verify that validation enabled.
    $entity->set('field_example', 'wrong');
    $violations = $entity->validate();
    $this->assertCount(1, $violations);
    $this->assertSame('This value is not a valid datetime.', (string) $violations[0]->getMessage());

    // -- Verify that the field has been created properly.
    $entity->set('field_example', '2023-12-01 00:00:00');
    $this->assertInstanceOf(DateTimeItemList::class, $entity->get('field_example'));
    $this->assertInstanceOf(DateTimeItem::class, $entity->get('field_example')->first());
    $this->assertSame([['value' => '2023-12-01 00:00:00']], $entity->get('field_example')->getValue());
    $entity->save();

    // -- Verify that field value is persisted.
    $persisted_entity = EntityTest::load($entity->id());
    $this->assertSame([['value' => '2023-12-01 00:00:00']], $persisted_entity->get('field_example')->getValue());
  }

  /**
   * {@selfdoc}
   */
  public function testSchema(): void {
    $get_schema = static function (Precision $precision): array {
      $storage_values = [
        'entity_type' => 'entity_test',
        'field_name' => 'test',
        'type' => 'dp_date_time',
        'settings' => ['precision' => $precision->value],
      ];
      return DateTimeItem::schema(FieldStorageConfig::create($storage_values));
    };

    $expected_schema = [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'description' => 'The date time value.',
          'mysql_type' => '<PLACEHOLDER>',
          'pgsql_type' => '<PLACEHOLDER>',
          'length' => '<PLACEHOLDER>',
          'not null' => FALSE,
        ],
      ],
      'indexes' => ['value' => ['value']],
    ];
    $value_column = &$expected_schema['columns']['value'];

    $value_column['mysql_type'] = 'datetime(0)';
    $value_column['pgsql_type'] = 'timestamp(0)';
    $value_column['length'] = 19;
    self::assertSame($expected_schema, $get_schema(Precision::SECOND));

    $value_column['mysql_type'] = 'datetime(3)';
    $value_column['pgsql_type'] = 'timestamp(3)';
    $value_column['length'] = 23;
    self::assertSame($expected_schema, $get_schema(Precision::MILLISECOND));

    $value_column['mysql_type'] = 'datetime(6)';
    $value_column['pgsql_type'] = 'timestamp(6)';
    $value_column['length'] = 26;
    self::assertSame($expected_schema, $get_schema(Precision::MICROSECOND));
  }

  /**
   * {@selfdoc}
   */
  public function testIsEmpty(): void {
    self::createField('field_example', 'second', 'manual');

    $entity = EntityTest::create();
    $field = $entity->get('field_example');

    self::assertTrue($field->isEmpty());

    $field->setValue('');
    self::assertTrue($field->isEmpty());

    $field->setValue('2023-12-01 00:00:00');
    self::assertFalse($field->isEmpty());

    $field->setValue(' ');
    self::assertFalse($field->isEmpty());

    $field->setValue('0');
    self::assertFalse($field->isEmpty());
  }

  /**
   * {@selfdoc}
   */
  public function testApplyDefaultValue(): void {
    $clock = \dp_clock_mock_memory();
    $clock->set('2024-01-01 15:00:00+00:00');

    self::createField('field_example_m', 'second', 'manual');
    self::createField('field_example_c', 'second', 'created');
    self::createField('field_example_u', 'second', 'updated');

    $entity = EntityTest::create();
    self::assertSame([], $entity->get('field_example_m')->getValue());
    self::assertSame([['value' => '2024-01-01 15:00:00']], $entity->get('field_example_c')->getValue());
    self::assertSame([], $entity->get('field_example_u')->getValue());
  }

  /**
   * {@selfdoc}
   */
  public function testGenerateSampleValues(): void {
    foreach (Precision::cases() as $delta => $precision) {
      $field_name = 'field_example_' . $delta;
      self::createField($field_name, $precision->value, 'manual');
      $field_definition = EntityTest::create()->get($field_name)->getFieldDefinition();
      $generated_value = DateTimeItem::generateSampleValue($field_definition)['value'];
      $format = match($precision) {
        Precision::SECOND => 'Y-m-d H:i:s',
        Precision::MILLISECOND => 'Y-m-d H:i:s.v',
        Precision::MICROSECOND => 'Y-m-d H:i:s.u',
      };
      $date = DTI::createFromFormat($format, $generated_value);
      self::assertInstanceOf(DTI::class, $date);
    }
  }

  /**
   * {@selfdoc}
   */
  public function testGetPrecision(): void {
    self::createField('field_example_1', 'second', 'manual');
    self::createField('field_example_2', 'millisecond', 'manual');
    self::createField('field_example_3', 'microsecond', 'manual');

    $values = [
      'field_example_1' => '2023-12-01 00:00:00',
      'field_example_2' => '2023-12-01 00:00:00.000',
      'field_example_3' => '2023-12-01 00:00:00.000000',
    ];
    $entity = EntityTest::create($values);

    // @phpstan-ignore-next-line
    self::assertSame(Precision::SECOND, $entity->get('field_example_1')->first()->getPrecision());
    // @phpstan-ignore-next-line
    self::assertSame(Precision::MILLISECOND, $entity->get('field_example_2')->first()->getPrecision());
    // @phpstan-ignore-next-line
    self::assertSame(Precision::MICROSECOND, $entity->get('field_example_3')->first()->getPrecision());
  }

  /**
   * {@selfdoc}
   */
  public function testGetFormat(): void {
    self::assertSame('Y-m-d H:i:s', DateTimeItem::getFormat(Precision::SECOND));
    self::assertSame('Y-m-d H:i:s.v', DateTimeItem::getFormat(Precision::MILLISECOND));
    self::assertSame('Y-m-d H:i:s.u', DateTimeItem::getFormat(Precision::MICROSECOND));
  }

  /**
   * {@selfdoc}
   */
  public function testGetDateTime(): void {
    self::createField('field_example', 'second', 'manual');
    $entity = EntityTest::create();
    $entity->set('field_example', '2023-05-12 22:15:28');
    $expected_date_point = new DTI('2023-05-12 22:15:28', new \DateTimeZone('UTC'));
    // @phpstan-ignore-next-line
    $actual_date_point = $entity->get('field_example')->first()->getDateTime();
    self::assertEquals($expected_date_point, $actual_date_point);

    $item = $entity->get('field_example')->first();
    $item->setValue(NULL);
    self::expectExceptionObject(new \LogicException('Cannot build DateTime object for empty item.'));
    $item->getDateTime();
  }

  /**
   * {@selfdoc}
   */
  public function testSetDateTime(): void {
    self::createField('field_example', 'second', 'manual');
    $entity = EntityTest::create();
    $date_point = new DTI('2023-05-12 22:15:28', new \DateTimeZone('UTC'));
    // @phpstan-ignore-next-line
    $entity->get('field_example')->appendItem()->setDateTime($date_point);
    self::assertEquals('2023-05-12 22:15:28', $entity->get('field_example')->value);
  }

  /**
   * {@selfdoc}
   */
  public function testGetStorageTimezone(): void {
    self::assertEquals(new \DateTimeZone('UTC'), DateTimeItem::getStorageTimezone());
  }

  /**
   * {@selfdoc}
   */
  private static function createField(string $field_name, string $precision, string $behavior): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'type' => 'dp_date_time',
      'settings' => ['precision' => $precision],
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'bundle' => 'entity_test',
      'settings' => ['behavior' => $behavior],
    ];
    FieldConfig::create($field_values)->save();
  }

}
