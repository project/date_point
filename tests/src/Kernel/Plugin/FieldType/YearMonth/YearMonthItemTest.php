<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\Date;

use DateTimeImmutable as DTI;
use Drupal\date_point\Data\YearMonth;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItemList;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class YearMonthItemTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testItem(): void {
    self::createField();

    $entity = EntityTest::create();

    // -- Verify that validation enabled.
    $entity->set('field_example', 'abc');
    $violations = $entity->validate();
    self::assertCount(1, $violations);
    self::assertSame('Wrong month value "abc".', (string) $violations[0]->getMessage());

    // -- Verify that the field has been created properly.
    $entity->set('field_example', '2023-12');
    $violations = $entity->validate();
    $this->assertCount(0, $violations);
    $this->assertInstanceOf(YearMonthItemList::class, $entity->get('field_example'));
    $this->assertInstanceOf(YearMonthItem::class, $entity->get('field_example')->first());
    $this->assertSame([['value' => '2023-12']], $entity->get('field_example')->getValue());
    $entity->save();

    // -- Verify that field value is persisted.
    $persisted_entity = EntityTest::load($entity->id());
    $this->assertSame([['value' => '2023-12']], $persisted_entity->get('field_example')->getValue());
  }

  /**
   * {@selfdoc}
   */
  public function testSchema(): void {
    $expected_schema = [
      'columns' => [
        'value' => [
          'type' => 'char',
          'description' => 'The month value.',
          'length' => 7,
          'not null' => FALSE,
        ],
      ],
      'indexes' => ['value' => ['value']],
    ];
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'test',
      'type' => 'dp_date',
    ];
    $actual_schema = YearMonthItem::schema(FieldStorageConfig::create($storage_values));
    self::assertSame($expected_schema, $actual_schema);
  }

  /**
   * {@selfdoc}
   */
  public function testGenerateSampleValues(): void {
    self::createField();
    $field_definition = EntityTest::create()->get('field_example')->getFieldDefinition();
    for ($i = 1; $i <= 10; $i++) {
      $generated_value = YearMonthItem::generateSampleValue($field_definition)['value'];
      self::assertMatchesRegularExpression('#^\d{4}-\d{2}$#', $generated_value);
      $date = DTI::createFromFormat('Y-m', $generated_value);
      self::assertInstanceOf(DTI::class, $date);
    }
  }

  /**
   * {@selfdoc}
   */
  public function testIsEmpty(): void {
    self::createField();

    $entity = EntityTest::create();
    $field = $entity->get('field_example');

    self::assertTrue($field->isEmpty());

    $field->setValue('');
    self::assertTrue($field->isEmpty());

    $field->setValue('2012-04');
    self::assertFalse($field->isEmpty());

    $field->setValue(' ');
    self::assertFalse($field->isEmpty());

    $field->setValue('0');
    self::assertFalse($field->isEmpty());
  }

  /**
   * {@selfdoc}
   */
  public function testGetMonth(): void {
    self::createField();
    $entity = EntityTest::create();
    $entity->set('field_example', '1923-05');
    $expected_month = new YearMonth('1923-05');
    // @phpstan-ignore-next-line
    $actual_month = $entity->get('field_example')->first()->getYearMonth();
    self::assertEquals($expected_month, $actual_month);
  }

  /**
   * {@selfdoc}
   */
  public function testSetMonth(): void {
    self::createField();
    $entity = EntityTest::create();
    // @phpstan-ignore-next-line
    $entity->get('field_example')->appendItem()->setYearMonth(new YearMonth('1923-05'));
    self::assertEquals('1923-05', $entity->get('field_example')->value);
  }

  /**
   * {@selfdoc}
   */
  private static function createField(): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'type' => 'dp_year_month',
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'bundle' => 'entity_test',
    ];
    FieldConfig::create($field_values)->save();
  }

}
