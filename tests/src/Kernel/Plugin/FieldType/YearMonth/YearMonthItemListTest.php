<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\Date;

use Drupal\date_point\Data\YearMonth;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItemList;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class YearMonthItemListTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testGetYearMonth(): void {
    self::createField();
    $entity = EntityTest::create();
    // @phpstan-ignore-next-line
    self::assertNull($entity->get('field_example')->getYearMonth());

    $entity->set('field_example', '2003-07');
    // @phpstan-ignore-next-line
    $date_point = $entity->get('field_example')->getYearMonth();
    self::assertSame('2003-07', $date_point->format('Y-m'));
  }

  /**
   * {@selfdoc}
   */
  public function testSetDate(): void {
    self::createField();
    $item_list = EntityTest::create()->get('field_example');
    \assert($item_list instanceof YearMonthItemList);

    $item_list->setYearMonth(new YearMonth('1995-11'));
    self::assertSame('1995-11', $item_list->value);
  }

  /**
   * {@selfdoc}
   */
  private static function createField(): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'type' => 'dp_year_month',
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'bundle' => 'entity_test',
    ];
    FieldConfig::create($field_values)->save();
  }

}
