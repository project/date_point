<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\Time;

use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\date_point\Data\Time;
use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItemList;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class TimeItemListTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testGetTime(): void {
    self::createField('field_example', 'second');
    $entity = EntityTest::create();
    // @phpstan-ignore-next-line
    self::assertNull($entity->get('field_example')->getTime());

    $entity->set('field_example', '15:04:23.123456');
    // @phpstan-ignore-next-line
    $date_point = $entity->get('field_example')->getTime();
    self::assertSame('15:04:23.123456', $date_point->format('H:i:s.u'));
  }

  /**
   * {@selfdoc}
   */
  public function testSetTime(): void {
    self::createField('field_example', 'millisecond');
    $item_list = EntityTest::create()->get('field_example');
    \assert($item_list instanceof TimeItemList);

    $item_list->setTime(new Time('20:12:57.123456'));
    self::assertSame('20:12:57.123', $item_list->value);
  }

  /**
   * {@selfdoc}
   */
  private static function createField(string $field_name, string $precision): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'type' => 'dp_time',
      'settings' => ['precision' => $precision],
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'bundle' => 'entity_test',
    ];
    FieldConfig::create($field_values)->save();
  }

}
