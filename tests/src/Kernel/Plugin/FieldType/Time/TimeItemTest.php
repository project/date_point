<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\Time;

use DateTimeImmutable as DTI;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Data\Time;
use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem;
use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItemList;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class TimeItemTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testTimeItem(): void {
    self::createField('field_example', 'second');

    $entity = EntityTest::create();

    // -- Verify that validation enabled.
    $entity->set('field_example', 'abc');
    $violations = $entity->validate();
    $this->assertCount(1, $violations);
    $this->assertSame('Wrong time value "abc".', (string) $violations[0]->getMessage());

    // -- Verify that the field has been created properly.
    $entity->set('field_example', '15:25:56');
    $this->assertInstanceOf(TimeItemList::class, $entity->get('field_example'));
    $this->assertInstanceOf(TimeItem::class, $entity->get('field_example')->first());
    $this->assertSame([['value' => '15:25:56']], $entity->get('field_example')->getValue());
    $entity->save();

    // -- Verify that field value is persisted.
    $persisted_entity = EntityTest::load($entity->id());
    $this->assertSame([['value' => '15:25:56']], $persisted_entity->get('field_example')->getValue());
  }

  /**
   * {@selfdoc}
   */
  public function testSchema(): void {
    $get_schema = static function (Precision $precision): array {
      $storage_values = [
        'entity_type' => 'entity_test',
        'field_name' => 'test',
        'type' => 'dp_time',
        'settings' => ['precision' => $precision->value],
      ];
      return TimeItem::schema(FieldStorageConfig::create($storage_values));
    };

    $expected_schema = [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'description' => 'The time value.',
          'mysql_type' => '<PLACEHOLDER>',
          'pgsql_type' => '<PLACEHOLDER>',
          'length' => '<PLACEHOLDER>',
          'not null' => FALSE,
        ],
      ],
      'indexes' => ['value' => ['value']],
    ];
    $value_column = &$expected_schema['columns']['value'];

    $value_column['mysql_type'] = 'time(0)';
    $value_column['pgsql_type'] = 'time(0)';
    $value_column['length'] = 8;
    self::assertSame($expected_schema, $get_schema(Precision::SECOND));

    $value_column['mysql_type'] = 'time(3)';
    $value_column['pgsql_type'] = 'time(3)';
    $value_column['length'] = 12;
    self::assertSame($expected_schema, $get_schema(Precision::MILLISECOND));

    $value_column['mysql_type'] = 'time(6)';
    $value_column['pgsql_type'] = 'time(6)';
    $value_column['length'] = 15;
    self::assertSame($expected_schema, $get_schema(Precision::MICROSECOND));
  }

  /**
   * {@selfdoc}
   */
  public function testGenerateSampleValues(): void {
    foreach (Precision::cases() as $delta => $precision) {
      $field_name = 'field_example_' . $delta;
      self::createField($field_name, $precision->value);
      $field_definition = EntityTest::create()->get($field_name)->getFieldDefinition();
      $generated_value = TimeItem::generateSampleValue($field_definition)['value'];
      $format = match($precision) {
        Precision::SECOND => 'H:i:s',
        Precision::MILLISECOND => 'H:i:s.v',
        Precision::MICROSECOND => 'H:i:s.u',
      };
      $date = DTI::createFromFormat($format, $generated_value);
      self::assertInstanceOf(DTI::class, $date);
    }
  }

  /**
   * {@selfdoc}
   */
  public function testIsEmpty(): void {
    self::createField('field_example', 'second');

    $entity = EntityTest::create();
    $field = $entity->get('field_example');

    self::assertTrue($field->isEmpty());

    $field->setValue('');
    self::assertTrue($field->isEmpty());

    $field->setValue('00:00:00');
    self::assertFalse($field->isEmpty());

    $field->setValue(' ');
    self::assertFalse($field->isEmpty());

    $field->setValue('0');
    self::assertFalse($field->isEmpty());
  }

  /**
   * {@selfdoc}
   */
  public function testGetPrecision(): void {
    self::createField('field_example_1', 'second');
    self::createField('field_example_2', 'millisecond');
    self::createField('field_example_3', 'microsecond');

    $values = [
      'field_example_1' => '00:00:00',
      'field_example_2' => '00:00:00.000',
      'field_example_3' => '00:00:00.000000',
    ];
    $entity = EntityTest::create($values);

    // @phpstan-ignore-next-line
    self::assertSame(Precision::SECOND, $entity->get('field_example_1')->first()->getPrecision());
    // @phpstan-ignore-next-line
    self::assertSame(Precision::MILLISECOND, $entity->get('field_example_2')->first()->getPrecision());
    // @phpstan-ignore-next-line
    self::assertSame(Precision::MICROSECOND, $entity->get('field_example_3')->first()->getPrecision());
  }

  /**
   * {@selfdoc}
   */
  public function testGetFormat(): void {
    self::assertSame('H:i:s', TimeItem::buildFormat(Precision::SECOND));
    self::assertSame('H:i:s.v', TimeItem::buildFormat(Precision::MILLISECOND));
    self::assertSame('H:i:s.u', TimeItem::buildFormat(Precision::MICROSECOND));
  }

  /**
   * {@selfdoc}
   */
  public function testGetTime(): void {
    self::createField('field_example', 'second');
    $entity = EntityTest::create();
    $entity->set('field_example', '22:15:28');
    $expected_time = new Time('22:15:28');
    // @phpstan-ignore-next-line
    $actual_time = $entity->get('field_example')->first()->getTime();
    self::assertEquals($expected_time, $actual_time);
  }

  /**
   * {@selfdoc}
   */
  public function testSetTime(): void {
    self::createField('field_example', 'second');
    $entity = EntityTest::create();
    $time = new Time('20:34:51');
    // @phpstan-ignore-next-line
    $entity->get('field_example')->appendItem()->setTime($time);
    self::assertEquals('20:34:51', $entity->get('field_example')->value);
  }

  /**
   * {@selfdoc}
   */
  public function testGetStorageTimezone(): void {
    self::assertEquals(new \DateTimeZone('UTC'), TimeItem::getStorageTimezone());
  }

  /**
   * {@selfdoc}
   */
  private static function createField(string $field_name, string $precision): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'type' => 'dp_time',
      'settings' => ['precision' => $precision],
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => $field_name,
      'bundle' => 'entity_test',
    ];
    FieldConfig::create($field_values)->save();
  }

}
