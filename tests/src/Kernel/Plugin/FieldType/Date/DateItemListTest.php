<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\Date;

use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\date_point\Data\Date;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItemList;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class DateItemListTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testGetDate(): void {
    self::createField();
    $entity = EntityTest::create();
    // @phpstan-ignore-next-line
    self::assertNull($entity->get('field_example')->getDate());

    $entity->set('field_example', '2003-05-29');
    // @phpstan-ignore-next-line
    $date_point = $entity->get('field_example')->getDate();
    self::assertSame('2003-05-29', $date_point->format('Y-m-d'));
  }

  /**
   * {@selfdoc}
   */
  public function testSetDate(): void {
    self::createField();
    $item_list = EntityTest::create()->get('field_example');
    \assert($item_list instanceof DateItemList);

    $item_list->setDate(new Date('1995-12-04'));
    self::assertSame('1995-12-04', $item_list->value);
  }

  /**
   * {@selfdoc}
   */
  private static function createField(): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'type' => 'dp_date',
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'bundle' => 'entity_test',
    ];
    FieldConfig::create($field_values)->save();
  }

}
