<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Plugin\FieldType\Date;

use DateTimeImmutable as DTI;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\date_point\Data\Date;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItemList;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * {@selfdoc}
 */
final class DateItemTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testDateItem(): void {
    self::createField();

    $entity = EntityTest::create();

    // -- Verify that validation enabled.
    $entity->set('field_example', 'abc');
    $violations = $entity->validate();
    $this->assertCount(1, $violations);
    $this->assertSame('Wrong date value "abc".', (string) $violations[0]->getMessage());

    // -- Verify that the field has been created properly.
    $entity->set('field_example', '2023-12-01');
    $this->assertInstanceOf(DateItemList::class, $entity->get('field_example'));
    $this->assertInstanceOf(DateItem::class, $entity->get('field_example')->first());
    $this->assertSame([['value' => '2023-12-01']], $entity->get('field_example')->getValue());
    $entity->save();

    // -- Verify that field value is persisted.
    $persisted_entity = EntityTest::load($entity->id());
    $this->assertSame([['value' => '2023-12-01']], $persisted_entity->get('field_example')->getValue());
  }

  /**
   * {@selfdoc}
   */
  public function testSchema(): void {
    $expected_schema = [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'description' => 'The date value.',
          'mysql_type' => 'date',
          'pgsql_type' => 'date',
          'length' => 10,
          'not null' => FALSE,
        ],
      ],
      'indexes' => ['value' => ['value']],
    ];
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'test',
      'type' => 'dp_date',
    ];
    $actual_schema = DateItem::schema(FieldStorageConfig::create($storage_values));
    self::assertSame($expected_schema, $actual_schema);
  }

  /**
   * {@selfdoc}
   */
  public function testGenerateSampleValues(): void {
    self::createField();
    $field_definition = EntityTest::create()->get('field_example')->getFieldDefinition();
    for ($i = 1; $i <= 10; $i++) {
      $generated_value = DateItem::generateSampleValue($field_definition)['value'];
      self::assertMatchesRegularExpression('#^\d{4}-\d{2}-\d{2}$#', $generated_value);
      $date = DTI::createFromFormat('Y-m-d', $generated_value);
      self::assertInstanceOf(DTI::class, $date);
    }
  }

  /**
   * {@selfdoc}
   */
  public function testIsEmpty(): void {
    self::createField();

    $entity = EntityTest::create();
    $field = $entity->get('field_example');

    self::assertTrue($field->isEmpty());

    $field->setValue('');
    self::assertTrue($field->isEmpty());

    $field->setValue('2012-04-12');
    self::assertFalse($field->isEmpty());

    $field->setValue(' ');
    self::assertFalse($field->isEmpty());

    $field->setValue('0');
    self::assertFalse($field->isEmpty());
  }

  /**
   * {@selfdoc}
   */
  public function testGetDate(): void {
    self::createField();
    $entity = EntityTest::create();
    $entity->set('field_example', '1923-05-12');
    $expected_date = new Date('1923-05-12');
    // @phpstan-ignore-next-line
    $actual_date = $entity->get('field_example')->first()->getDate();
    self::assertEquals($expected_date, $actual_date);
  }

  /**
   * {@selfdoc}
   */
  public function testSetDate(): void {
    self::createField();
    $entity = EntityTest::create();
    // @phpstan-ignore-next-line
    $entity->get('field_example')->appendItem()->setDate(new Date('1923-05-12'));
    self::assertEquals('1923-05-12', $entity->get('field_example')->value);
  }

  /**
   * {@selfdoc}
   */
  public function testGetStorageTimezone(): void {
    self::assertEquals(new \DateTimeZone('UTC'), DateItem::getStorageTimezone());
  }

  /**
   * {@selfdoc}
   */
  private static function createField(): void {
    $storage_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'type' => 'dp_date',
    ];
    FieldStorageConfig::create($storage_values)->save();
    $field_values = [
      'entity_type' => 'entity_test',
      'field_name' => 'field_example',
      'bundle' => 'entity_test',
    ];
    FieldConfig::create($field_values)->save();
  }

}
