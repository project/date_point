<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Command;

use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Command\ClockList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * A test for 'date-point:clock-list' console command.
 *
 * @group date_point
 */
final class ClockListTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testCommand(): void {
    $tester = new CommandTester(new ClockList($this->container));

    $actual_result = $tester->execute([]);
    self::assertSame(Command::SUCCESS, $actual_result);

    $expected_display = <<< 'TXT'
      Drupal Core — Drupal\Component\Datetime\TimeInterface
       ----------------- -------------------------------- ------------------------------- -------------------------------
        ID                Class                            Now                             Request time
       ----------------- -------------------------------- ------------------------------- -------------------------------
        datetime.time     Drupal\Component\Datetime\Time   <datetime>   <datetime>
        date_point.time   Drupal\date_point\Time           <datetime>   <datetime>
       ----------------- -------------------------------- ------------------------------- -------------------------------

      PSR-20 — Psr\Clock\ClockInterface
       -------------------------- -------------------------------------- -------------------------------
        ID                         Class                                  Now
       -------------------------- -------------------------------------- -------------------------------
        date_point.clock.system    Drupal\date_point\Clock\SystemClock    <datetime>
        date_point.clock.request   Drupal\date_point\Clock\RequestClock   <datetime>
        date_point.clock.random    Drupal\date_point\Clock\RandomClock    <datetime>
        date_point.clock           Drupal\date_point\Clock\SystemClock    <datetime>
       -------------------------- -------------------------------------- -------------------------------
      TXT;

    // 1. No way to mock all clock services.
    // 2. Remove trailing spaces from the display.
    $actual_display = \preg_replace(
      ['#\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3}\+\d{2}:\d{2}#', "# *\n#m"],
      ['<datetime>', "\n"],
      \rtrim($tester->getDisplay()),
    );
    self::assertSame($expected_display, $actual_display);
  }

}
