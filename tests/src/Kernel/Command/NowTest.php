<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Command;

use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Command\Now;
use Drupal\dp_clock_mock\Clock\MemoryClock;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * A test for 'date-point:now' console command.
 *
 * @group date_point
 */
final class NowTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * @dataProvider inputProvider
   */
  public function testCommand(array $input, int $expected_result, string $expected_display): void {
    $clock = $this->container->get('dp_clock_mock.clock.memory');
    \assert($clock instanceof MemoryClock);
    $clock->set('2024-03-15 14:35:58.123+00:00');
    $tester = new CommandTester(new Now($clock));

    $actual_result = $tester->execute($input);
    self::assertSame($expected_result, $actual_result);
    self::assertSame($expected_display, \trim($tester->getDisplay()));
  }

  /**
   * {@selfdoc}
   */
  public static function inputProvider(): \Generator {
    yield [[], 0, '2024-03-15 14:35:58.123+00:00'];
    yield [['--format' => 'd.m.Y'], 0, '15.03.2024'];
    yield [['--timezone' => 'Europe/Moscow'], 0, '2024-03-15 17:35:58.123+03:00'];
    yield [['--timezone' => 'wrong'], 1, '[ERROR] Wrong timezone'];
    yield [['--modifier' => '+5 days'], 0, '2024-03-20 14:35:58.123+00:00'];
    yield [['--modifier' => 'wrong'], 1, '[ERROR] Wrong modifier'];
  }

}
