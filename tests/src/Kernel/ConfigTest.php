<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * A test for date formats.
 *
 * @group date_point
 */
final class ConfigTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testClock(): void {
    $this->installConfig(['date_point']);
    $storage = $this->container->get('entity_type.manager')->getStorage('date_format');
    $defaults = ['uuid' => '<VOLATILE>', '_core' => '<VOLATILE>'];

    self::assertSame(
      $defaults + $storage->load('dp_storage')->toArray(),
      [
        'uuid' => '<VOLATILE>',
        '_core' => '<VOLATILE>',
        'langcode' => 'en',
        'status' => TRUE,
        'dependencies' => [],
        'id' => 'dp_storage',
        'label' => 'Date point storage',
        'locked' => TRUE,
        'pattern' => 'Y-m-d H:i:s',
      ],
    );
    self::assertSame(
      $defaults + $storage->load('dp_storage_ms')->toArray(),
      [
        'uuid' => '<VOLATILE>',
        '_core' => '<VOLATILE>',
        'langcode' => 'en',
        'status' => TRUE,
        'dependencies' => [],
        'id' => 'dp_storage_ms',
        'label' => 'Date point storage (with milliseconds)',
        'locked' => TRUE,
        'pattern' => 'Y-m-d H:i:s.v',
      ],
    );
    self::assertSame(
      $defaults + $storage->load('dp_storage_us')->toArray(),
      [
        'uuid' => '<VOLATILE>',
        '_core' => '<VOLATILE>',
        'langcode' => 'en',
        'status' => TRUE,
        'dependencies' => [],
        'id' => 'dp_storage_us',
        'label' => 'Date point storage (with microseconds)',
        'locked' => TRUE,
        'pattern' => 'Y-m-d H:i:s.u',
      ],
    );
  }

}
