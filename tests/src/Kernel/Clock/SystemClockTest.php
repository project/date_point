<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Clock;

use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Clock\SystemClock;
use Psr\Clock\ClockInterface;

/**
 * A test for system clock service.
 *
 * @covers \Drupal\date_point\Clock\SystemClock
 */
final class SystemClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testSystemClock(): void {
    $clock = $this->container->get('date_point.clock.system');
    \assert($clock instanceof ClockInterface);
    \assert($clock instanceof SystemClock);

    $tz = new \DateTimeZone('UTC');
    $expected_timestamp = (new \DateTimeImmutable())->setTimezone($tz)->getTimestamp();
    $actual_timestamp = $clock->now()->setTimezone($tz)->getTimestamp();
    self::assertContains($actual_timestamp - $expected_timestamp, [0, 1]);
  }

}
