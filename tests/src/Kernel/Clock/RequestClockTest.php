<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Clock;

use DateTimeImmutable as DTI;
use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Clock\RequestClock;
use Psr\Clock\ClockInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A test for request clock service.
 *
 * @group date_point
 */
final class RequestClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point'];

  /**
   * {@selfdoc}
   */
  public function testRequestClock(): void {
    // The request stack needs to be preserved as it is used in base class
    // to clear session.
    // @see \Drupal\KernelTests\KernelTestBase::tearDown()
    $original_request_stack = $this->container->get('request_stack');

    // Mock request stack.
    $request_stack = $this->prophesize(RequestStack::class);
    $request = new Request(server: ['REQUEST_TIME_FLOAT' => '123.456789']);
    // @phpstan-ignore-next-line
    $request_stack->getCurrentRequest()->willReturn($request);
    $this->container->set('request_stack', $request_stack->reveal());

    $clock = $this->container->get('date_point.clock.request');
    \assert($clock instanceof ClockInterface);
    \assert($clock instanceof RequestClock);
    // @phpstan-ignore-next-line
    self::assertInstanceOf(DTI::class, $clock->now());
    self::assertSame('123.456789', $clock->now()->format('U.u'));

    // Return original request stack to container.
    $this->container->set('request_stack', $original_request_stack);
  }

}
