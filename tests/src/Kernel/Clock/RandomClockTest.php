<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Clock;

use Drupal\KernelTests\KernelTestBase;
use Drupal\date_point\Clock\RandomClock;
use Psr\Clock\ClockInterface;

/**
 * A test for random clock service.
 *
 * @group date_point
 */
final class RandomClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testRandomClock(): void {
    \dp_clock_mock('dp_clock_mock.clock.millennium');

    $clock = $this->container->get('date_point.clock.random');
    \assert($clock instanceof ClockInterface);
    \assert($clock instanceof RandomClock);

    $min_dti = new \DateTimeImmutable('-1 year');
    $max_dti = new \DateTimeImmutable('+1 year');

    // Using data provider would significantly slow down the test.
    for ($i = 1; $i <= 1_000; $i++) {
      $now = new \DateTimeImmutable();
      self::assertGreaterThanOrEqual($min_dti, $now);
      self::assertLessThanOrEqual($max_dti, $now);
    }
  }

}
