<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\DateTime;

use DateTimeImmutable as DTI;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Validates the date time strings ((RFC 3339)).
 */
final class DateTimeConstraintValidator extends ConstraintValidator {

  /**
   * A length of datetime stored in a base format ('Y-m-d H:i:s).
   */
  private const int SECONDS_FORMAT_LENGTH = 19;

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {
    if (!$constraint instanceof DateTimeConstraint) {
      throw new UnexpectedTypeException($constraint, DateTimeConstraint::class);
    }

    if (!\is_scalar($value) && !$value instanceof \Stringable) {
      throw new UnexpectedValueException($value, 'string');
    }

    $value = (string) $value;

    $this->validateFormat($value, $constraint);
    // There is no point to validate range if date is not correct.
    if (\count($this->context->getViolations()) === 0) {
      $this->validateRange($value, $constraint);
    }
  }

  /**
   * {@selfdoc}
   */
  private function validateFormat(string $value, DateTimeConstraint $constraint): void {

    // Normalize value as Postgres truncates trailing zeros for timestamps.
    if (\strlen($value) === self::SECONDS_FORMAT_LENGTH) {
      $value .= match($constraint->precision) {
        Precision::SECOND => '',
        Precision::MILLISECOND => '.000',
        Precision::MICROSECOND => '.000000',
      };
    }

    DTI::createFromFormat(
      DateTimeItem::getFormat($constraint->precision), $value,
    );

    if (!$result = DTI::getLastErrors()) {
      return;
    }

    if ($constraint->verbose) {
      /** @var non-empty-list<string> $all_errors */
      $all_errors = [...$result['errors'], ... $result['warnings']];
      $error = \mb_strtolower(\rtrim(\reset($all_errors), '.'));
      // Display just one error.
      $this->context->addViolation(
        $constraint->verboseMessage, ['%error' => $error],
      );
    }
    else {
      $this->context->addViolation($constraint->message);
    }
  }

  /**
   * {@selfdoc}
   */
  private function validateRange(string $value, DateTimeConstraint $constraint): void {
    if ($constraint->min && $value < $constraint->min) {
      $this->context->addViolation($constraint->minMessage, ['@min' => $constraint->min]);
    }
    if ($constraint->max && $value > $constraint->max) {
      $this->context->addViolation($constraint->maxMessage, ['@max' => $constraint->max]);
    }
  }

}
