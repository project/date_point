<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\DateTime;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\date_point\Data\Precision;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Provides a constraint for date time.
 */
#[Constraint(
  id: self::ID,
  label: new TM('Date time', options: ['context' => 'Validation'])
)]
final class DateTimeConstraint extends SymfonyConstraint {

  public const string ID = 'DateTime';

  /**
   * {@selfdoc}
   */
  public bool $verbose = FALSE;

  /**
   * {@selfdoc}
   */
  public ?string $min = NULL;

  /**
   * {@selfdoc}
   */
  public ?string $max = NULL;

  /**
   * {@selfdoc}
   */
  public Precision $precision = Precision::SECOND;

  /**
   * {@selfdoc}
   */
  public string $message = 'This value is not a valid datetime.';

  /**
   * {@selfdoc}
   */
  public string $verboseMessage = 'This value is not a valid datetime (%error).';

  /**
   * {@selfdoc}
   */
  public string $minMessage = 'This date should be "@min" or later.';

  /**
   * {@selfdoc}
   */
  public string $maxMessage = 'This date should be "@max" or earlier.';

}
