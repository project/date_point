<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\YearMonth;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Provides a constraint for year-month strings (RFC 3339).
 */
#[Constraint(
  id: self::ID,
  label: new TM('Month', options: ['context' => 'Validation'])
)]
final class YearMonthConstraint extends SymfonyConstraint {

  public const string ID = 'YearMonth';

  /**
   * {@selfdoc}
   */
  public string $message = 'This value is not a valid month.';

}
