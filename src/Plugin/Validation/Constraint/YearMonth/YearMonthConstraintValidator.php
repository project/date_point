<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\YearMonth;

use Drupal\date_point\Data\YearMonth;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Validates the YearMonth constraint.
 */
final class YearMonthConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {
    if (!$constraint instanceof YearMonthConstraint) {
      throw new UnexpectedTypeException($constraint, YearMonthConstraint::class);
    }

    if (!\is_scalar($value) && !$value instanceof \Stringable) {
      throw new UnexpectedValueException($value, 'string');
    }

    $value = (string) $value;

    try {
      new YearMonth($value);
    }
    catch (\DateMalformedStringException $exception) {
      $this->context->addViolation($exception->getMessage());
    }
  }

}
