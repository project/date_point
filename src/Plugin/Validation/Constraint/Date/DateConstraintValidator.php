<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\Date;

use Drupal\date_point\Data\Date;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Validates the date constraint.
 */
final class DateConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {
    if (!$constraint instanceof DateConstraint) {
      throw new UnexpectedTypeException($constraint, DateConstraint::class);
    }

    if (!\is_scalar($value) && !$value instanceof \Stringable) {
      throw new UnexpectedValueException($value, 'string');
    }

    $value = (string) $value;

    try {
      new Date($value);
    }
    catch (\DateMalformedStringException $exception) {
      $this->context->addViolation($exception->getMessage());
    }
  }

}
