<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\Date;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Provides a constraint for date strings (RFC 3339).
 */
#[Constraint(
  id: self::ID,
  label: new TM('Date', options: ['context' => 'Validation'])
)]
final class DateConstraint extends SymfonyConstraint {

  public const string ID = 'Date';

  /**
   * {@selfdoc}
   */
  public string $message = 'This value is not a valid date.';

}
