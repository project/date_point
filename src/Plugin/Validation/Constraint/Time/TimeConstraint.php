<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\Time;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\Validation\Attribute\Constraint;
use Drupal\date_point\Data\Precision;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Provides a constraint for time strings.
 */
#[Constraint(
  id: self::ID,
  label: new TM('Time', options: ['context' => 'Validation'])
)]
final class TimeConstraint extends SymfonyConstraint {

  public const string ID = 'Time';

  /**
   * {@selfdoc}
   */
  public Precision $precision = Precision::SECOND;

  /**
   * {@selfdoc}
   */
  public string $message = 'This value is not a valid time.';

}
