<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Validation\Constraint\Time;

use Drupal\date_point\Data\Time;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Validates the time constraint.
 */
final class TimeConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $value, Constraint $constraint): void {
    if (!$constraint instanceof TimeConstraint) {
      throw new UnexpectedTypeException($constraint, TimeConstraint::class);
    }

    if (!\is_scalar($value) && !$value instanceof \Stringable) {
      throw new UnexpectedValueException($value, 'string');
    }

    $value = (string) $value;

    try {
      new Time($value);
    }
    catch (\DateMalformedStringException $exception) {
      $this->context->addViolation($exception->getMessage());
    }
  }

}
