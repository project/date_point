<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\Time;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem;

/**
 * Plugin implementation of the 'time' widget.
 */
#[FieldWidget(
  id: self::ID,
  label: new TM('Time'),
  field_types: [TimeWidget::ID],
)]
final class TimeWidget extends WidgetBase {

  public const string ID = 'dp_time';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['step' => NULL] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $description = [
      new TM('Set the value to a multiple of 60 to hide seconds.'),
      new TM('Leave empty to apply step based on time precision.'),
    ];
    $element['step'] = [
      '#type' => 'number',
      '#title' => new TM('Step'),
      '#default_value' => $this->getSetting('step'),
      '#description' => \implode('<br/>', $description),
      '#min' => 0,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      new TM('Step: @step', ['@step' => $this->getSetting('step') ?? new TM('auto')]),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\Time\TimeItemList $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $item = $items[$delta];
    \assert($item instanceof TimeItem);

    $element['value'] = $element + [
      '#type' => 'date',
      '#default_value' => $item->value,
      '#attributes' => ['type' => 'time'],
      '#step' => $this->getSetting('step') ?? 10 ** -$item->getPrecision()->toInteger(),
    ];
    return $element;
  }

}
