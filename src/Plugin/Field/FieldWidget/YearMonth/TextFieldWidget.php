<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\YearMonth;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;

/**
 * Plugin implementation of the 'dp_year_month_textfield' widget.
 */
#[FieldWidget(
  id: self::ID,
  label: new TM('Textfield'),
  field_types: [YearMonthItem::ID],
)]
final class TextFieldWidget extends WidgetBase {

  public const string ID = 'dp_year_month_textfield';

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItemList $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'textfield',
      // @phpstan-ignore property.nonObject
      '#default_value' => $items[$delta]->value ?: NULL,
      '#placeholder' => new TM('YYYY-MM'),
    ];
    return $element;
  }

}
