<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\YearMonth;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;

/**
 * Plugin implementation of the 'dp_year_month' widget.
 */
#[FieldWidget(
  id: self::ID,
  label: new TM('Month'),
  field_types: [YearMonthItem::ID],
)]
final class YearMonthWidget extends WidgetBase {

  public const string ID = 'dp_year_month';

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItemList $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'date',
      '#attributes' => ['type' => 'month'],
      // @phpstan-ignore method.nonObject
      '#default_value' => $items[$delta]->getYearMonth()?->format('Y-m'),
    ];
    return $element;
  }

}
