<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\Date;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;

/**
 * Plugin implementation of the 'dp_date' widget.
 */
#[FieldWidget(
  id: self::ID,
  label: new TM('Date'),
  field_types: [DateItem::ID],
)]
final class DateWidget extends WidgetBase {

  public const string ID = 'dp_date';

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\Date\DateItemList $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'date',
      // @phpstan-ignore property.nonObject
      '#default_value' => $items[$delta]->value ?: NULL,
    ];
    return $element;
  }

}
