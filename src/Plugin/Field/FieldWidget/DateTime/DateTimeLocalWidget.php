<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\DateTime;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Data\DateTime\Behavior as Behavior;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Plugin implementation of the 'dp_date_time_local' widget.
 */
#[FieldWidget(
  id: self::ID,
  label: new TM('Date Time Local'),
  field_types: [DateTimeItem::ID],
)]
final class DateTimeLocalWidget extends AbstractWidget {

  public const string ID = 'dp_date_time_local';

  /**
   * {@inheritdoc}
   *
   * @return array{step: int|null}
   */
  public static function defaultSettings(): array {
    // @phpstan-ignore return.type
    return ['step' => NULL] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $description = [
      new TM('Set the value to a multiple of 60 to hide seconds.'),
      new TM('Leave empty to apply step based on time precision.'),
    ];
    $element['step'] = [
      '#type' => 'number',
      '#title' => new TM('Step'),
      '#default_value' => $this->getSetting('step'),
      '#description' => \implode('<br/>', $description),
      '#min' => 0,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      new TM('Step: @step', ['@step' => $this->getSetting('step') ?? new TM('auto')]),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $item = $items[$delta];
    \assert($item instanceof DateTimeItem);

    $value = NULL;
    if (!$item->isEmpty()) {
      // The value in of the input must be presented in default site timezone.
      $format = $item::getFormat($item->getPrecision());
      $value = $item
        ->getDateTime()
        ->setTimezone($this->getTimezone())
        ->format($format);
      $value = \str_replace(' ', 'T', $value);
    }

    $element['value'] = $element + [
      '#type' => 'date',
      '#default_value' => $value,
      '#attributes' => ['type' => 'datetime-local'],
      '#step' => $this->getSetting('step') ?? 10 ** -$item->getPrecision()->toInteger(),
    ];
    // There is no point to edit this value when behavior is set to 'updated'.
    if (Behavior::from($items->getSetting('behavior')) === Behavior::UPDATED) {
      $element['value']['#disabled'] = TRUE;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    foreach ($values as &$value) {
      $value['value'] = \str_replace('T', ' ', $value['value']);
    }
    parent::massageFormValues($values, $form, $form_state);
    return $values;
  }

}
