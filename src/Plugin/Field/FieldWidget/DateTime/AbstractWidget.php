<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Base class for date point widgets.
 */
abstract class AbstractWidget extends WidgetBase {

  /**
   * {@selfdoc}
   */
  final protected function getTimezone(): \DateTimeZone {
    return new \DateTimeZone(\date_default_timezone_get());
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $format = DateTimeItem::getFormat(
      Precision::from($this->getFieldSetting('precision')),
    );

    foreach ($values as &$value) {
      if (!$value['value']) {
        continue;
      }
      try {
        $value['value'] = (new DTI($value['value'], $this->getTimezone()))
          ->setTimezone(DateTimeItem::getStorageTimezone())
          ->format($format);
      }
      catch (\DateException) {
        continue;
      }
    }
    return $values;
  }

}
