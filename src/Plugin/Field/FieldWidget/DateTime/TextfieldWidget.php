<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldWidget\DateTime;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Data\DateTime\Behavior as Behavior;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Plugin implementation of the 'date_point' widget.
 */
#[FieldWidget(
  id: self::ID,
  label: new TM('Textfield'),
  field_types: [DateTimeItem::ID],
)]
final class TextfieldWidget extends AbstractWidget {

  public const string ID = 'dp_date_time_textfield';

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList $items
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $item = $items[$delta];
    \assert($item instanceof DateTimeItem);

    $value = NULL;
    if (!$item->isEmpty()) {
      // The value in of the input must be presented in default site timezone.
      $format = $item::getFormat($item->getPrecision());
      $value = $item
        ->getDateTime()
        ->setTimezone($this->getTimezone())
        ->format($format);
    }

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $value,
    ];
    // There is no point to edit this value when behavior is set to 'updated'.
    if (Behavior::from($items->getSetting('behavior')) === Behavior::UPDATED) {
      $element['value']['#disabled'] = TRUE;
    }

    return $element;
  }

}
