<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\YearMonth;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem;

/**
 * Plugin implementation of the 'Custom' formatter for month fields.
 */
#[FieldFormatter(
  id: self::ID,
  label: new TM('Custom'),
  field_types: [YearMonthItem::ID],
)]
final class CustomFormatter extends FormatterBase {

  use ClockTrait;

  public const string ID = 'dp_year_month_custom';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['format' => 'F Y'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    $form['format'] = [
      '#type' => 'textfield',
      '#title' => new TM('Month format'),
      '#size' => 10,
      '#description' => new TM(
        'See <a href=":url" target="_blank">the documentation for PHP date and time formats</a>.',
        [':url' => 'https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters'],
      ),
      '#default_value' => $this->getSetting('format'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $format = $this->getSetting('format');
    $example = self::getClock()->now()->format($format);
    $summary[] = new TM('Format: @format – @example', ['@format' => $format, '@example' => $example]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItemList $items
   *
   * @return array<array>
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      $month = $item->getYearMonth();
      $elements[$delta] = [
        '#theme' => 'time',
        '#text' => $month->format($this->getSetting('format')),
        '#attributes' => ['datetime' => $month->format('Y-m')],
      ];
    }
    return $elements;
  }

}
