<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\Time;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem;

/**
 * Plugin implementation of the 'Default' formatter for 'datetime' fields.
 */
#[FieldFormatter(
  id: self::ID,
  label: new TM('Time'),
  field_types: [TimeItem::ID],
)]
final class TimeFormatter extends FormatterBase {

  use ClockTrait;

  public const string ID = 'dp_time';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['format' => TimeItem::STORAGE_BASE_FORMAT] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $url = 'https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters';
    $form['format'] = [
      '#type' => 'textfield',
      '#title' => new TM('Time format'),
      '#size' => 10,
      '#description' => new TM('See <a href=":url" target="_blank">the documentation for PHP date and time formats</a>.', [':url' => $url]),
      '#default_value' => $this->getSetting('format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $format = $this->getSetting('format');
    $example = self::getClock()
      ->now()
      ->format($format);
    $summary[] = new TM('Format: @format – @example', ['@format' => $format, '@example' => $example]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\Time\TimeItemList $items
   *
   * @return array<array>
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      $time = $item->getTime();
      $format = $item::buildFormat($item->getPrecision());
      $elements[$delta] = [
        '#theme' => 'time',
        '#text' => $time->format($this->getSetting('format')),
        '#attributes' => ['datetime' => $time->format($format)],
        '#cache' => ['contexts' => ['timezone']],
      ];
    }
    return $elements;
  }

}
