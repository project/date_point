<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\Date;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;

/**
 * Plugin implementation of the 'Date' formatter for 'date' fields.
 *
 * @deprecated Use CustomFormatterTest instead.
 */
#[FieldFormatter(
  id: self::ID,
  label: new TM('Date (deprecated)'),
  field_types: [DateItem::ID],
)]
final class DateFormatter extends FormatterBase {

  use ClockTrait;

  public const string ID = 'dp_date';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['format' => DateItem::STORAGE_FORMAT] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $url = 'https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters';
    $form['format'] = [
      '#type' => 'textfield',
      '#title' => new TM('Date format'),
      '#size' => 10,
      '#description' => new TM('See <a href=":url" target="_blank">the documentation for PHP date and time formats</a>.', [':url' => $url]),
      '#default_value' => $this->getSetting('format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $format = $this->getSetting('format');
    $example = self::getClock()
      ->now()
      ->format($format);
    $summary[] = new TM('Format: @format – @example', ['@format' => $format, '@example' => $example]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\Date\DateItemList $items
   *
   * @return array<array>
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      $date = $item->getDate();
      $elements[$delta] = [
        '#theme' => 'time',
        '#text' => $date->format($this->getSetting('format')),
        '#attributes' => ['datetime' => $date->format($item::STORAGE_FORMAT)],
        '#cache' => ['contexts' => ['timezone']],
      ];
    }
    return $elements;
  }

}
