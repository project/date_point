<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\DateTime;

use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * Base class for date point formatters.
 */
abstract class AbstractFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   *
   * @return array{timezone_override: ''}
   */
  public static function defaultSettings(): array {
    // @see https://github.com/phpstan/phpstan/issues/8438
    // @phpstan-ignore return.type
    return ['timezone_override' => ''] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['timezone_override'] = [
      '#type' => 'select',
      '#title' => new TM('Time zone override'),
      '#description' => new TM('The time zone selected here will always be used'),
      '#options' => TimeZoneFormHelper::getOptionsListByRegion(TRUE),
      '#default_value' => $this->getSetting('timezone_override'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $timezone = $this->getSetting('timezone_override') ?: new TM('Default');
    $summary[] = $this->t('Time zone: @timezone', ['@timezone' => $timezone]);
    return $summary;
  }

  /**
   * {@selfdoc}
   */
  final protected function getTimezone(): \DateTimeZone {
    return new \DateTimeZone(
      $this->getSetting('timezone_override') ?: \date_default_timezone_get(),
    );
  }

}
