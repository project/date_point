<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\DateTime;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Plugin implementation of the 'Custom' formatter for date point field type.
 */
#[FieldFormatter(
  id: self::ID,
  label: new TM('Custom'),
  field_types: [DateTimeItem::ID],
)]
final class CustomFormatter extends AbstractFormatter {

  use ClockTrait;

  public const string ID = 'dp_date_time_custom';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['format' => DateTimeItem::STORAGE_FORMAT_SECONDS] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $url = 'https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters';
    $form['format'] = [
      '#type' => 'textfield',
      '#title' => new TM('Date/time format'),
      '#description' => new TM('See <a href=":url" target="_blank">the documentation for PHP date formats</a>.', [':url' => $url]),
      '#default_value' => $this->getSetting('format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $format = $this->getSetting('format');
    $example = self::getClock()
      ->now()
      ->setTimezone($this->getTimezone())
      ->format($format);
    $summary[] = new TM('Format: @format – @example', ['@format' => $format, '@example' => $example]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList $items
   *
   * @return array<array>
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $current_timezone = $this->getTimezone();
    foreach ($items as $delta => $item) {
      \assert($item instanceof DateTimeItem);
      $utc_date = $item->getDateTime();
      // The datetime must be presented in site timezone.
      $local_date = $utc_date->setTimezone($current_timezone);
      $elements[$delta] = [
        '#theme' => 'time',
        '#text' => $local_date->format($this->getSetting('format')),
        '#attributes' => [
          'datetime' => $utc_date->format('Y-m-d\TH:i:s.v') . 'Z',
        ],
        '#cache' => ['contexts' => ['timezone']],
      ];
    }

    return $elements;
  }

}
