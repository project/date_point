<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\DateTime;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Plugin implementation of the 'Plain' formatter for 'datetime' fields.
 */
#[FieldFormatter(
  id: self::ID,
  label: new TM('Plain'),
  field_types: [DateTimeItem::ID],
)]
final class PlainFormatter extends AbstractFormatter {

  public const string ID = 'dp_date_time_plain';

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList $items
   *
   * @return array<array>
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $site_timezone = $this->getTimezone();
    $elements = [];
    foreach ($items as $delta => $item) {
      \assert($item instanceof DateTimeItem);
      $format = $item::getFormat($item->getPrecision());
      $elements[$delta] = [
        '#markup' => $item->getDateTime()
          // The datetime must be presented in site timezone.
          ->setTimezone($site_timezone)
          ->format($format),
        '#cache' => ['contexts' => ['timezone']],
      ];
    }
    return $elements;
  }

}
