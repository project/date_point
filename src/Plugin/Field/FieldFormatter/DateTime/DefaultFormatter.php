<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldFormatter\DateTime;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use LogicException;

/**
 * Plugin implementation of the 'Default' formatter for 'datetime' fields.
 */
#[FieldFormatter(
  id: self::ID,
  label: new TM('Default'),
  field_types: [DateTimeItem::ID],
)]
final class DefaultFormatter extends AbstractFormatter {

  use ClockTrait;

  public const string ID = 'dp_date_time_default';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return ['format_id' => 'medium'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $datetime = self::getClock()->now()->setTimezone($this->getTimezone());

    $options = [];
    foreach (self::getDateFormats() as $format) {
      $example = $datetime->format($format->getPattern());
      $options[$format->id()] = $format->label() . ' – ' . $example;
    }

    $form['format_id'] = [
      '#type' => 'select',
      '#title' => new TM('Format'),
      '#description' => new TM('Choose a format for displaying the date. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date.'),
      '#options' => $options,
      '#default_value' => $this->getSetting('format_id'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $format = self::getDateFormat($this->getSetting('format_id'));
    $example = self::getClock()
      ->now()
      ->setTimezone($this->getTimezone())
      ->format($format->getPattern());
    $summary[] = new TM('Format: @format – @example', ['@format' => $format->label(), '@example' => $example]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItemList $items
   *
   * @return array<array>
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    $pattern = self::getDateFormat($this->getSetting('format_id'))->getPattern();
    // The datetime must be presented in site timezone.
    $current_timezone = $this->getTimezone();

    foreach ($items as $delta => $item) {
      \assert($item instanceof DateTimeItem);
      $utc_date = $item->getDateTime();
      $local_date = $utc_date->setTimezone($current_timezone);
      $elements[$delta] = [
        '#theme' => 'time',
        '#text' => $local_date->format($pattern),
        '#attributes' => [
          'datetime' => $utc_date->format('Y-m-d\TH:i:s.v\Z'),
        ],
        '#cache' => ['contexts' => ['timezone']],
      ];
    }

    return $elements;
  }

  /**
   * {@selfdoc}
   */
  private static function getDateFormat(string $id): DateFormat {
    $format = \Drupal::entityTypeManager()->getStorage('date_format')->load($id);
    if ($format === NULL) {
      throw new LogicException(\sprintf('Could not load date format "%s".', $id));
    }
    \assert($format instanceof DateFormat);
    return $format;
  }

  /**
   * {@selfdoc}
   */
  private static function getDateFormats(): array {
    return \Drupal::entityTypeManager()->getStorage('date_format')->loadMultiple();
  }

}
