<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\Time;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Data\Time;
use Drupal\date_point\Plugin\Field\FieldFormatter\Time\TimeFormatter;
use Drupal\date_point\Plugin\Field\FieldWidget\Time\TimeWidget;

/**
 * Defines the 'time' entity field type.
 *
 * @property string|null $value
 */
#[FieldType(
  id: self::ID,
  label: new TM('Time (DP)'),
  description: [
    new TM('Time stored using native DB time type.'),
    new TM('Ideal for using time calculations or comparisons.'),
    new TM('Compact and efficient for storage, sorting and calculations.'),
  ],
  category: 'date_time',
  default_widget: TimeWidget::ID,
  default_formatter: TimeFormatter::ID,
  list_class: TimeItemList::class,
)]
final class TimeItem extends FieldItemBase {

  public const string ID = 'dp_time';
  public const string STORAGE_TIMEZONE = 'UTC';
  public const string STORAGE_BASE_FORMAT = 'H:i:s';

  /**
   * @return array{precision: value-of<\Drupal\date_point\Data\Precision>}
   */
  public static function defaultStorageSettings(): array {
    return ['precision' => Precision::SECOND->value];
  }

  /**
   * {@inheritdoc}
   *
   * @return array{precision: array}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $element['precision'] = [
      '#type' => 'radios',
      '#title' => new TM('Precision'),
      '#default_value' => $this->getSetting('precision'),
      '#required' => TRUE,
      '#options' => Precision::getOptions(),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function storageSettingsSummary(FieldStorageDefinitionInterface $storage_definition): array {
    return [
      new TM('Precision: @precision', ['@precision' => $storage_definition->getSetting('precision')]),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @return array{value: \Drupal\Core\TypedData\DataDefinition}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TM('Time value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Symfony\Component\Validator\Constraint>
   */
  public function getConstraints(): array {
    /** @var list<\Symfony\Component\Validator\Constraint> $constraints */
    $constraints = parent::getConstraints();
    $options = [
      'value' => [
        'NotBlank' => [],
        'Time' => [],
      ],
    ];
    $constraints[] = \Drupal::typedDataManager()
      ->getValidationConstraintManager()
      ->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   *
   * @return array{columns: array{value: array}, indexes: array{value: array}}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $precision = Precision::from($field_definition->getSetting('precision'));
    $native_type = \sprintf('time(%d)', $precision->toInteger());
    $date_formatted = (new Time('00:00'))->format(self::buildFormat($precision));
    $columns['value'] = [
      'type' => 'varchar',
      'description' => 'The time value.',
      'mysql_type' => $native_type,
      'pgsql_type' => $native_type,
      'length' => \strlen($date_formatted),
      'not null' => FALSE,
    ];
    return [
      'columns' => $columns,
      'indexes' => ['value' => ['value']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   *
   * @return array{value: string}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $time = Time::fromSeconds(
      \mt_rand(1, 24 * 3_600) - 1 + \mt_rand() / \mt_getrandmax(),
    );
    $format = self::buildFormat(
      Precision::from($field_definition->getSetting('precision')),
    );
    return [
      'value' => $time->format($format),
    ];
  }

  /**
   * {@selfdoc}
   */
  public function getPrecision(): Precision {
    return Precision::from($this->getSetting('precision'));
  }

  /**
   * {@selfdoc}
   */
  public static function buildFormat(Precision $precision): string {
    return match ($precision) {
      Precision::SECOND => self::STORAGE_BASE_FORMAT,
      Precision::MILLISECOND => self::STORAGE_BASE_FORMAT . '.v',
      Precision::MICROSECOND => self::STORAGE_BASE_FORMAT . '.u',
    };
  }

  /**
   * {@selfdoc}
   */
  public function getTime(): ?Time {
    return $this->value ? new Time($this->value) : NULL;
  }

  /**
   * {@selfdoc}
   */
  public function setTime(Time $time): void {
    $format = self::buildFormat($this->getPrecision());
    $this->setValue(['value' => $time->format($format)]);
  }

  /**
   * {@selfdoc}
   */
  public static function getStorageTimezone(): \DateTimeZone {
    return new \DateTimeZone(self::STORAGE_TIMEZONE);
  }

}
