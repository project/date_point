<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\Time;

use Drupal\Core\Field\FieldItemList;
use Drupal\date_point\Data\Time;

/**
 * Defines an item list class for time fields.
 *
 * @extends \Drupal\Core\Field\FieldItemList<\Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem>
 *
 * @method \Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem|null first()
 * @method \Drupal\date_point\Plugin\Field\FieldType\Time\TimeItem|null get()
 */
final class TimeItemList extends FieldItemList {

  /**
   * {@selfdoc}
   */
  public function getTime(): ?Time {
    return $this->first()?->getTime();
  }

  /**
   * {@selfdoc}
   */
  public function setTime(Time $time): void {
    $item = $this->first() ?: $this->appendItem();
    \assert($item instanceof TimeItem);
    $item->setTime($time);
  }

}
