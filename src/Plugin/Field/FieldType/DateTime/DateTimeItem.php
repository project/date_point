<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Data\DateTime\Behavior;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Plugin\Field\FieldFormatter\DateTime\DefaultFormatter;
use Drupal\date_point\Plugin\Field\FieldWidget\DateTime\DateTimeLocalWidget;

use Drupal\date_point\Plugin\Validation\Constraint\DateTime\DateTimeConstraint;

use function sprintf as s;

/**
 * Defines the 'dp_date_time' entity field type.
 *
 * @property string|null $value
 */
#[FieldType(
  id: self::ID,
  label: new TM('Date Time (DP)'),
  description: [
    new TM('Date and time stored in RFC 3339 format using native DB data type.'),
    new TM('Ideal for using date and time calculations or comparisons.'),
    new TM('Compact and efficient for storage, sorting and calculations.'),
  ],
  category: 'date_time',
  default_widget: DateTimeLocalWidget::ID,
  default_formatter: DefaultFormatter::ID,
  list_class: DateTimeItemList::class,
)]
final class DateTimeItem extends FieldItemBase {

  use ClockTrait;

  /**
   * @deprecated Use self::STORAGE_FORMAT_SECONDS
   */
  public const string STORAGE_BASE_FORMAT = 'Y-m-d H:i:s';

  public const string ID = 'dp_date_time';
  public const string STORAGE_TIMEZONE = 'UTC';
  public const string STORAGE_FORMAT_SECONDS = 'Y-m-d H:i:s';
  public const string STORAGE_FORMAT_MILLISECONDS = self::STORAGE_FORMAT_SECONDS . '.v';
  public const string STORAGE_FORMAT_MICROSECONDS = self::STORAGE_FORMAT_SECONDS . '.u';

  /**
   * @return array{precision: value-of<\Drupal\date_point\Data\Precision>}
   */
  public static function defaultStorageSettings(): array {
    return ['precision' => Precision::SECOND->value];
  }

  /**
   * {@inheritdoc}
   *
   * @return array{precision: array}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $element['precision'] = [
      '#type' => 'radios',
      '#title' => new TM('Precision'),
      '#default_value' => $this->getSetting('precision'),
      '#options' => Precision::getOptions(),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function storageSettingsSummary(FieldStorageDefinitionInterface $storage_definition): array {
    $precision = Precision::from($storage_definition->getSetting('precision'));
    return [
      new TM('Precision: @precision', ['@precision' => $precision->getLabel()]),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @return array{behavior: 'manual'}
   */
  public static function defaultFieldSettings(): array {
    return ['behavior' => Behavior::MANUAL->value];
  }

  /**
   * {@inheritdoc}
   *
   * @return array{behavior: array}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element['behavior'] = [
      '#type' => 'radios',
      '#title' => new TM('Behavior'),
      '#default_value' => $this->getSetting('behavior'),
      '#options' => Behavior::getOptions(),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function fieldSettingsSummary(FieldDefinitionInterface $field_definition): array {
    $behavior = Behavior::from($field_definition->getSetting('behavior'));
    return [
      new TM('Behavior: @behavior', ['@behavior' => $behavior->getLabel()]),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @return array{value: \Drupal\Core\TypedData\DataDefinition}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TM('Date time value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Symfony\Component\Validator\Constraint>
   */
  public function getConstraints(): array {
    /** @var list<\Symfony\Component\Validator\Constraint> $constraints */
    $constraints = parent::getConstraints();
    $options = [
      'value' => [
        'NotBlank' => [],
        DateTimeConstraint::ID => ['precision' => $this->getPrecision()],
      ],
    ];
    $constraints[] = \Drupal::typedDataManager()
      ->getValidationConstraintManager()
      ->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   *
   * @return array{columns: array{value: array}, indexes: array{value: array}}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $precision = Precision::from($field_definition->getSetting('precision'));
    $date_formatted = (new DTI())->format(self::getFormat($precision));
    $columns['value'] = [
      'type' => 'varchar',
      'description' => 'The date time value.',
      'mysql_type' => s('datetime(%d)', $precision->toInteger()),
      'pgsql_type' => s('timestamp(%d)', $precision->toInteger()),
      'length' => \strlen($date_formatted),
      'not null' => FALSE,
    ];
    return [
      'columns' => $columns,
      'indexes' => ['value' => ['value']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE): self {
    parent::applyDefaultValue($notify);
    // Unlike 'updated' behavior this value should be set when entity is
    // created rather than when it is saved.
    if ($this->getSetting('behavior') === Behavior::CREATED->value) {
      $this->setDateTime(self::getClock()->now());
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * The implementation is a bit tricky because timestamp with microseconds may
   * exceed PHP_FLOAT_MAX. So we first create a date with only microseconds then
   * modify it with integer timestamp.
   *
   * @return array{value: string}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $microseconds = \mt_rand(0, 1_000_000) / 1_000_000;
    $date = DTI::createFromFormat('U.u', (string) \round($microseconds, 6));
    \assert($date instanceof \DateTimeImmutable);

    $year = 60 * 60 * 24 * 365;
    $timestamp = (new DTI())->getTimestamp() + \mt_rand(-$year, $year);
    $modifier = s('%d seconds', $timestamp);

    $format = self::getFormat(
      Precision::from($field_definition->getSetting('precision')),
    );
    return [
      'value' => $date->modify($modifier)->format($format),
    ];
  }

  /**
   * {@selfdoc}
   */
  public function getPrecision(): Precision {
    return Precision::from($this->getSetting('precision'));
  }

  /**
   * @deprecated Use self::getFormat() instead.
   */
  public static function buildFormat(Precision $precision): string {
    return self::getFormat($precision);
  }

  /**
   * {@selfdoc}
   */
  public static function getFormat(Precision $precision): string {
    return match ($precision) {
      Precision::SECOND => self::STORAGE_FORMAT_SECONDS,
      Precision::MILLISECOND => self::STORAGE_FORMAT_MILLISECONDS,
      Precision::MICROSECOND => self::STORAGE_FORMAT_MICROSECONDS,
    };
  }

  /**
   * {@selfdoc}
   */
  public function getDateTime(): DTI {
    if ($this->isEmpty()) {
      throw new \LogicException('Cannot build DateTime object for empty item.');
    }
    // @phpstan-ignore argument.type
    return new DTI($this->value, self::getStorageTimezone());
  }

  /**
   * {@selfdoc}
   */
  public function setDateTime(DTI $datetime): void {
    $datetime = $datetime->setTimezone(self::getStorageTimezone());
    $format = self::getFormat($this->getPrecision());
    $this->setValue(['value' => $datetime->format($format)]);
  }

  /**
   * {@selfdoc}
   */
  public static function getStorageTimezone(): \DateTimeZone {
    return new \DateTimeZone(self::STORAGE_TIMEZONE);
  }

}
