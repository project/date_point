<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Data\DateTime\Behavior as Behavior;

/**
 * Defines an item list class for date point fields.
 *
 * @extends \Drupal\Core\Field\FieldItemList<\Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem>
 *
 * @method \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem|null first()
 * @method \Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem|null get()
 */
final class DateTimeItemList extends FieldItemList {

  use ClockTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    if ($this->getSetting('behavior') === Behavior::UPDATED->value) {
      $this->setDateTime(self::getClock()->now());
    }
    parent::presave();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state): array {
    // Default values are configure through item behavior.
    return [];
  }

  /**
   * {@selfdoc}
   */
  public function getDateTime(): ?DTI {
    return $this->first()?->getDateTime();
  }

  /**
   * {@selfdoc}
   */
  public function setDateTime(DTI $datetime): void {
    $item = $this->first() ?: $this->appendItem();
    \assert($item instanceof DateTimeItem);
    $item->setDateTime($datetime);
  }

}
