<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\YearMonth;

use Drupal\Core\Field\FieldItemList;
use Drupal\date_point\Data\YearMonth;

/**
 * Defines an item list class for month fields.
 *
 * @extends \Drupal\Core\Field\FieldItemList<\Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem>
 *
 * @method \Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem|null first()
 * @method \Drupal\date_point\Plugin\Field\FieldType\YearMonth\YearMonthItem|null get()
 */
final class YearMonthItemList extends FieldItemList {

  /**
   * {@selfdoc}
   */
  public function getYearMonth(): ?YearMonth {
    return $this->first()?->getYearMonth();
  }

  /**
   * {@selfdoc}
   */
  public function setYearMonth(YearMonth $month): void {
    $item = $this->first() ?: $this->appendItem();
    \assert($item instanceof YearMonthItem);
    $item->setYearMonth($month);
  }

}
