<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\YearMonth;

use DateTimeImmutable as DTI;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Data\YearMonth;
use Drupal\date_point\Plugin\Field\FieldFormatter\YearMonth\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldWidget\YearMonth\YearMonthWidget;
use Drupal\date_point\Plugin\Validation\Constraint\YearMonth\YearMonthConstraint;

/**
 * Defines the 'dp_year_month' entity field type.
 *
 * @property string|null $value
 */
#[FieldType(
  id: self::ID,
  label: new TM('Month (DP)'),
  description: [new TM('For storing month with year.')],
  category: 'date_time',
  default_widget: YearMonthWidget::ID,
  default_formatter: CustomFormatter::ID,
  list_class: YearMonthItemList::class,
)]
final class YearMonthItem extends FieldItemBase {

  use ClockTrait;

  public const string ID = 'dp_year_month';
  public const string STORAGE_FORMAT = 'Y-m';

  /**
   * {@inheritdoc}
   *
   * @return array{value: \Drupal\Core\TypedData\DataDefinition}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TM('Month value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Symfony\Component\Validator\Constraint>
   */
  public function getConstraints(): array {
    /** @var list<\Symfony\Component\Validator\Constraint> $constraints */
    $constraints = parent::getConstraints();
    $options = [
      'value' => [
        'NotBlank' => [],
        YearMonthConstraint::ID => [],
      ],
    ];
    $constraints[] = \Drupal::typedDataManager()
      ->getValidationConstraintManager()
      ->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   *
   * @return array{columns: array{value: array}, indexes: array{value: array}}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $columns['value'] = [
      'type' => 'char',
      'description' => 'The month value.',
      'length' => 7,
      'not null' => FALSE,
    ];
    return [
      'columns' => $columns,
      'indexes' => ['value' => ['value']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   *
   * @return array{value: string}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $days = \mt_rand(-240, 48);
    $date = YearMonth::fromDateTime(new DTI(\sprintf('%d months', $days)));
    return ['value' => $date->format(self::STORAGE_FORMAT)];
  }

  /**
   * {@selfdoc}
   */
  public function getYearMonth(): ?YearMonth {
    return $this->value ? new YearMonth($this->value) : NULL;
  }

  /**
   * {@selfdoc}
   */
  public function setYearMonth(YearMonth $month): void {
    $this->setValue(['value' => $month->format(self::STORAGE_FORMAT)]);
  }

}
