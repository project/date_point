<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\Date;

use DateTimeImmutable as DTI;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\date_point\Clock\ClockTrait;
use Drupal\date_point\Data\Date;
use Drupal\date_point\Plugin\Field\FieldFormatter\Date\CustomFormatter;
use Drupal\date_point\Plugin\Field\FieldWidget\Date\DateWidget;

/**
 * Defines the 'dp_date' entity field type.
 *
 * @property string|null $value
 */
#[FieldType(
  id: self::ID,
  label: new TM('Date (DP)'),
  description: [
    new TM('Date stored using native DB date type.'),
    new TM('Ideal for using time calculations or comparisons.'),
    new TM('Compact and efficient for storage, sorting and calculations.'),
  ],
  category: 'date_time',
  default_widget: DateWidget::ID,
  default_formatter: CustomFormatter::ID,
  list_class: DateItemList::class,
)]
final class DateItem extends FieldItemBase {

  use ClockTrait;

  public const string ID = 'dp_date';
  public const string STORAGE_TIMEZONE = 'UTC';
  public const string STORAGE_FORMAT = 'Y-m-d';

  /**
   * {@inheritdoc}
   *
   * @return array{value: \Drupal\Core\TypedData\DataDefinition}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TM('Date value'))
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   *
   * @return non-empty-list<\Symfony\Component\Validator\Constraint>
   */
  public function getConstraints(): array {
    /** @var list<\Symfony\Component\Validator\Constraint> $constraints */
    $constraints = parent::getConstraints();
    $options = [
      'value' => [
        'NotBlank' => [],
        'Date' => [],
      ],
    ];
    $constraints[] = \Drupal::typedDataManager()
      ->getValidationConstraintManager()
      ->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   *
   * @return array{columns: array{value: array}, indexes: array{value: array}}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $columns['value'] = [
      'type' => 'varchar',
      'description' => 'The date value.',
      'mysql_type' => 'date',
      'pgsql_type' => 'date',
      'length' => 10,
      'not null' => FALSE,
    ];
    return [
      'columns' => $columns,
      'indexes' => ['value' => ['value']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   *
   * @return array{value: string}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $days = \mt_rand(-365, 365);
    $date = Date::fromDateTime(new DTI(\sprintf('%d days', $days)));
    return ['value' => $date->format(self::STORAGE_FORMAT)];
  }

  /**
   * {@selfdoc}
   */
  public function getDate(): ?Date {
    return $this->value ? new Date($this->value) : NULL;
  }

  /**
   * {@selfdoc}
   */
  public function setDate(Date $date): void {
    $this->setValue(['value' => $date->format(self::STORAGE_FORMAT)]);
  }

  /**
   * {@selfdoc}
   */
  public static function getStorageTimezone(): \DateTimeZone {
    return new \DateTimeZone(self::STORAGE_TIMEZONE);
  }

}
