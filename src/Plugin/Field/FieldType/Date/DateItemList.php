<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\Field\FieldType\Date;

use Drupal\Core\Field\FieldItemList;
use Drupal\date_point\Data\Date;

/**
 * Defines an item list class for date fields.
 *
 * @extends \Drupal\Core\Field\FieldItemList<\Drupal\date_point\Plugin\Field\FieldType\Date\DateItem>
 *
 * @method \Drupal\date_point\Plugin\Field\FieldType\Date\DateItem|null first()
 * @method \Drupal\date_point\Plugin\Field\FieldType\Date\DateItem|null get()
 */
final class DateItemList extends FieldItemList {

  /**
   * {@selfdoc}
   */
  public function getDate(): ?Date {
    return $this->first()?->getDate();
  }

  /**
   * {@selfdoc}
   */
  public function setDate(Date $date): void {
    $item = $this->first() ?: $this->appendItem();
    \assert($item instanceof DateItem);
    $item->setDate($date);
  }

}
