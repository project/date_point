<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\views\argument;

use DateTimeImmutable as DTI;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Data\DateTime\Granularity;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\views\Attribute\ViewsArgument;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;

/**
 * Argument handler for datetime fields.
 *
 * @property \Drupal\views\Plugin\views\query\Sql $query
 *
 * @ingroup views_argument_handlers
 */
#[ViewsArgument(self::ID)]
final class DateTime extends ArgumentPluginBase {

  public const string ID = 'dp_date_time';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['dp_granularity'] = ['default' => Granularity::SECOND->value];
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array $form
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $form['dp_granularity'] = [
      '#type' => 'select',
      '#title' => new TM('Granularity'),
      '#options' => Granularity::getOptions(),
      '#default_value' => $this->options['dp_granularity'],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param bool $group_by
   */
  public function query($group_by = FALSE): void {
    $this->ensureMyTable();
    $value = self::buildValue($this->argument);
    if (!$value) {
      // As there no validation for user input we just filter out all records
      // when the supplied date is not valid.
      $this->query->addWhereExpression('0', '1 = 2');
      return;
    }
    $this->query->addWhere('0', "$this->tableAlias.$this->realField", $value);
  }

  /**
   * {@selfdoc}
   */
  public function buildValue(string $input): ?string {
    try {
      // Input time is supposed to be in default site timezone.
      $datetime = (new DTI($input))->setTimezone(DateTimeItem::getStorageTimezone());
    }
    catch (\DateMalformedStringException) {
      return NULL;
    }
    $precision = match ($this->getGranularity()) {
      Granularity::MICROSECOND, Granularity::MILLISECOND => Precision::MICROSECOND,
      default => Precision::SECOND,
    };
    return $datetime->format(DateTimeItem::getFormat($precision));
  }

  /**
   * {@selfdoc}
   */
  private function getGranularity(): Granularity {
    if (!isset($this->options['dp_granularity'])) {
      throw new \LogicException('Granularity option is not set.');
    }
    return Granularity::from($this->options['dp_granularity']);
  }

}
