<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\views\filter;

use DateTimeImmutable as DTI;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Data\DateTime\Granularity;
use Drupal\date_point\Data\Precision;
use Drupal\date_point\Data\Views\QueryOperator;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;
use Drupal\views\Attribute\ViewsFilter;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use function sprintf as s;

/**
 * Filter to handle dates points.
 *
 * @property \Drupal\views\Plugin\views\query\Sql $query
 * @property array{
 *   dp_granularity: value-of<\Drupal\date_point\Data\DateTime\Granularity>,
 *   dp_widget: string,
 *   dp_timezone_override: string,
 *   expose: array,
 *   exposed: bool,
 *   group: string,
 * } $options
 */
#[ViewsFilter(self::ID)]
final class DateTime extends FilterPluginBase {

  public const string ID = 'dp_date_time';

  /**
   * {@inheritdoc}
   */
  protected $alwaysMultiple = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['dp_widget'] = ['default' => 'datetime-local'];
    $options['dp_granularity'] = ['default' => Granularity::SECOND->value];
    $options['dp_timezone_override'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function operatorOptions(): array {
    $options = QueryOperator::getOptions();
    if (empty($this->definition['allow empty'])) {
      unset($options['empty'], $options['not_empty']);
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form['dp_widget'] = [
      '#type' => 'select',
      '#title' => new TM('Widget'),
      '#options' => [
        'date' => new TM('Date'),
        'datetime-local' => new TM('Datetime'),
        'textfield' => new TM('Text field'),
      ],
      '#default_value' => $this->options['dp_widget'],
    ];

    $form['dp_granularity'] = [
      '#type' => 'select',
      '#title' => new TM('Granularity'),
      '#options' => Granularity::getOptions(),
      '#default_value' => $this->options['dp_granularity'],
    ];

    $form['dp_timezone_override'] = [
      '#type' => 'select',
      '#title' => new TM('Time zone override'),
      '#description' => new TM('Override input time zone if date points are rendered in non-default timezone.'),
      '#options' => TimeZoneFormHelper::getOptionsListByRegion(TRUE),
      '#default_value' => $this->options['dp_timezone_override'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state): void {

    $form['value'] = [
      '#type' => 'date',
      '#title' => new TM('Value'),
      '#attributes' => ['type' => $this->options['dp_widget']],
      '#default_value' => $this->value,
    ];

    if ($this->options['dp_widget'] === 'datetime-local') {
      $form['value']['#step'] = match ($this->getGranularity()) {
        Granularity::MICROSECOND => 0.000_001,
        Granularity::MILLISECOND => 0.001,
        Granularity::SECOND => 1,
        Granularity::MINUTE => 60,
        Granularity::HOUR => 3_600,
        default => 24 * 3_600,
      };
    }

    if ($form_state->get('exposed')) {
      $identifier = $this->options['expose']['identifier'];
      $user_input = $form_state->getUserInput();
      if (!isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value;
        $form_state->setUserInput($user_input);
      }
    }

    $form['value']['#states']['invisible'][] = [':input[name="options[operator]"]' => ['value' => 'empty']];
    $form['value']['#states']['invisible'][] = [':input[name="options[operator]"]' => ['value' => 'not_empty']];
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary(): TM|string {
    if ($this->isAGroup()) {
      return new TM('grouped');
    }
    elseif ($this->options['exposed']) {
      return new TM('exposed');
    }
    return parent::adminSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function validateExposeForm($form, FormStateInterface $form_state): void {
    parent::validate();
    $input = $form_state->getValue('options')['value'] ?? NULL;
    if ($input && !self::buildValue($input)) {
      $form_state->setError($form['value'], (string) new TM('Wrong date value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";

    switch ($this->operator) {
      case QueryOperator::EMPTY->value:
        $this->query->addWhere($this->options['group'], $field, NULL, 'IS NULL');
        break;

      case QueryOperator::NOT_EMPTY->value:
        $this->query->addWhere($this->options['group'], $field, NULL, 'IS NOT NULL');
        break;

      default:
        $value = $this->buildDateTime($this->value);
        if (!$value) {
          // As there no validation for user input we just filter out all
          // records when the supplied date is not valid.
          $this->query->addWhereExpression($this->options['group'], '1 = 2');
          return;
        }

        [$start, $end] = $this->getGranularity()->getBoundaries($value);
        $expression = match($this->operator) {
          '<=' => s("%s < '%s'", $field, $end),
          '<' => s("%s < '%s'", $field, $start),
          '=' => s("'%s' <= %s AND %s < '%s'", $start, $field, $field, $end),
          '!=' => s("%s < '%s' OR '%s' <= %s", $field, $start, $end, $field),
          '>=' => s("%s >= '%s'", $field, $start),
          '>' => s("%s >= '%s'", $field, $end),
          default => throw new \InvalidArgumentException('Unknown operator'),
        };
        $this->query->addWhereExpression($this->options['group'], $expression);
    }
  }

  /**
   * {@selfdoc}
   */
  private function buildDateTime(string $input): ?DTI {
    $override = $this->options['dp_timezone_override'];
    $tz = $override ? new \DateTimeZone($override) : NULL;
    try {
      return new DTI($input, $tz);
    }
    catch (\DateMalformedStringException) {
      return NULL;
    }
  }

  /**
   * {@selfdoc}
   */
  public function buildValue(string $input): ?string {
    try {
      // Input time is supposed to be in default site timezone.
      $datetime = (new DTI($input))->setTimezone(DateTimeItem::getStorageTimezone());
    }
    catch (\DateMalformedStringException) {
      return NULL;
    }
    $precision = match ($this->getGranularity()) {
      Granularity::MICROSECOND, Granularity::MILLISECOND => Precision::MICROSECOND,
      default => Precision::SECOND,
    };
    return $datetime->format(DateTimeItem::getFormat($precision));
  }

  /**
   * {@selfdoc}
   */
  private function getGranularity(): Granularity {
    if (!isset($this->options['dp_granularity'])) {
      throw new \LogicException('Granularity option is not set.');
    }
    return Granularity::from($this->options['dp_granularity']);
  }

}
