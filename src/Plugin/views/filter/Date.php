<?php

declare(strict_types=1);

namespace Drupal\date_point\Plugin\views\filter;

use DateTimeImmutable as DTI;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Data\Views\QueryOperator;
use Drupal\date_point\Plugin\Field\FieldType\Date\DateItem;
use Drupal\views\Attribute\ViewsFilter;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter to handle time points.
 *
 * @property \Drupal\views\Plugin\views\query\Sql $query
 */
#[ViewsFilter(self::ID)]
final class Date extends FilterPluginBase {

  public const string ID = 'dp_date';

  /**
   * {@inheritdoc}
   */
  protected $alwaysMultiple = TRUE;

  /**
   * {@inheritdoc}
   */
  public function operatorOptions(): array {
    $options = QueryOperator::getOptions();
    if (empty($this->definition['allow empty'])) {
      unset($options['empty'], $options['not_empty']);
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state): void {
    $form['value'] = [
      '#type' => 'date',
      '#title' => new TM('Value'),
      '#attributes' => ['type' => 'date'],
      '#default_value' => $this->value,
    ];

    if ($form_state->get('exposed')) {
      $identifier = $this->options['expose']['identifier'];
      $user_input = $form_state->getUserInput();
      if (!isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value;
        $form_state->setUserInput($user_input);
      }
    }

    $form['value']['#states']['invisible'][] = [':input[name="options[operator]"]' => ['value' => 'empty']];
    $form['value']['#states']['invisible'][] = [':input[name="options[operator]"]' => ['value' => 'not_empty']];
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary(): TM|string {
    if ($this->isAGroup()) {
      return new TM('grouped');
    }
    elseif ($this->options['exposed']) {
      return new TM('exposed');
    }
    return parent::adminSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function validateExposeForm($form, FormStateInterface $form_state): void {
    parent::validate();
    $value = $form_state->getValue('options')['value'] ?? NULL;
    if ($value && !self::buildDate($value)) {
      $form_state->setError($form['value'], (string) new TM('Wrong date value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $this->ensureMyTable();
    $field = "$this->tableAlias.$this->realField";
    $datetime = self::buildDate($this->value);
    if (!$datetime) {
      // As there no validation for user input we just filter out all records
      // when the supplied date is not valid.
      $this->query->addWhereExpression($this->options['group'], '1 = 2');
      return;
    }

    switch ($this->operator) {
      case 'empty':
        $this->query->addWhere($this->options['group'], $field, NULL, 'IS NULL');
        break;

      case 'not_empty':
        $this->query->addWhere($this->options['group'], $field, NULL, 'IS NOT NULL');
        break;

      default:
        $this->query->addWhere($this->options['group'], $field, $this->value, $this->operator);
    }
  }

  /**
   * {@selfdoc}
   */
  private static function buildDate(string $value): ?DTI {
    try {
      return new DTI($value, DateItem::getStorageTimezone());
    }
    catch (\DateMalformedStringException) {
      return NULL;
    }
  }

}
