<?php

declare(strict_types=1);

namespace Drupal\date_point\Clock;

use Psr\Clock\ClockInterface;

/**
 * Provides access to clock service.
 */
trait ClockTrait {

  /**
   * {@selfdoc}
   */
  public static function getClock(): ClockInterface {
    return \Drupal::service('date_point.clock');
  }

}
