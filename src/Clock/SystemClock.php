<?php

declare(strict_types=1);

namespace Drupal\date_point\Clock;

use Psr\Clock\ClockInterface;

/**
 * A service for obtaining system time.
 */
final readonly class SystemClock implements ClockInterface {

  /**
   * {@inheritdoc}
   *
   * The timestamp can be in any timezone. The caller itself must convert the
   * timestamp to the desired timezone before formatting.
   */
  public function now(): \DateTimeImmutable {
    return new \DateTimeImmutable();
  }

}
