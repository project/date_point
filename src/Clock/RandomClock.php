<?php

declare(strict_types=1);

namespace Drupal\date_point\Clock;

use Psr\Clock\ClockInterface;

/**
 * A service for generating random date point.
 */
final readonly class RandomClock implements ClockInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(
    private string $minDatePoint,
    private string $maxDatePoint,
  ) {}

  /**
   * {@inheritdoc }
   */
  public function now(): \DateTimeImmutable {
    $now = new \DateTimeImmutable();
    // The float timestamp may exceed PHP_FLOAT_MAX. Therefore, we first create
    // a timestamp with only microseconds, like 0.123456, and then modify it
    // with the integer timestamp.
    $now_ts = \mt_rand(
      min: $now->modify($this->minDatePoint)->getTimestamp(),
      max: $now->modify($this->maxDatePoint)->getTimestamp() - 1,
    );
    $now_us = \number_format(\mt_rand(0, 1_000_000) / 1_000_000, 6, '.', '');
    // @phpstan-ignore method.nonObject
    return \DateTimeImmutable::createFromFormat('U.u', $now_us)
      ->modify(\sprintf('%d seconds', $now_ts));
  }

}
