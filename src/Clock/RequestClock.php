<?php

declare(strict_types=1);

namespace Drupal\date_point\Clock;

use DateTimeImmutable as DTI;
use Psr\Clock\ClockInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A service for obtaining request time.
 */
final readonly class RequestClock implements ClockInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(private ?RequestStack $requestStack = NULL) {}

  /**
   * {@selfdoc}
   */
  public function now(): DTI {
    $request = $this->requestStack?->getCurrentRequest();
    $timestamp = $request ? $request->server->get('REQUEST_TIME_FLOAT') : $_SERVER['REQUEST_TIME_FLOAT'];
    // @todo Use DTI::createFromTimeStamp() once we drop support for PHP 8.3
    // @todo Is casting to string really needed?
    // @phpstan-ignore return.type
    return DTI::createFromFormat('U.u', (string) $timestamp);
  }

}
