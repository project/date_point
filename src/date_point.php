<?php

/**
 * @file
 * Helper constants and functions for Date Point module.
 *
 * @phpcs:disable Drupal.Commenting.FileComment.Missing
 */

declare(strict_types=1);

namespace Drupal\date_point;

use Psr\Clock\ClockInterface;

/**
 * Returns clock service.
 */
function clock(): ClockInterface {
  return \Drupal::service('date_point.clock');
}

/**
 * Returns the current time as a DateTimeImmutable object
 */
function now(): \DateTimeImmutable {
  return clock()->now();
}

/**
 * Returns current Unix timestamp.
 */
function time(): int {
  return now()->getTimestamp();
}
