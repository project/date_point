<?php

namespace Drupal\date_point;

use Drupal\Component\Datetime\TimeInterface;
use Psr\Clock\ClockInterface;

/**
 * Provides a class for obtaining system time.
 */
final readonly class Time implements TimeInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(
    private ClockInterface $systemClock,
    private ClockInterface $requestClock,
  ) {

  }

  /**
   * {@inheritdoc}
   */
  public function getRequestTime(): int {
    return $this->requestClock->now()->getTimestamp();
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestMicroTime(): float {
    return (float) $this->requestClock->now()->format('U.u');
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTime(): int {
    return $this->systemClock->now()->getTimestamp();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentMicroTime(): float {
    return (float) $this->systemClock->now()->format('U.u');
  }

}
