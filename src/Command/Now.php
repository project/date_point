<?php

declare(strict_types=1);

namespace Drupal\date_point\Command;

use Psr\Clock\ClockInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * {@selfdoc}
 */
#[AsCommand(
  name: 'date-point:now',
  description: 'Displays current timestamp in a given format.',
  aliases: ['now']
)]
final class Now extends Command {

  /**
   * {@inheritdoc}
   */
  public function __construct(private readonly ClockInterface $clock) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this
      ->addOption(
        name: 'modifier',
        shortcut: 'm',
        mode: InputOption::VALUE_REQUIRED,
        description: 'A date/time string. See https://www.php.net/manual/en/datetime.formats.php for details.'
      )
      ->addOption(
        name: 'format',
        shortcut: 'f',
        mode: InputOption::VALUE_REQUIRED,
        description: 'The format of the outputted date string.',
        default: 'Y-m-d H:i:s.vP',
      )
      ->addOption(
        name: 'timezone',
        shortcut: 't',
        mode: InputOption::VALUE_REQUIRED,
        description: 'Timezone to convert the date before formatting.',
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    $io = new SymfonyStyle($input, $output);
    $timestamp = $this->clock->now();

    if ($modifier = $input->getOption('modifier')) {
      try {
        $timestamp = $timestamp->modify($modifier);
      }
      catch (\DateMalformedStringException) {
        $io->getErrorStyle()->error('Wrong modifier');
        return self::FAILURE;
      }
    }

    if ($timezone = $input->getOption('timezone')) {
      try {
        $tz = new \DateTimeZone($timezone);
      }
      catch (\DateInvalidTimeZoneException) {
        $io->getErrorStyle()->error('Wrong timezone');
        return self::FAILURE;
      }
      $timestamp = $timestamp->setTimezone($tz);
    }

    $io->writeln($timestamp->format($input->getOption('format')));
    return self::SUCCESS;
  }

}
