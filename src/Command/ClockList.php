<?php

declare(strict_types=1);

namespace Drupal\date_point\Command;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\DependencyInjection\ContainerInterface;
use Psr\Clock\ClockInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\OutputStyle;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * {@selfdoc}
 */
#[AsCommand(
  name: 'date-point:clock-list',
  description: 'An overview of available clocks and mocked values.',
  aliases: ['clocks']
)]
final class ClockList extends Command {

  private const string TIMESTAMP_FORMAT = 'Y-m-d H:i:s.vP';
  private const string DEFAULT_TIME_SERVICE = 'datetime.time';
  private const string DEFAULT_CLOCK_SERVICE = 'date_point.clock';

  /**
   * {@inheritdoc}
   */
  public function __construct(private readonly ContainerInterface $container) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    $io = new SymfonyStyle($input, $output);
    $this->printCoreClocks($io);
    $this->printDatePointClocks($io);
    return self::SUCCESS;
  }

  /**
   * {@selfdoc}
   */
  private function printCoreClocks(OutputStyle $io): void {
    $clock_ids = \array_filter(
      $this->container->getServiceIds(),
      static fn (string $service_id): bool => \str_contains($service_id, '.time'),
    );
    $headers = ['ID', 'Class', 'Now', 'Request time'];
    $rows = [];
    foreach ($clock_ids as $clock_id) {
      $clock = $this->container->get($clock_id);
      if ($clock instanceof TimeInterface) {
        $row = [
          $clock_id,
          $clock::class,
          self::formatMicroTime($clock->getCurrentMicroTime()),
          self::formatMicroTime($clock->getRequestMicroTime()),
        ];
        if ($clock_id === self::DEFAULT_TIME_SERVICE) {
          $row = \array_map(self::highlight(...), $row);
        }
        $rows[] = $row;
      }
    }
    $io->writeln(\sprintf('<comment>Drupal Core — %s</comment>', TimeInterface::class));
    $io->table($headers, $rows);
  }

  /**
   * {@selfdoc}
   */
  private function printDatePointClocks(OutputStyle $io): void {
    $clock_ids = \array_filter(
      $this->container->getServiceIds(),
      static fn (string $service_id): bool => \str_contains($service_id, '.clock'),
    );
    // Default clock service is actually a service alias which many not present
    // in service ID list.
    if (!\in_array(self::DEFAULT_CLOCK_SERVICE, $clock_ids)) {
      \array_unshift($clock_ids, self::DEFAULT_CLOCK_SERVICE);
    }
    $headers = ['ID', 'Class', 'Now'];
    $rows = [];
    foreach ($clock_ids as $clock_id) {
      $clock = $this->container->get($clock_id);
      if ($clock instanceof ClockInterface) {
        $row = [
          $clock_id,
          $clock::class,
          self::formatTimeStamp($clock->now()),
        ];
        if ($clock_id === self::DEFAULT_CLOCK_SERVICE) {
          $row = \array_map(self::highlight(...), $row);
        }
        $rows[] = $row;
      }
    }
    $io->writeln(\sprintf('<comment>PSR-20 — %s</comment>', ClockInterface::class));
    $io->table($headers, $rows);
  }

  /**
   * {@selfdoc}
   */
  private static function highlight(string $value): string {
    return \sprintf('<fg=#d0c92b>%s</>', $value);
  }

  /**
   * {@selfdoc}
   */
  private static function formatTimeStamp(\DateTimeImmutable $timestamp): string {
    $current_tz = (new \DateTimeImmutable())->getTimezone();
    return $timestamp->setTimezone($current_tz)->format(self::TIMESTAMP_FORMAT);
  }

  /**
   * {@selfdoc}
   */
  private static function formatMicroTime(float $timestamp): string {
    return self::formatTimeStamp(
      // @phpstan-ignore argument.type
      \DateTimeImmutable::createFromFormat('U.u', \number_format($timestamp, 6, thousands_separator: '')),
    );
  }

}
