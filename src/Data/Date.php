<?php

declare(strict_types=1);

namespace Drupal\date_point\Data;

use DateTimeImmutable as DTI;

/**
 * Represents a date.
 */
final readonly class Date implements \Stringable, \JsonSerializable {

  private const string STORAGE_FORMAT = 'Y-m-d';

  /**
   * {@selfdoc}
   */
  public function __construct(private string $value) {
    $dti = DTI::createFromFormat(self::STORAGE_FORMAT, $this->value);
    if (!$dti) {
      throw new \DateMalformedStringException(\sprintf('Wrong date value "%s".', $this->value));
    }

    // Unlike PHP DateTime object this one does not allow overflows, which
    // means that it's not possible to set date to something like 2024-13-32.
    [, $month, $day] = \explode('-', $this->value);
    if ($month > 12 || $day > 31) {
      throw new \DateMalformedStringException(\sprintf('Wrong date value "%s".', $this->value));
    }
  }

  /**
   * {@selfdoc}
   */
  public static function fromDateTime(DTI $datetime): self {
    return new self($datetime->format(self::STORAGE_FORMAT));
  }

  /**
   * {@selfdoc}
   */
  public function format(string $format): string {
    return $this->toDateTime()->format($format);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->format(self::STORAGE_FORMAT);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): string {
    return (string) $this;
  }

  /**
   * {@selfdoc}
   */
  public function toDateTime(?\DateTimeZone $timezone = NULL): DTI {
    return new DTI($this->value, $timezone);
  }

  /**
   * @deprecated
   */
  public function toStorage(): string {
    return (string) $this;
  }

}
