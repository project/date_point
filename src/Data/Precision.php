<?php

declare(strict_types=1);

namespace Drupal\date_point\Data;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * Represents storage precision for date and time fields.
 */
enum Precision: string {

  case SECOND = 'second';
  case MILLISECOND = 'millisecond';
  case MICROSECOND = 'microsecond';

  /**
   * Returns numeric precision suitable for defining database schema.
   *
   * @return int<0, 6>
   */
  public function toInteger(): int {
    return match ($this) {
      self::SECOND => 0,
      self::MILLISECOND => 3,
      self::MICROSECOND => 6,
    };
  }

  /**
   * Returns translated precision label.
   */
  public function getLabel(): TM {
    return match ($this) {
      self::SECOND => new TM('Second'),
      self::MILLISECOND => new TM('Millisecond'),
      self::MICROSECOND => new TM('Microsecond'),
    };
  }

  /**
   * Returns precision options.
   *
   * @return array<value-of<\Drupal\date_point\Data\Precision>, \Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function getOptions(): array {
    $options = [];
    foreach (self::cases() as $precision) {
      $options[$precision->value] = $precision->getLabel();
    }
    return $options;
  }

}
