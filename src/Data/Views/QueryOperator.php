<?php

declare(strict_types=1);

namespace Drupal\date_point\Data\Views;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * Represents an operator for Views queries.
 */
enum QueryOperator: string {

  case EARLIER = '<';
  case EARLIER_EQUAL = '<=';
  case EQUAL = '=';
  case NOT_EQUAL = '!=';
  case LATER = '>';
  case LATER_EQUAL = '>=';
  case EMPTY = 'empty';
  case NOT_EMPTY = 'not_empty';

  /**
   * Returns operator options.
   *
   * @return array<value-of<\Drupal\date_point\Data\Views\QueryOperator>, \Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function getOptions(): array {
    return [
      self::EARLIER->value => new TM('Is earlier than'),
      self::EARLIER_EQUAL->value => new TM('Is earlier than or equal to'),
      self::EQUAL->value => new TM('Is equal to'),
      self::NOT_EQUAL->value => new TM('Is not equal to'),
      self::LATER->value => new TM('Is later than'),
      self::LATER_EQUAL->value => new TM('Is later than or equal to'),
      self::EMPTY->value => new TM('Is empty (NULL)'),
      self::NOT_EMPTY->value => new TM('Is not empty (NOT NULL)'),
    ];
  }

}
