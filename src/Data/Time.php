<?php

declare(strict_types=1);

namespace Drupal\date_point\Data;

use DateTimeImmutable as DTI;

/**
 * Represents a time point without time zone.
 *
 * Note that this object should only be used for time points. It is not suitable
 * to represent durations, i.e. travel time and so on.
 */
final readonly class Time implements \Stringable, \JsonSerializable {

  private const string STORAGE_FORMAT = 'H:i:s.u';

  /**
   * {@selfdoc}
   */
  private string $value;

  /**
   * {@selfdoc}
   */
  public function __construct(string $time) {
    $format = match(\strlen($time)) {
      5 => 'H:i',
      8 => 'H:i:s',
      10, 11, 12 => 'H:i:s.v',
      13, 14, 15 => 'H:i:s.u',
      default => self::throwException($time),
    };
    $datetime = DTI::createFromFormat($format, $time, self::getStorageTimeZone());
    if (!$datetime) {
      self::throwException($time);
    }

    // Unlike PHP DateTime object this one does not allow overflows, which means
    // it is not possible to set time to something like 25:61:61.
    [$hours, $minutes, $seconds] = \array_pad(\explode(':', $time), 3, 0);
    if ($hours >= 24 || $minutes >= 60 || $seconds >= 60) {
      self::throwException($time);
    }

    $this->value = $datetime->format(self::STORAGE_FORMAT);
  }

  /**
   * {@selfdoc}
   */
  public static function fromDateTime(DTI $datetime): self {
    return new self($datetime->format(self::STORAGE_FORMAT));
  }

  /**
   * {@selfdoc}
   */
  public static function fromSeconds(int|float $seconds): self {
    if ($seconds < 0 || $seconds >= 24 * 3_600) {
      throw new \OutOfRangeException('The time value should be in withing 0 - 86400 seconds.');
    }
    $seconds_part = (int) \floor($seconds);
    $microseconds_part = (int) (1_000_000 * ($seconds - $seconds_part));
    $datetime = (new DTI(timezone: self::getStorageTimeZone()))
      ->setTime(0, 0, $seconds_part, $microseconds_part);
    return self::fromDateTime($datetime);
  }

  /**
   * {@selfdoc}
   */
  public function format(string $format): string {
    return $this->getDateTime()->format($format);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->format('H:i:s');
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): string {
    return (string) $this;
  }

  /**
   * @deprecated
   */
  public function toStorage(): string {
    return $this->value;
  }

  /**
   * @throws \DateMalformedStringException
   *
   * @return never
   */
  private static function throwException(string $time): void {
    throw new \DateMalformedStringException(\sprintf('Wrong time value "%s".', $time));
  }

  /**
   * {@selfdoc}
   */
  private static function getStorageTimeZone(): \DateTimeZone {
    return new \DateTimeZone('UTC');
  }

  /**
   * {@selfdoc}
   */
  private function getDateTime(): DTI {
    return new DTI($this->value);
  }

}
