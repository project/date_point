<?php

declare(strict_types=1);

namespace Drupal\date_point\Data;

use DateTimeImmutable as DTI;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Represents a month of a specific year.
 */
final readonly class YearMonth implements \Stringable, \JsonSerializable {

  private const string STORAGE_FORMAT = 'Y-m';

  /**
   * {@selfdoc}
   */
  public function __construct(private string $value) {
    if (!\preg_match('#^\d{4}-(0[1-9]|1[0-2])$#', $value)) {
      throw new \DateMalformedStringException(\sprintf('Wrong month value "%s".', $this->value));
    }
  }

  /**
   * {@selfdoc}
   */
  public static function fromDateTime(DTI $datetime): self {
    return new self($datetime->format(self::STORAGE_FORMAT));
  }

  /**
   * {@selfdoc}
   */
  public function toDateTime(?\DateTimeZone $timezone = NULL): DTI {
    return new DTI($this->value, $timezone);
  }

  /**
   * {@selfdoc}
   */
  public function format(string $format): string {
    return $this->toDateTime()->format($format);
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    return $this->format(self::STORAGE_FORMAT);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): string {
    return (string) $this;
  }

  /**
   * Defines timestamp boundaries suitable for SQL queries.
   *
   * These boundaries only suitable for querying Date Point timestamps.
   *
   * @phpstan-return array{0: non-empty-string, 1: non-empty-string}
   *
   * @see \Drupal\date_point\Data\DateTime\Granularity::getBoundaries
   */
  public function getBoundaries(\DateTimeZone $timezone): array {
    $storage_timezone = DateTimeItem::getStorageTimezone();
    $start = $this->toDateTime($storage_timezone);
    $end = $start->modify('+1 month');
    $format = 'Y-m-d H:i:s.000000';
    return [
      $start->setTimezone($timezone)->format($format),
      $end->setTimezone($timezone)->format($format),
    ];
  }

}
