<?php

declare(strict_types=1);

namespace Drupal\date_point\Data\DateTime;

use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * {@selfdoc}
 */
enum Behavior: string {

  case MANUAL = 'manual';
  case CREATED = 'created';
  case UPDATED = 'updated';

  /**
   * Returns translated behavior label.
   */
  public function getLabel(): TM {
    return match ($this) {
      self::MANUAL => new TM('Manual'),
      self::CREATED => new TM('Created'),
      self::UPDATED => new TM('Updated'),
    };
  }

  /**
   * Returns behavior options.
   *
   * @return array<value-of<\Drupal\date_point\Data\DateTime\Behavior>, \Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function getOptions(): array {
    $options = [];
    foreach (self::cases() as $behavior) {
      $options[$behavior->value] = $behavior->getLabel();
    }
    return $options;
  }

}
