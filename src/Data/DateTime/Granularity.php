<?php

declare(strict_types=1);

namespace Drupal\date_point\Data\DateTime;

use DateTimeImmutable as DTI;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\date_point\Plugin\Field\FieldType\DateTime\DateTimeItem;

/**
 * Represents date point granularity.
 */
enum Granularity: string {

  case MICROSECOND = 'microsecond';
  case MILLISECOND = 'millisecond';
  case SECOND = 'second';
  case MINUTE = 'minute';
  case HOUR = 'hour';
  case DAY = 'day';
  case MONTH = 'month';
  case YEAR = 'year';

  /**
   * Returns translated granularity label.
   */
  public function getLabel(): TM {
    return match ($this) {
      self::MICROSECOND => new TM('Microsecond'),
      self::MILLISECOND => new TM('Millisecond'),
      self::SECOND => new TM('Second'),
      self::MINUTE => new TM('Minute'),
      self::HOUR => new TM('Hour'),
      self::DAY => new TM('Day'),
      self::MONTH => new TM('Month'),
      self::YEAR => new TM('Year'),
    };
  }

  /**
   * Returns granularity options.
   *
   * @return array<value-of<\Drupal\date_point\Data\DateTime\Granularity>, \Drupal\Core\StringTranslation\TranslatableMarkup>
   */
  public static function getOptions(): array {
    $options = [];
    foreach (self::cases() as $granularity) {
      $options[$granularity->value] = $granularity->getLabel();
    }
    return $options;
  }

  /**
   * Defines timestamp boundaries suitable for SQL queries.
   *
   * Filtering timestamps with granularity requires following special rules.
   * Example:
   * input: '2024-06-22 11:53:00'
   * granularity: 'day
   * op: '<=' → WHERE field <  '2024-06-23 00:00:00'
   * op: '<'  → WHERE field <  '2024-06-22 00:00:00'
   * op: '>=' → WHERE field >= '2024-06-22 00:00:00'
   * op: '>'  → WHERE field >= '2024-06-23 00:00:00'
   *
   * @phpstan-return array{0: non-empty-string, 1: non-empty-string}
   */
  public function getBoundaries(DTI $input): array {
    $start_format = match($this) {
      self::MICROSECOND => 'Y-m-d H:i:s.u',
      self::MILLISECOND => 'Y-m-d H:i:s.v',
      self::SECOND => 'Y-m-d H:i:s.000000',
      self::MINUTE => 'Y-m-d H:i:00.000000',
      self::HOUR => 'Y-m-d H:00:00.000000',
      self::DAY => 'Y-m-d 00:00:00.000000',
      self::MONTH => 'Y-m-01 00:00:00.000000',
      self::YEAR => 'Y-01-01 00:00:00.000000',
    };
    $start = new DTI($input->format($start_format), $input->getTimezone());
    $end = $start->modify('+1 ' . $this->value);

    $storage_tz = DateTimeItem::getStorageTimezone();
    // @todo Should we always use format with max precision?
    $format = match ($this) {
      self::MICROSECOND => 'Y-m-d H:i:s.u',
      self::MILLISECOND => 'Y-m-d H:i:s.v',
      default => 'Y-m-d H:i:s',
    };
    return [
      $start->setTimezone($storage_tz)->format($format),
      $end->setTimezone($storage_tz)->format($format),
    ];
  }

}
