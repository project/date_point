# Date Point

A field type for date points.

Key features:

1. Native database types for datetime storage
2. Compatibility with [PSR-20](https://www.php-fig.org/psr/psr-20/)
3. Support for HTML5 input elements
4. Configurable precision (seconds, milliseconds, microseconds)

## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
