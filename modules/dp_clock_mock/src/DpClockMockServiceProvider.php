<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\Site\Settings;

/**
 * Defines a service provider for the Clock Mock module.
 */
final class DpClockMockServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    // If clock mock is not set leave the clock with default implementation.
    if ($clock_mock = self::getMock('clock')) {
      $state_clock_definition = $container->getDefinition($clock_mock);
      $container->setDefinition('date_point.clock', $state_clock_definition);
      $container->setDefinition('date_point.clock.request', $state_clock_definition);
    }
    // Note that mocking this time service is pointless without mocking date
    // point clocks.
    // @see \Drupal\date_point\Time
    if ($time_mock = self::getMock('time')) {
      $container->setDefinition('datetime.time', $container->getDefinition($time_mock));
    }
  }

  /**
   * @phpstan-param 'clock'|'time' $type
   */
  public static function setMock(string $type, ?string $service): void {
    $filename = self::getFileName($type);
    \file_put_contents($filename, $service);
  }

  /**
   * @phpstan-param 'clock'|'time' $type
   */
  public static function getMock(string $type): ?string {
    $filename = self::getFileName($type);
    if (\file_exists($filename)) {
      // Trim content in case someone edited that file manually.
      // @phpstan-ignore argument.type
      return \rtrim(\file_get_contents($filename));
    }
    return NULL;
  }

  /**
   * @phpstan-param 'clock'|'time' $type
   */
  public static function resetMock(string $type): void {
    if (\file_exists(self::getFileName($type))) {
      \unlink(self::getFileName($type));
    }
  }

  /**
   * @phpstan-param 'clock'|'time' $type
   */
  private static function getFileName(string $type): string {
    // We cannot use 'temporary' scheme here as stream wrappers may not
    // available when Drupal is not fully bootstrapped. For instance during
    // update DB procedure.
    $tmp_dir = \sys_get_temp_dir();
    // Encrypt the hash salt as the /tmp directory might be available by
    // non-privileged users.
    $hash = \substr(\hash('sha256', Settings::get('hash_salt')), 0, 16);
    return \sprintf('%s/clock_mock.%s.%s.txt', $tmp_dir, $type, $hash);
  }

}
