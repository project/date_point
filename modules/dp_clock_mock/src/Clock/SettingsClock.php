<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock\Clock;

use Drupal\Core\Site\Settings;
use Psr\Clock\ClockInterface;

/**
 * A clock that uses Drupal settings for storing current timestamp.
 */
final readonly class SettingsClock implements ClockInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(private Settings $settings) {}

  /**
   * {@inheritdoc}
   */
  public function now(): \DateTimeImmutable {
    return new \DateTimeImmutable($this->settings::get('dp_clock_mock', 'now'), new \DateTimeZone('UTC'));
  }

}
