<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock\Clock;

use Drupal\Core\State\StateInterface;
use Psr\Clock\ClockInterface;

/**
 * A clock that uses Drupal state storage for storing current timestamp.
 */
final readonly class StateClock implements ClockInterface, ClockHandlerInterface {

  private const string STORAGE_KEY = 'date_point.clock.state';

  /**
   * {@selfdoc}
   */
  public function __construct(private StateInterface $state) {}

  /**
   * {@inheritdoc}
   */
  public function now(): \DateTimeImmutable {
    // Explicit timezone is required when the date/time string is in a relative
    // format, i.e. '+10 months'. Otherwise, the clock may be advanced according
    // to DST rules in the current site timezone.
    return new \DateTimeImmutable($this->get(), new \DateTimeZone('UTC'));
  }

  /**
   * {@inheritdoc}
   */
  public function get(): string {
    return $this->state->get(self::STORAGE_KEY, 'now');
  }

  /**
   * {@selfdoc}
   */
  public function set(string $timestamp): self {
    $this->state->set(self::STORAGE_KEY, $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function reset(): self {
    $this->state->delete(self::STORAGE_KEY);
    return $this;
  }

}
