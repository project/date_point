<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock\Clock;

use Psr\Clock\ClockInterface;

/**
 * A clock implementation that returns a fixed date-time.
 *
 * Note: Although this class is named `FrozenClock`, the date and time returned
 * by the `now()` method depends on the string provided during initialization.
 * If the string represents a relative time (e.g., 'now' or '+ 1 day'), the
 * returned time may not be fixed and could vary depending on when `now()` is
 * called.
 */
final readonly class FrozenClock implements ClockInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(private string $timestamp) {}

  /**
   * {@inheritdoc}
   */
  public function now(): \DateTimeImmutable {
    return new \DateTimeImmutable($this->timestamp, new \DateTimeZone('UTC'));
  }

}
