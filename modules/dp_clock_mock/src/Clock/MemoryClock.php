<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock\Clock;

use Psr\Clock\ClockInterface;

/**
 * A clock that uses static variable for storing current timestamp.
 */
final class MemoryClock implements ClockInterface, ClockHandlerInterface {

  /**
   * {@selfdoc}
   */
  private string $timestamp = 'now';

  /**
   * {@inheritdoc}
   */
  public function now(): \DateTimeImmutable {
    return new \DateTimeImmutable($this->get(), new \DateTimeZone('UTC'));
  }

  /**
   * {@inheritdoc}
   */
  public function set(string $timestamp): self {
    $this->timestamp = $timestamp;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function get(): string {
    return $this->timestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function reset(): self {
    return $this->set('now');
  }

}
