<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock\Clock;

/**
 * Interface for clocks that allow altering current timestamp.
 */
interface ClockHandlerInterface {

  /**
   * Gets current timestamp as string.
   */
  public function get(): string;

  /**
   * Sets current timestamp.
   */
  public function set(string $timestamp): self;

  /**
   * Resets the timestamp.
   */
  public function reset(): self;

}
