<?php

declare(strict_types=1);

namespace Drupal\dp_clock_mock\Clock;

use Drupal\Core\Site\Settings;
use Psr\Clock\ClockInterface;

/**
 * A clock that uses file system for storing current timestamp.
 */
final readonly class FileClock implements ClockInterface, ClockHandlerInterface {

  /**
   * {@selfdoc}
   */
  public function __construct(private Settings $settings) {}

  /**
   * {@inheritdoc}
   */
  public function now(): \DateTimeImmutable {
    return new \DateTimeImmutable($this->get(), new \DateTimeZone('UTC'));
  }

  /**
   * {@inheritdoc}
   */
  public function get(): string {
    if (\file_exists($this->getFileName())) {
      // Trim content in case someone edited that file manually.
      // @phpstan-ignore argument.type
      return \rtrim(\file_get_contents($this->getFileName()));
    }
    return 'now';
  }

  /**
   * {@selfdoc}
   */
  public function set(string $timestamp): self {
    \file_put_contents($this->getFileName(), $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function reset(): self {
    if (\file_exists($this->getFileName())) {
      \unlink($this->getFileName());
    }
    return $this;
  }

  /**
   * {@selfdoc}
   */
  private function getFileName(): string {
    // Encrypt the hash salt as the /tmp directory might be available by
    // non-privileged users.
    $hash = \substr(\hash('sha256', $this->settings::get('hash_salt')), 0, 16);
    return \sprintf('temporary://dp_clock_mock.file_timestamp.%s.txt', $hash);
  }

}
