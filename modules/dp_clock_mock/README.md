# Clock Mock

A helper module for mocking system time.

## Usage

The mock clocks are intended for use in automated tests. However, some
of them can also be useful for manual testing of clock-related functionality
on real sites.

### Mock system time
```php
// Mock date point clock.
\dp_clock_mock_file()->set('2024-09-30 18:45:53+00:00');
// Mock time service from Drupal core.
\dp_time_mock('date_point.time');
```

### Cancel mocking
```php
\dp_clock_mock(NULL);
\dp_time_mock(NULL);
```

