<?php

declare(strict_types=1);

namespace Drupal\Tests\dp_clock_mock\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * @group date_point
 */
final class ClockMockTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin(
      // @phpstan-ignore argument.type
      $this->drupalCreateUser(['administer site configuration']),
    );
    $this->config('core.date_format.long')->set('pattern', \DateTimeImmutable::ATOM)->save();
    $this->config('system.date')->set('timezone.default', 'UTC')->save();
    $this->config('system.date')->set('timezone.user.configurable', FALSE)->save();
  }

  /**
   * {@selfdoc}
   */
  public function testClockMock(): void {
    $this->assertClockMock(NULL, 'SystemClock');

    \dp_clock_mock('date_point.clock.request');
    $this->assertClockMock(NULL, 'RequestClock');

    \dp_clock_mock('date_point.clock.random');
    $this->assertClockMock(NULL, 'RandomClock');

    // @phpstan-ignore-next-line method.notFound
    \dp_clock_mock('dp_clock_mock.clock.state')->set('2023-08-12T15:30:00+00:00');
    $this->assertClockMock('2023-08-12T15:30:00+00:00', 'StateClock');

    // @phpstan-ignore-next-line method.notFound
    \dp_clock_mock('dp_clock_mock.clock.memory')->set('2023-08-1215:30:00+00:00');
    $this->assertClockMock(NULL, 'MemoryClock');

    // @phpstan-ignore-next-line method.notFound
    \dp_clock_mock('dp_clock_mock.clock.file')->set('2023-01-14 15:30:00+00:00');
    $this->assertClockMock('2023-01-14T15:30:00+00:00', 'FileClock');

    \dp_clock_mock('dp_clock_mock.clock.settings');
    $this->assertClockMock(NULL, 'SettingsClock');

    \dp_clock_mock('dp_clock_mock.clock.unix_epoch');
    $this->assertClockMock('1970-01-01T00:00:00+00:00', 'FrozenClock');

    \dp_clock_mock('dp_clock_mock.clock.millennium');
    $this->assertClockMock('2001-01-01T00:00:00+00:00', 'FrozenClock');

    // -- Shortcuts.
    \dp_clock_mock_memory()->set('2023-08-1215:30:00+00:00');
    $this->assertClockMock(NULL, 'MemoryClock');

    \dp_clock_mock_state()->set('2023-08-12T15:30:00+00:00');
    $this->assertClockMock('2023-08-12T15:30:00+00:00', 'StateClock');

    \dp_clock_mock_file()->set('2023-01-14 15:30:00+00:00');
    $this->assertClockMock('2023-01-14T15:30:00+00:00', 'FileClock');

    \dp_clock_mock_settings();
    $this->assertClockMock(NULL, 'SettingsClock');
  }

  /**
   * {@selfdoc}
   */
  public function testTimeMock(): void {
    \dp_clock_mock_file()->set('2023-01-01 21:30:00+00:00');
    \dp_time_mock('date_point.time');
    $this->drupalGet('/admin/config/regional/date-time');
    $this->assertTimeMock('2023-01-01 21:30:00');
  }

  /**
   * {@selfdoc}
   */
  private function assertClockMock(?string $expected_time, string $expected_clock): void {
    $this->drupalGet('/admin/reports/status');
    $elements = $this->xpath('//div[contains(text(), "Current timestamp")]/following-sibling::div');
    self::assertCount(1, $elements);

    $text = $elements[0]->getText();
    if (!\preg_match('#^(.+) Time is fetched from "(.+)" service.#', $text, $matches)) {
      self::fail('Clock requirement exists');
    }
    [, $rendered_time, $actual_clock] = $matches;
    // @phpstan-ignore method.nonObject
    $actual_time = \DateTimeImmutable::createFromFormat(\DateTimeImmutable::ATOM, $rendered_time)
      ->format(\DateTimeImmutable::ATOM);
    if ($expected_time) {
      self::assertSame($expected_time, $actual_time);
    }
    self::assertSame($expected_clock, $actual_clock);
  }

  /**
   * {@selfdoc}
   */
  private function assertTimeMock(string $expected_time): void {
    $this->drupalGet('/admin/config/regional/date-time');
    $elements = $this->xpath('//td[text() = "Date point storage"]/following-sibling::td[1]');
    self::assertCount(1, $elements);
    self::assertSame($expected_time, $elements[0]->getText());
  }

}
