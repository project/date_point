<?php

declare(strict_types=1);

namespace Drupal\Tests\dp_clock_mock\Kernel\Clock;

use DateTimeImmutable as DTI;
use Drupal\KernelTests\KernelTestBase;
use Drupal\dp_clock_mock\Clock\ClockHandlerInterface;
use Drupal\dp_clock_mock\Clock\MemoryClock;

/**
 * A test for memory clock service
 */
final class MemoryClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testClock(): void {
    $clock = $this->container->get('dp_clock_mock.clock.memory');
    self::assertInstanceOf(MemoryClock::class, $clock);
    // @phpstan-ignore-next-line
    self::assertInstanceOf(ClockHandlerInterface::class, $clock);

    self::assertContains($clock->now()->getTimestamp() - (new DTI())->getTimestamp(), [0, 1]);

    $clock->set('2024-12-04 00:00:12+000');
    self::assertEquals('2024-12-04 00:00:12+000', $clock->get());
    self::assertEquals(new DTI('2024-12-04 00:00:12+000'), $clock->now());

    $clock->set('+1 minute');
    self::assertContains($clock->now()->getTimestamp() - (new DTI())->getTimestamp(), [60, 61]);

    $clock->reset();
    self::assertContains($clock->now()->getTimestamp() - (new DTI())->getTimestamp(), [0, 1]);
  }

}
