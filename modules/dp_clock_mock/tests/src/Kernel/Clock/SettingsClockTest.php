<?php

declare(strict_types=1);

namespace Drupal\Tests\dp_clock_mock\Kernel\Clock;

use DateTimeImmutable as DTI;
use Drupal\KernelTests\KernelTestBase;
use Drupal\dp_clock_mock\Clock\SettingsClock;

/**
 * A test for settings clock service
 */
final class SettingsClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testClock(): void {
    $clock = $this->container->get('dp_clock_mock.clock.settings');
    self::assertInstanceOf(SettingsClock::class, $clock);

    self::assertContains($clock->now()->getTimestamp() - (new DTI())->getTimestamp(), [0, 1]);

    $this->setSetting('dp_clock_mock', '2024-12-04 00:00:12+000');
    self::assertEquals(new DTI('2024-12-04 00:00:12+000'), $clock->now());

    $this->setSetting('dp_clock_mock', '+1 minute');
    self::assertContains($clock->now()->getTimestamp() - (new DTI())->getTimestamp(), [60, 61]);
  }

}
