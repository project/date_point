<?php

declare(strict_types=1);

namespace Drupal\Tests\dp_clock_mock\Kernel\Clock;

use Drupal\KernelTests\KernelTestBase;
use Psr\Clock\ClockInterface;

/**
 * A test for a frozen clock.
 *
 * @group date_point
 */
final class FrozenClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testClock(): void {
    $clock = $this->container->get('dp_clock_mock.clock.unix_epoch');
    self::assertInstanceOf(ClockInterface::class, $clock);
    self::assertEquals(new \DateTimeImmutable('1970-01-01 00:00:00.000000+00:00'), $clock->now());

    $clock = $this->container->get('dp_clock_mock.clock.millennium');
    self::assertInstanceOf(ClockInterface::class, $clock);
    self::assertEquals(new \DateTimeImmutable('2001-01-01 00:00:00.000000+00:00'), $clock->now());
  }

}
