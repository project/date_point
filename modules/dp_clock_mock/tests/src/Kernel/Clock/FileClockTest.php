<?php

declare(strict_types=1);

namespace Drupal\Tests\dp_clock_mock\Kernel\Clock;

use DateTimeImmutable as DTI;
use Drupal\KernelTests\KernelTestBase;
use Drupal\dp_clock_mock\Clock\ClockHandlerInterface;
use Drupal\dp_clock_mock\Clock\FileClock;

/**
 * A test for file clock service
 */
final class FileClockTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testClock(): void {
    $clock = $this->container->get('dp_clock_mock.clock.file');
    self::assertInstanceOf(FileClock::class, $clock);
    // @phpstan-ignore-next-line
    self::assertInstanceOf(ClockHandlerInterface::class, $clock);

    $tz = new \DateTimeZone('UTC');

    self::assertEquals('now', $clock->get());
    $actual_timestamp = $clock->now()->setTimezone($tz)->getTimestamp();
    $expected_timestamp = (new DTI())->setTimezone($tz)->getTimestamp();
    self::assertContains($actual_timestamp - $expected_timestamp, [0, 1]);

    $clock->set('2024-12-04 00:00:12+000');
    self::assertEquals('2024-12-04 00:00:12+000', $clock->get());
    self::assertEquals(new DTI('2024-12-04 00:00:12+000'), $clock->now());

    $clock->set('+1 minute');
    self::assertEquals('+1 minute', $clock->get());
    $actual_timestamp = $clock->now()->setTimezone($tz)->getTimestamp();
    $expected_timestamp = (new DTI())->setTimezone($tz)->getTimestamp();
    self::assertContains($actual_timestamp - $expected_timestamp, [60, 61]);

    $clock->reset();
    self::assertEquals('now', $clock->get());
    $actual_timestamp = $clock->now()->setTimezone($tz)->getTimestamp();
    $expected_timestamp = (new DTI())->setTimezone($tz)->getTimestamp();
    self::assertContains($actual_timestamp - $expected_timestamp, [0, 1]);
  }

}
