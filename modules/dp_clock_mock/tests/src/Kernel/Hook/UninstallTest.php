<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel;

use Drupal\Core\State\StateInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * A test for hook_uninstall().
 *
 * @covers \dp_clock_mock_uninstall
 */
final class UninstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['date_point', 'dp_clock_mock'];

  /**
   * {@selfdoc}
   */
  public function testHook(): void {
    \dp_clock_mock('dp_clock_mock.clock.file');
    \dp_time_mock('date_point.time');
    $state = $this->container->get('state');
    \assert($state instanceof StateInterface);

    $file_clock = $this->container->get('dp_clock_mock.clock.file');
    $file_clock->set('00:00:00');
    self::assertSame('00:00:00', $file_clock->get());
    $state_clock = $this->container->get('dp_clock_mock.clock.state');
    $state_clock->set('00:00:00');
    self::assertSame('00:00:00', $state_clock->get());

    $this->container->get('module_installer')->uninstall(['dp_clock_mock']);

    self::assertSame('now', $file_clock->get());
    self::assertSame('now', $state_clock->get());
    self::assertNULL($state->get('dp_clock_mock'));
    self::assertNULL($state->get('dp_time_mock'));
  }

}
