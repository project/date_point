<?php

declare(strict_types=1);

namespace Drupal\Tests\date_point\Kernel\Hook;

use Drupal\KernelTests\KernelTestBase;
use Drupal\dp_clock_mock\Clock\StateClock;

/**
 * A test for hook_requirements().
 *
 * @covers \dp_clock_mock_requirements
 */
final class RequirementsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'date_point',
    'dp_clock_mock',
    'system',
  ];

  /**
   * {@selfdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);
    // The default pattern has been changed in Drupal 11.1, se we replace the
    // pattern to the formatter on older Drupal versions.
    // @see https://www.drupal.org/node/3467774
    $this->config('core.date_format.long')->set('pattern', 'd.m.Y h:i:s')->save();
  }

  /**
   * {@selfdoc}
   */
  public function testHook(): void {
    $this->config('system.date')->set('timezone.default', 'UTC')->save();
    $requirement = $this->getRequirement();
    self::assertSame('Current timestamp', $requirement['title']);
    // System time is not permanent, so just checking the year.
    self::assertStringContainsString(\date('Y'), $requirement['value']);
    self::assertSame('Time is fetched from "SystemClock" service.', $requirement['description']);
    self::assertSame(\REQUIREMENT_INFO, $requirement['severity']);

    $state_clock = \dp_clock_mock('dp_clock_mock.clock.state');
    \assert($state_clock instanceof StateClock);

    $state_clock->set('2024-03-28 16:30:00+00:00');

    $requirement = $this->getRequirement();
    self::assertSame('Current timestamp', $requirement['title']);
    self::assertSame('28.03.2024 04:30:00', $requirement['value']);
    self::assertSame('Time is fetched from "StateClock" service.', $requirement['description']);
    self::assertSame(\REQUIREMENT_INFO, $requirement['severity']);
  }

  /**
   * {@selfdoc}
   */
  private function getRequirement(): array {
    $module_handler = $this->container->get('module_handler');
    $file = $module_handler->loadInclude('dp_clock_mock', 'install');
    \assert(\is_string($file));
    self::assertStringEndsWith('/dp_clock_mock.install', $file);
    $requirement = $module_handler->invoke('dp_clock_mock', 'requirements', ['runtime'])['dp_clock_mock_timestamp'];
    $requirement['title'] = (string) $requirement['title'];
    $requirement['description'] = (string) $requirement['description'];
    return $requirement;
  }

}
