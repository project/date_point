<?php

declare(strict_types=1);

/**
 * @file
 * Primary module hooks for DP Clock Mock module.
 */
use Drupal\Component\Datetime\TimeInterface;
use Drupal\dp_clock_mock\Clock\FileClock;
use Drupal\dp_clock_mock\Clock\MemoryClock;
use Drupal\dp_clock_mock\Clock\SettingsClock;
use Drupal\dp_clock_mock\Clock\StateClock;
use Drupal\dp_clock_mock\DpClockMockServiceProvider;
use Psr\Clock\ClockInterface;

/**
 * Sets clock service.
 *
 * @phpstan-param non-empty-string|null $service
 *   ID of clock service or null if clock mock needs to be reset.
 */
function dp_clock_mock(?string $service): ClockInterface {
  $service ? DpClockMockServiceProvider::setMock('clock', $service) : DpClockMockServiceProvider::resetMock('clock');
  $kernel = \Drupal::service('kernel');
  $kernel->invalidateContainer();
  $kernel->rebuildContainer();
  return $kernel->getContainer()->get('date_point.clock');
}

/**
 * Sets memory clock as main clock service.
 */
function dp_clock_mock_memory(): MemoryClock {
  $clock = dp_clock_mock('dp_clock_mock.clock.memory');
  \assert($clock instanceof MemoryClock);
  return $clock;
}

/**
 * Sets state clock as main clock service.
 */
function dp_clock_mock_state(): StateClock {
  $clock = dp_clock_mock('dp_clock_mock.clock.state');
  \assert($clock instanceof StateClock);
  return $clock;
}

/**
 * Sets file clock as main clock service.
 */
function dp_clock_mock_file(): FileClock {
  $clock = dp_clock_mock('dp_clock_mock.clock.file');
  \assert($clock instanceof FileClock);
  return $clock;
}

/**
 * Sets settings clock as main clock service.
 */
function dp_clock_mock_settings(): SettingsClock {
  $clock = dp_clock_mock('dp_clock_mock.clock.settings');
  \assert($clock instanceof SettingsClock);
  return $clock;
}

/**
 * Sets time service.
 *
 * @phpstan-param non-empty-string|null $service
 *   ID of time service or null if time mock needs to be reset.
 */
function dp_time_mock(?string $service): TimeInterface {
  $service ? DpClockMockServiceProvider::setMock('time', $service) : DpClockMockServiceProvider::resetMock('time');
  $kernel = \Drupal::service('kernel');
  $kernel->invalidateContainer();
  $kernel->rebuildContainer();
  return $kernel->getContainer()->get('datetime.time');
}
