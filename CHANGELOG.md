# Changelog

## 1.x

## 1.1.0 – 2024-02-10

- Add Year-Month field type
- Add \Drupal\date_point\Data\Date::toDateTime() method

## 1.0.0 – 2024-12-05

- Replace state storage in DpClockMockServiceProvider

### 1.0.0-rc7 – 2024-09-21

- Rename Default date formatter
- Make datetime parameter required in DateTimeItem::setTime() and DateTimeItemList::setTime() methods

### 1.0.0-rc6 – 2024-08-20

- Rename date formats
- Add 'date-point:clocks' command
- Rename 'date-point' command to 'date-point:now'
- Add dp_time_mock() function
- Remove Drupal\date_point\Clock\StateClock service
- Add RandomClock service
- Add SettingsClock service
- Add FileClock service
- Add time service
- Display current timestamp in status report
- Remove Date::modify() and Time::modify() methods

### 1.0.0-rc5 – 2024-07-29

- Deprecate Date::modify() and Time::modify() methods
- Deprecate Date::toStorage() and Time::toStorage methods
- Make time parameter required in DateItem::setTime() and TimeItem::setTime() methods
- Add timezone override options to datetime filter
- Deprecate Time::fromDTI() method
- Deprecate Date::fromDTI() method
- Rename RFC date formats
- Add a service provider to set clock mock through state variable
- Deprecate \Drupal\date_point\Clock\StateClock service
- Add dp_mock_clock sub-module
- Change namespace for clock services
- Add support for Drupal 11
- Deprecate DateTimeItem::buildFormat() method
- Add constants for plugin IDs
- Convert plugin annotations to PHP attributes
- Drop support for PHP 8.2
- Remove request_clock() function

### 1.0.0-rc4 – 2024-07-01
